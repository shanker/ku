import java.io.*;
import java.util.*;
class MultScanApp {
  public static void main (String args[]) {
    Scanner infile = new Scanner(System.in);
    System.out.print("Please enter name of your input file: ");
    String IN = infile.nextLine();
    File INFILE = new File(IN);
    Scanner MS = new Scanner(System.in);
    System.out.print("Please enter number of scans desired: ");
    int maxScan = MS.nextInt();
    Scanner ctpt = new Scanner(System.in);
    System.out.print("Please enter name of a file to save table parameters: ");
    String COUT = ctpt.nextLine();
    List<List<String>> table = file2Table(INFILE);                             //import file as table
    Set<String> cnptSet = new HashSet<String>(table.get(table.size()-1).subList(1, table.get(table.size()-1).size()));
    int countAttr = (table.size()-1), countCase = (table.get(0).size()-1), countCnpt = cnptSet.size();       //table parameters
    List<List<Integer>> B_star = new ArrayList<List<Integer>>();
    List<Integer> rowIndex = new ArrayList<Integer>();
    List<String> rowID = new ArrayList<String>(Arrays.asList("rowID"));
    for (int i=1; i<table.get(0).size(); i++) {                                //iterate through cases
      rowIndex.add(i);
      rowID.add(Integer.toString(i));
    }
    B_star.add(rowIndex);                                                      //initialize {B}*
    List<List<String>> tableRow = new ArrayList<List<String>>(Arrays.asList(rowID)),
                       tableNum = new ArrayList<List<String>>(),
                       tableCat = new ArrayList<List<String>>();
    Map<String, Set<Float>> mapCutpoints = new HashMap<String, Set<Float>>();
    for (int i=0; i<table.size()-1; i++) {                                     //iterate through attributes
      if (table.get(i).get(1).matches("-?[0-9]+\\.?[0-9]*")) {
        mapCutpoints.put(table.get(i).get(0), new HashSet<Float>());
        tableNum.add(table.get(i));                                            //numeric attributes
      }
      else
        tableCat.add(table.get(i));                                            //categorical attributes
    }
    tableNum.add(table.get(table.size()-1));                                   //add decision column
    tableRow.addAll(tableNum);                                                 //add rowID column
    int count = 0;
    if (maxScan > count)
      mapCutpoints = multScanApproach(tableRow, B_star, mapCutpoints, count, maxScan);
    tableNum = DcrtTable(tableNum, mapCutpoints);
    List<List<String>> tableDcrt = new ArrayList<List<String>>(tableCat);
    tableDcrt.addAll(tableNum);
    PrintWriter cpw = null;
    try {
      File CTPTFILE = new File(COUT);
      if (!CTPTFILE.exists()) {
        CTPTFILE.createNewFile();
      }
      FileOutputStream ctptw = new FileOutputStream(CTPTFILE.getAbsoluteFile());
      cpw = new PrintWriter(ctptw);
      cpw.println("# cases = " + countCase);
      cpw.println("# attributes = " + countAttr);
      cpw.println("# concepts = " + countCnpt);
      cpw.println("\nCut-points:");
      printHash(mapCutpoints, cpw);
      cpw.println("\nTable parameters before interval-merging:");
      calcInterval(tableDcrt, cpw);
    }
    catch (IOException e) {
      e.printStackTrace();
    }
    tableNum = safemerge(tableNum);
//    List<List<String>> tableSafe = new ArrayList<List<String>>(tableCat);
//    tableSafe.addAll(tableNum);
    Scanner outproper = new Scanner(System.in);
    System.out.print("Please enter name of a file to save discretized and properly merged table: ");      //proper.txt
    String POUT = outproper.nextLine();
    try {
      File PROPERFILE = new File(POUT);
      if (!PROPERFILE.exists()) {
        PROPERFILE.createNewFile();
      }
      FileOutputStream properw = new FileOutputStream(PROPERFILE.getAbsoluteFile());
      PrintWriter prw = new PrintWriter(properw);
      tableNum = propermerge(tableNum);
      List<List<String>> tableProper = new ArrayList<List<String>>(tableCat);
      tableProper.addAll(tableNum);
      cpw.println("\nTable parameters after interval-merging:");
      calcInterval(tableProper, cpw);
      cpw.close();
      printTable(tableProper, prw);
      prw.close();
    }
    catch (IOException e) {
      e.printStackTrace();
    }
  }

//  METHODS
  //import table into a 2D array
  static List<List<String>> file2Table(File FILE) {
    List<List<String>> Arr2D = new ArrayList<List<String>>();
    int m=0, n=0, flag1=0, flag2=0, ncol=0;
    String strLine;
    try {
      FileInputStream fstream = new FileInputStream(FILE);
      DataInputStream in = new DataInputStream(fstream);
      BufferedReader br = new BufferedReader(new InputStreamReader(in));
      while ((strLine = br.readLine()) != null) {
        if (flag1 == 0) {
          ncol += strLine.split("[,\\s]+").length;
          if(strLine.matches(".*>\\s*")) {
            flag1 = 1;
            ncol -= 2;
            for (int x=0; x<ncol; x++)
              Arr2D.add(new ArrayList<String>());
          }
        }
        else if (flag2 == 0) {
          String[] stln = strLine.split("[,\\s]+");
          for (String s : stln) {
            if (!(s.equals("[") || s.equals("]"))) {
              if (m >= ncol)
                m = 0;
              Arr2D.get(m).add(s);
              m++;
            }
          }
          if (strLine.matches(".*\\]\\s*"))
            flag2 = 1;
        }
        else {
          if (! strLine.matches("")) {
            String[] stln = strLine.split("[,\\s]+");
            for (String s : stln) {
              if (n >= ncol)
                n = 0;
              Arr2D.get(n).add(s);
              n++;
            }
          }
        }
      }
      in.close();
    }
    catch (Exception e) {
      System.err.println("Error: " + e.getMessage());
    }
    return Arr2D;
  }

  //  Function to compute entropy
  //  input parameter = 1 attribute
  //  return value = entropy
  static float Etpy (List<String> attr) {
    int total = attr.size()-1;
    Set<String> attrSet = new HashSet<String>(attr.subList(1, attr.size()));
    float E = 0;
    Iterator<String> itr = attrSet.iterator();
    while (itr.hasNext()) {
      String item = itr.next();
      List<String> subattr = new ArrayList<String>();
      Iterator<String> itrA = attr.iterator();
      if (itrA.hasNext())
        itrA.next();
      while (itrA.hasNext()) {
        String i = itrA.next();
        if(item.equals(i))
          subattr.add(i);
      }
      int countA = subattr.size();
      E = (float) (E + ((- (float)countA / (float)total) * (Math.log((float)countA / (float)total)/Math.log(2))));
    }
    return E;
  }
  
  // Function to compute conditional entropy
  // input parameter = attribute and decision columns
  // return value = conditional entropy H(dcsn|attr)
  static float CndEtpy(List<String> attr, List<String> dcsn) {
    int total = attr.size()-1;                                                 //total number of elements
    Set<String> attrSet = new HashSet<String>(attr.subList(1, attr.size()));   //list of unique elements in the attribute
    Map<String, List<String>> pairAttrDcsn = new HashMap<String, List<String>>();
    Iterator<String> itr = attrSet.iterator();
    while (itr.hasNext()) {
      String iAttrSet = itr.next();
      List<String> subdcsn = new ArrayList<String>();
      for (int i=1; i<=total; i++)
        if(iAttrSet.equals(attr.get(i)))
          subdcsn.add(dcsn.get(i));
      pairAttrDcsn.put(iAttrSet, subdcsn);
    }
    float CE = 0;                                                              //initialize conditional entropy variable
    Iterator<Map.Entry<String, List<String>>> entries = pairAttrDcsn.entrySet().iterator();
    while (entries.hasNext()) {
        Map.Entry<String, List<String>> entry = entries.next();
        int countA = entry.getValue().size();                                  //initialize unique attribute count
        Set<String> valueSet = new HashSet<String>(entry.getValue());
        Iterator<String> itrVS = valueSet.iterator();
        while (itrVS.hasNext()) {
          String iVS = itrVS.next();
        int countD = 0;                                                        //initialize unique decision count within unique attributes
          Iterator<String> itrV = entry.getValue().iterator();
          while (itrV.hasNext()) {
            String iV = itrV.next();
            if(iV.equals(iVS))
              countD++;
          }
          CE = (float) (CE+(((float)countA/(float)total)*(-(float)countD/(float)countA)*(Math.log((float)countD / (float)countA)/Math.log(2)))); 
        }
    }
    return CE;
  }

  // Function to determine best cut-point
  // input parameter = best-attribute, decision
  // return value = best-cut-point
  static float bestCutpoint(List<String>attr, List<String>dcsn) {
    TreeSet<Float> attrTS = new TreeSet<Float>();
    TreeSet<Float> cutpoints = new TreeSet<Float>();
    TreeMap<Float, Float> cutpointTM = new TreeMap<Float, Float>();
    Iterator<String> itr = attr.iterator();
    String attrName = attr.get(0);
    if (itr.hasNext())
      itr.next();
    while (itr.hasNext())
      attrTS.add(Float.parseFloat(itr.next()));
    List<Float> attrLst = new ArrayList<Float>(attrTS);
    for (int i=1; i<attrLst.size(); i++) {
      float cutpoint = (attrLst.get(i)+attrLst.get(i-1))/2;
      cutpoints.add(cutpoint);
    }
    for (float j : cutpoints) {
      List<Integer> iLower = new ArrayList<Integer>();
      List<Integer> iUpper = new ArrayList<Integer>();
      List<String> Lower = new ArrayList<String>();
      List<String> Upper = new ArrayList<String>();
      Lower.add(attrName);
      Upper.add(attrName);
      for (int i=1; i<attr.size(); i++)
        if(j > Float.parseFloat(attr.get(i)))
          iLower.add(i);
        else
          iUpper.add(i);
      Iterator<Integer> itrL = iLower.iterator();
      while (itrL.hasNext())
        Lower.add(dcsn.get(itrL.next()));
      Iterator<Integer> itrU = iUpper.iterator();
      while (itrU.hasNext())
        Upper.add(dcsn.get(itrU.next()));
      int U = attr.size()-1, S1 = iLower.size(), S2 = iUpper.size();
      float cutptEntropy = (((float)S1 / (float)U) * Etpy(Lower)) + (((float)S2 / (float)U) * Etpy(Upper));
      if(! cutpointTM.containsKey(cutptEntropy))
        cutpointTM.put(cutptEntropy, j);
    }
    return cutpointTM.firstEntry().getValue();
  }

  // Function to compute partition on U by attribute
  // input parameter = attribute
  // return value = partition on U by attribute
  static List<List<Integer>> prtn(List<String> attr) {
    Set<String> attrSet = new HashSet<String>(attr.subList(1, attr.size()));
    List<List<Integer>> partition = new ArrayList<List<Integer>>();
    for (String i : attrSet) {
      List<Integer> block = new ArrayList<Integer>();
      Iterator<String> itr = attr.iterator();
      if (itr.hasNext())
        itr.next();
      int index = 1;
      while (itr.hasNext()) {
        String j = itr.next();
        if(i.equals(j))
          block.add(index);
        index++;
      }
      partition.add(block);
    }
    return partition;
  }

  // Function to compute partition on U by attribute
  // input parameter = attribute, rowIDs
  // return value = partition on U by attribute
  static List<List<Integer>> prtn(List<String> attr, List<String> id) {
    Set<String> attrSet = new HashSet<String>(attr.subList(1, attr.size()));
    List<List<Integer>> partition = new ArrayList<List<Integer>>();
    for (String i : attrSet) {
      List<Integer> block = new ArrayList<Integer>();
      Iterator<String> itr = attr.iterator(), itrID = id.iterator();
      if (itr.hasNext()) {
        itr.next();
        itrID.next();
      }
      while (itr.hasNext()) {
        String j = itr.next();
        int index = Integer.parseInt(itrID.next());
        if(i.equals(j))
          block.add(index);
      }
      partition.add(block);
    }
    return partition;
  }

  // Function to compute double partition
  // input parameter = partition1, partition2
  // return value = double partition
  static List<List<Integer>> doublePrtn(List<List<Integer>> prtn1, List<List<Integer>> prtn2) {
    List<List<Integer>> partition = new ArrayList<List<Integer>>();
    Iterator<List<Integer>> itrP1 = prtn1.iterator();
    while(itrP1.hasNext()) {
      List<Integer> p1 = itrP1.next();
      Iterator<List<Integer>> itrP2 = prtn2.iterator();
      while(itrP2.hasNext()) {
        List<Integer> p2 = itrP2.next();
        List<Integer> dp = new ArrayList<Integer>();
        dp.addAll(p1);
        dp.retainAll(p2);
        if (!dp.isEmpty())
          partition.add(dp);
      }
    }
    return partition;
  }

  // Function to compute partition of 3 or more attributes
  // input parameters = table of attributes
  // return value = partition of all attributes
  static List<List<Integer>> allAttrPrtn(List<List<String>> T) {
    List<List<Integer>> partition = prtn(T.get(0));
    for (int i=1; i<T.size()-1; i++)
      partition = doublePrtn(partition, prtn(T.get(i)));
    return partition;
  }

  // Function to compute level of consistency
  // input parameter = partition1, partition2, U
  // return value = level of consistency
  static float Cnstcy(List<List<Integer>> prtnDcsn, List<List<Integer>> prtnAttr, int U) {
    int sgmaCrdnlty=0;
    Iterator<List<Integer>> ItrDcsn = prtnDcsn.iterator();
    while (ItrDcsn.hasNext()) {
      List<Integer> d = ItrDcsn.next();
      int Crdnlty=0;
      Iterator <List<Integer>> ItrAttr = prtnAttr.iterator();
      while (ItrAttr.hasNext()) {
        List<Integer> a = ItrAttr.next();
        if (d.containsAll(a))
          Crdnlty = Crdnlty+a.size();
      }
      sgmaCrdnlty = sgmaCrdnlty + Crdnlty;
    }
    return (float) sgmaCrdnlty/U;
  }
  
  // Recursive algorithm to compute all cut-points
  // input parameter = table, partition, cut-point-container, scanCounter, maxScan
  // return value = cut-point container
  static Map<String, Set<Float>> multScanApproach(List<List<String>> T, List<List<Integer>> B_star, Map<String, Set<Float>> C, int count, int maxScan) {
    List<String> decision = T.get(T.size()-1);
    TreeMap<Float, String> CEtree = new TreeMap<Float, String>();              // attribute conditional entropy
    Map<String, Float> attrBestCP = new HashMap<String, Float>();              // attr -> best cutpoint
    Iterator<List<String>> itr = T.iterator();                                 // column iterator
    List<String> rowID = itr.next();                                           // skip 1st column
    while (itr.hasNext()) {
      List<String> attribute = itr.next();
      Set<String> attrUnq = new HashSet<String>(attribute);
      String attrName = attribute.get(0);
      if(attrUnq.size() > 2 & ! attrName.equals(decision.get(0))) {            // skip decision & attribute with only 1 unique case
        float condEntropy = CndEtpy(attribute, decision);                      // conditional entropy
        float bestCP = bestCutpoint(attribute, decision);                      // best cut-point
        // Fill containers
        if(! CEtree.containsKey(condEntropy)) {
          CEtree.put(condEntropy, attrName);
          attrBestCP.put(attrName, bestCP);
        }
        // Split attribute across cut-point
        List<Integer> lesser = new ArrayList<Integer>(),
                  greater = new ArrayList<Integer>();
        Iterator<String> itrRow = rowID.iterator(), itrAttr = attribute.iterator();  // row iterator
        itrRow.next();
        itrAttr.next();                                                        // skip header
        while (itrAttr.hasNext()) {
          float X = Float.parseFloat(itrAttr.next());
          if (X < bestCP)
            lesser.add(Integer.parseInt(itrRow.next()));
          else
            greater.add(Integer.parseInt(itrRow.next()));
        }
        B_star = doublePrtn(B_star, Arrays.asList(lesser, greater));           // update {B}*
        // Add new cut-point to C
        Iterator<Map.Entry<String, Set<Float>>> entries = C.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry<String, Set<Float>> entry = entries.next();
            if (entry.getKey().equals(attrName))
              entry.getValue().add(bestCP);
        }
      }
    }
    int U = T.get(0).size()-1;
    List<List<Integer>> prtnDcsn = prtn(decision, rowID);
    float LB = Cnstcy(prtnDcsn, B_star, U);
    // Continue discretization if table is inconsistent
    if (LB < 1) {
      // Construct subsets for further discretization
      Iterator<List<Integer>> itrB = B_star.iterator();                        // B_star iterator
      while (itrB.hasNext()) {
        List<Integer> B = itrB.next();
        //check if B is consistent by decision
        int flag = 0;
        Iterator<List<Integer>> itrDcsn = prtnDcsn.iterator();
        while (itrDcsn.hasNext())
          if (itrDcsn.next().containsAll(B))
            flag = 1;
        // skip subset if satisfied by decision
        if (B.size() > 1 & flag == 0) {
          List<List<String>> subset = new ArrayList<List<String>>();
          List<String> rowCol = T.get(0);
          Iterator<List<String>> itrT = T.iterator();                          // column iterator
          while (itrT.hasNext()) {
            List<String> attr = itrT.next();
            List<String> subAttr = new ArrayList<String>();
            Iterator<String> itrE = attr.iterator();                           // row iterator
            subAttr.add(itrE.next());                                          // add header
            int i = 1;                                                         // row counter
            while (itrE.hasNext()) {
              String element = itrE.next();
              if (B.contains(Integer.parseInt(rowCol.get(i))))
                subAttr.add(element);                                          // construct sub-attribute
              i++;
            }
            subset.add(subAttr);                                               // construct sub-table
          }
          if (subset.get(0).size() > 2)
            if (count < maxScan) {
              count++;
              C = multScanApproach(subset, B_star, C, count, maxScan);
            }
            else
              C = domAttrApproach(subset, B_star, C);
        }
      }
    }
    return C;
  }
  
  // Recursive algorithm to compute all cut-points
  // input parameter = table, partition, cut-point container
  // return value = cut-point container
  static Map<String, Set<Float>> domAttrApproach(List<List<String>> T, List<List<Integer>> B_star, Map<String, Set<Float>> C) {
    //compute attribute conditional entropies
    List<String> decision = T.get(T.size()-1), bestAttr = new ArrayList<String>();
    TreeMap<Float, String> CEtree = new TreeMap<Float, String>();
    Iterator<List<String>> itr = T.iterator();
    itr.next();                                                                //skip 1st column
    while (itr.hasNext()) {                                                    //iterate through columns i.e., attributes and decision
      List<String> attribute = itr.next();
      if(! attribute.get(0).equals(decision.get(0))) {                         //skip decision
        float condEntropy = CndEtpy(attribute, decision);                      //conditional entropy
        if(! CEtree.containsKey(condEntropy))
          CEtree.put(condEntropy, attribute.get(0));
      }
    }
    //select best attribute and find its best cut-point
    String bestAttrName = CEtree.firstEntry().getValue();
    Iterator<List<String>> itr1 = T.iterator();
    while (itr1.hasNext()) {                                                   //iterate through columns i.e., attributes and decision
      List<String> attribute = itr1.next();
      if(attribute.get(0).equals(bestAttrName))                                //select best attribute
        bestAttr.addAll(attribute);
    }
    float bestCP = bestCutpoint(bestAttr, decision);                           //best cut-point
    //split table across best cut-point
    Iterator<List<String>> itr2 = T.iterator();
    List<String> rowID = itr2.next();
    while (itr2.hasNext()) {
      List<String> attribute = itr2.next();
      Set<String> attrUnq = new HashSet<String>(attribute);
      if(attrUnq.size() > 2 & attribute.get(0).equals(bestAttrName)) {         //skip decision
        Iterator<String> itrRow = rowID.iterator(), itrAttr = attribute.iterator();      // row iterator
        itrRow.next();
        itrAttr.next();                                                        // skip header
        List<Integer> lesser = new ArrayList<Integer>(), greater = new ArrayList<Integer>();
        while (itrAttr.hasNext()) {
          float X = Float.parseFloat(itrAttr.next());
          if (X < bestCP)
            lesser.add(Integer.parseInt(itrRow.next()));
          else
            greater.add(Integer.parseInt(itrRow.next()));
        }
        B_star = doublePrtn(B_star, Arrays.asList(lesser, greater));           // update {B}*
      }
    }
    //Fill hash table: attributeName -> cut-points
    Iterator<Map.Entry<String, Set<Float>>> entries = C.entrySet().iterator();
    while (entries.hasNext()) {
      Map.Entry<String, Set<Float>> entry = entries.next();
      if (entry.getKey().equals(bestAttrName))
        entry.getValue().add(bestCP);
    }
    int U = bestAttr.size()-1;
    List<List<Integer>> prtnDcsn = prtn(decision, rowID);
    float LB = Cnstcy(prtnDcsn, B_star, U);
    //recursive part of the algorithm
    if (LB < 1) {
      // Construct subsets for further discretization
      Iterator<List<Integer>> itrB = B_star.iterator();                        // B_star iterator
      while (itrB.hasNext()) {
        List<Integer> B = itrB.next();
        //check if B is consistent by decision
        int flag = 0;
        Iterator<List<Integer>> itrDcsn = prtnDcsn.iterator();
        while (itrDcsn.hasNext())
          if (itrDcsn.next().containsAll(B))
            flag = 1;
        // skip subset if satisfied by decision
        if (B.size() > 1 & flag == 0) {
          List<List<String>> subset = new ArrayList<List<String>>();
          List<String> rowCol = T.get(0);
          Iterator<List<String>> itrT = T.iterator();                          // column iterator
          while (itrT.hasNext()) {
            List<String> attr = itrT.next();
            List<String> subAttr = new ArrayList<String>();
            Iterator<String> itrE = attr.iterator();                           // row iterator
            subAttr.add(itrE.next());                                          // add header
            int i = 1;                                                         // row counter
            while (itrE.hasNext()) {
              String element = itrE.next();
              if (B.contains(Integer.parseInt(rowCol.get(i))))
                subAttr.add(element);                                          // construct sub-attribute
              i++;
            }
            subset.add(subAttr);                                               // construct sub-table
          }
          if (subset.get(0).size() > 2)
            domAttrApproach(subset, B_star, C);
        }
      }
    }
    return C;                                                                  //return complete list of attribute->cutpoints pairs
  }
  
  // Function to convert original table into discretized table
  // input parameter = table, cut-points
  // return value = discretized table
  static List<List<String>> DcrtTable(List<List<String>> T, Map<String, Set<Float>> C) {
    for (int i=0; i<T.size()-1; i++) {                                         //iterate through attributes
      String header = T.get(i).get(0);                                         //attribute name
      List<String> strList = T.get(i).subList(1, T.get(i).size());             //attribute elements
      //string to float conversion
      List<Float> fltList = new ArrayList<Float>();
      Iterator<String> itrS = strList.iterator();
      while (itrS.hasNext())
        fltList.add(Float.parseFloat(itrS.next()));
      //bin boundaries
      float minimum = Collections.min(fltList);
      float maximum = Collections.max(fltList);
      TreeSet<Float> rangeTree = new TreeSet<Float>(C.get(header));            //add cutpoints
      rangeTree.add(minimum);                                                  //add minimum
      rangeTree.add(maximum);                                                  //add maximum
      List<Float> rangeList = new ArrayList<Float>(rangeTree);                 //convert tree to list structure
      //substitute value with range
      int index=0;
      Iterator<Float> itrF = fltList.iterator();
      while (itrF.hasNext()) {
        float fltVal = itrF.next();
        String rngVal = "";
        for (int j=0; j<rangeList.size()-1; j++) {
          float a = rangeList.get(j), b = rangeList.get(j+1);
          if (fltVal >= a && fltVal <= b)
            rngVal = a + ".." + b;
        }
        strList.set(index++, rngVal);
      }
      strList.add(0,header);
            //reconstructed table with discretized attribute
      T.set(i, strList);
    }
    return T;
  }

  // Function to safely merge neighboring intervals
  // input parameter = discretized table
  // return value = safely merged table
  static List<List<String>> safemerge(List<List<String>> T) {
    for (int i=0; i<T.size()-1; i++)                                           //iterate through attributes
      for (int j=1; j<T.get(0).size(); j++) {                                  //iterate through cases
        String val1 = T.get(i).get(j);                                         //1st interval (format e.g, i..j)
        String[] tokensVal1 = val1.split("\\.\\.");                            //store interval boundaries into a 1st array
        String dcsnVal1 = T.get(T.size()-1).get(j);                            //decision corresponding to 1st value
        int flag = 1;
        for (int k=1; k<T.get(0).size(); k++) {                                //iterate though cases
          String val2 = T.get(i).get(k);                                       //2nd interval
          String[] tokensVal2 = val2.split("\\.\\.");                          //store interval boundaries into a 2nd array
          String dcsnVal2 = T.get(T.size()-1).get(k);                          //decision corresponding to 2nd value
          if ((val1.equals(val2) || tokensVal1[1].equals(tokensVal2[0])) && (!(dcsnVal1.equals(dcsnVal2)))) {
            flag = 0;
            break;
          }
        }
        if (flag==1)
          for (int k=1; k<T.get(0).size(); k++) {                              //iterate though cases
            String val2 = T.get(i).get(k);                                     //2nd interval
            String[] tokensVal2 = val2.split("\\.\\.");                        //store interval boundaries into a 2nd array
            String dcsnVal2 = T.get(T.size()-1).get(k);                        //decision corresponding to 2nd value
            if (tokensVal1[1].equals(tokensVal2[0]) && dcsnVal1.equals(dcsnVal2)) {
              String val = tokensVal1[0]+".."+tokensVal2[1];
              T.get(i).set(j, val);
              T.get(i).set(k, val);
            }
          }
      }
    return T;
  }

  // Function to properly merge neighboring intervals
  // input parameter = safely merged table
  // return value = properly merged table
  static List<List<String>> propermerge(List<List<String>> T) {
    int U = T.get(0).size()-1;                                                 //total number of cases
    List<List<Integer>> prtnDcsn = prtn(T.get(T.size()-1));                    //{d}*
    List<List<Integer>> A_star = allAttrPrtn(T);                               //{A}*
    float LA = Cnstcy(prtnDcsn, A_star, U);                                    //level of consistency
    for (int i=0; i<T.size()-1; i++) {                                         //iterate through attributes
      for (int j=1; j<T.get(0).size(); j++) {                                  //iterate through cases        
        List<String> temp = new ArrayList<String>();
        for (int x=0; x<T.get(0).size(); x++)
          temp.add(T.get(i).get(x));                                           //duplicate for restoration if consistency is reduced
        String val1 = T.get(i).get(j);                                         //1st interval
        String[] tokensVal1 = val1.split("\\.\\.");                            //tokenize
        for (int k=1; k<T.get(0).size(); k++) {                                //iterate through cases again
          String val2 = T.get(i).get(k);                                       //2nd interval
          String[] tokensVal2 = val2.split("\\.\\.");                          //tokenize
          if (! tokensVal2[0].isEmpty())
            if (tokensVal1[1].equals(tokensVal2[0])) {                         //if val1 and val2 are neighboring intervals
              String val = tokensVal1[0]+".."+tokensVal2[1];                   //construct for merged interval
              // globally merge neighboring intervals
              for (int l=1; l<T.get(0).size(); l++) { 
                String valCurr = T.get(i).get(l);
                if (valCurr.equals(val1) || valCurr.equals(val2))
                  T.get(i).set(l, val);
              }
            }
        }
        float newLA = Cnstcy(prtnDcsn, allAttrPrtn(T), U);
        // if consistency is reduced, restore original attribute
        if(newLA < LA)
          T.set(i, temp);
      }
    }
    return T;
  }
  
  // Function to print table
  // Input parameter = table
  static void printTable(List<List<String>> T, PrintWriter pw) {
    for (int i=0; i<=T.size()+1; i++) {
      if (i == 0)
        pw.print("< ");
      else if (i == T.size()+1)
        pw.println(">");
      else if (i == T.size())
        pw.print("d ");
      else
        pw.print("a ");
    }
    for (int i=0; i<=T.size()+1; i++) {
      if (i == 0)
        pw.print("[ ");
      else if (i == T.size()+1)
        pw.println("]");
      else
        pw.print(T.get(i-1).get(0) + " ");
    }
    for (int i=1; i<T.get(0).size(); i++) {
      for (int j=0; j<T.size(); j++)
        pw.print(T.get(j).get(i) + " ");
      pw.println();
    }
  }

  // Function to print hash-function
  // Input parameter = hash-function
  static void printHash(Map<String, Set<Float>> H, PrintWriter cpw) {
    Iterator<Map.Entry<String, Set<Float>>> entries = H.entrySet().iterator();
    while (entries.hasNext()) {
      Map.Entry<String, Set<Float>> entry = entries.next();
      cpw.println(entry.getKey() + " -> " + entry.getValue());
    }
  }
  
  // Function to count table attributes
  // Input parameter = table
  // Printed parameters = Unique intervals, total number of intervals, number of intervals per attribute
  static void calcInterval(List<List<String>> T, PrintWriter cpw) {
    int countAttr = T.size()-1, countIntvl = 0;
    cpw.println("Unique intervals:");
    for (int j=0; j<countAttr; j++) {
      Set<String> attrSet = new HashSet<String>(T.get(j).subList(1, T.get(j).size()));
      cpw.println(T.get(j).get(0) + " -> " + attrSet);
      countIntvl = countIntvl + attrSet.size();
    }
    float avgIntvl = (float) countIntvl/countAttr;
    cpw.println("# intervals = " + countIntvl);
    cpw.println("# intervals/attribute = " + String.format("%.2f", avgIntvl));
  }
}
