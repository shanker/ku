#!/usr/local/bin/perl -w
$input = "training_set.txt";
$output = "Mutual_Information_Fshb.txt";
@Fshb=(3.94597054,4.040515314,4.077007964,2.261500773,2.302763708,2.2757719,2.336259552);
$Fshb_sum=$Fshb[0]+$Fshb[1]+$Fshb[2]+$Fshb[3]+$Fshb[4]+$Fshb[5]+$Fshb[6];
$p1_Fshb=$Fshb[0]/$Fshb_sum;
$p2_Fshb=$Fshb[1]/$Fshb_sum;
$p3_Fshb=$Fshb[2]/$Fshb_sum;
$p4_Fshb=$Fshb[3]/$Fshb_sum;
$p5_Fshb=$Fshb[4]/$Fshb_sum;
$p6_Fshb=$Fshb[5]/$Fshb_sum;
$p7_Fshb=$Fshb[6]/$Fshb_sum;
open (INFILE, $input);
open (OUTFILE, ">>$output");
print OUTFILE "Mutual Information with Fshb\nMI score\tProbe ID\n";
while ($line = <INFILE>)
{
  @linearr = split(/\t/, $line);
  $sum=$linearr[2]+$linearr[5]+$linearr[8]+$linearr[11]+$linearr[14]+$linearr[17]+$linearr[20];
  $p1=$linearr[2]/$sum;
  $p2=$linearr[5]/$sum;
  $p3=$linearr[8]/$sum;
  $p4=$linearr[11]/$sum;
  $p5=$linearr[14]/$sum;
  $p6=$linearr[17]/$sum;
  $p7=$linearr[20]/$sum;
  $p1_p1_Fshb=($linearr[2]+$Fshb[0])/($sum+$Fshb_sum);
  $p2_p2_Fshb=($linearr[5]+$Fshb[1])/($sum+$Fshb_sum);
  $p3_p3_Fshb=($linearr[8]+$Fshb[2])/($sum+$Fshb_sum);
  $p4_p4_Fshb=($linearr[11]+$Fshb[3])/($sum+$Fshb_sum);
  $p5_p5_Fshb=($linearr[14]+$Fshb[4])/($sum+$Fshb_sum);
  $p6_p6_Fshb=($linearr[17]+$Fshb[5])/($sum+$Fshb_sum);
  $p7_p7_Fshb=($linearr[20]+$Fshb[6])/($sum+$Fshb_sum);
  $MI=-(($p1_p1_Fshb*log($p1*$p1_Fshb/$p1_p1_Fshb))
       + ($p2_p2_Fshb*log($p2*$p2_Fshb/$p2_p2_Fshb))
       + ($p3_p3_Fshb*log($p3*$p3_Fshb/$p3_p3_Fshb))
       + ($p4_p4_Fshb*log($p4*$p4_Fshb/$p4_p4_Fshb))
       + ($p5_p5_Fshb*log($p5*$p5_Fshb/$p5_p5_Fshb))
       + ($p6_p6_Fshb*log($p6*$p6_Fshb/$p6_p6_Fshb))
       + ($p7_p7_Fshb*log($p7*$p7_Fshb/$p7_p7_Fshb)));
  print OUTFILE "$MI\t$linearr[0]\n";
}
close INFILE;
close OUTFILE;
