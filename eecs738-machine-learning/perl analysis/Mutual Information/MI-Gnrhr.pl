#!/usr/local/bin/perl -w
$input = "training_set.txt";
$output = "Mutual_Information_Gnrhr.txt";
@Gnrhr=(3.419145852,3.474361976,3.410659157,2.44963265,2.397418542,2.421932813,2.295127085);
$Gnrhr_sum=$Gnrhr[0]+$Gnrhr[1]+$Gnrhr[2]+$Gnrhr[3]+$Gnrhr[4]+$Gnrhr[5]+$Gnrhr[6];
$p1_Gnrhr=$Gnrhr[0]/$Gnrhr_sum;
$p2_Gnrhr=$Gnrhr[1]/$Gnrhr_sum;
$p3_Gnrhr=$Gnrhr[2]/$Gnrhr_sum;
$p4_Gnrhr=$Gnrhr[3]/$Gnrhr_sum;
$p5_Gnrhr=$Gnrhr[4]/$Gnrhr_sum;
$p6_Gnrhr=$Gnrhr[5]/$Gnrhr_sum;
$p7_Gnrhr=$Gnrhr[6]/$Gnrhr_sum;
open (INFILE, $input);
open (OUTFILE, ">>$output");
print OUTFILE "Mutual Information with Gnrhr\nMI score\tProbe ID\n";
while ($line = <INFILE>)
{
  @linearr = split(/\t/, $line);
  $sum=$linearr[2]+$linearr[5]+$linearr[8]+$linearr[11]+$linearr[14]+$linearr[17]+$linearr[20];
  $p1=$linearr[2]/$sum;
  $p2=$linearr[5]/$sum;
  $p3=$linearr[8]/$sum;
  $p4=$linearr[11]/$sum;
  $p5=$linearr[14]/$sum;
  $p6=$linearr[17]/$sum;
  $p7=$linearr[20]/$sum;
  $p1_p1_Gnrhr=($linearr[2]+$Gnrhr[0])/($sum+$Gnrhr_sum);
  $p2_p2_Gnrhr=($linearr[5]+$Gnrhr[1])/($sum+$Gnrhr_sum);
  $p3_p3_Gnrhr=($linearr[8]+$Gnrhr[2])/($sum+$Gnrhr_sum);
  $p4_p4_Gnrhr=($linearr[11]+$Gnrhr[3])/($sum+$Gnrhr_sum);
  $p5_p5_Gnrhr=($linearr[14]+$Gnrhr[4])/($sum+$Gnrhr_sum);
  $p6_p6_Gnrhr=($linearr[17]+$Gnrhr[5])/($sum+$Gnrhr_sum);
  $p7_p7_Gnrhr=($linearr[20]+$Gnrhr[6])/($sum+$Gnrhr_sum);
  $MI=-(($p1_p1_Gnrhr*log($p1*$p1_Gnrhr/$p1_p1_Gnrhr))
       + ($p2_p2_Gnrhr*log($p2*$p2_Gnrhr/$p2_p2_Gnrhr))
       + ($p3_p3_Gnrhr*log($p3*$p3_Gnrhr/$p3_p3_Gnrhr))
       + ($p4_p4_Gnrhr*log($p4*$p4_Gnrhr/$p4_p4_Gnrhr))
       + ($p5_p5_Gnrhr*log($p5*$p5_Gnrhr/$p5_p5_Gnrhr))
       + ($p6_p6_Gnrhr*log($p6*$p6_Gnrhr/$p6_p6_Gnrhr))
       + ($p7_p7_Gnrhr*log($p7*$p7_Gnrhr/$p7_p7_Gnrhr)));
  print OUTFILE "$MI\t$linearr[0]\n";
}
close INFILE;
close OUTFILE;
