#!/usr/local/bin/perl -w
$input = "training_set.txt";
$output = "Mutual_Information_Lhb.txt";
@Lhb=(4.167210964,4.285381756,4.202439272,2.345961542,2.350829274,2.323870607,2.419294722);
$Lhb_sum=$Lhb[0]+$Lhb[1]+$Lhb[2]+$Lhb[3]+$Lhb[4]+$Lhb[5]+$Lhb[6];
$p1_Lhb=$Lhb[0]/$Lhb_sum;
$p2_Lhb=$Lhb[1]/$Lhb_sum;
$p3_Lhb=$Lhb[2]/$Lhb_sum;
$p4_Lhb=$Lhb[3]/$Lhb_sum;
$p5_Lhb=$Lhb[4]/$Lhb_sum;
$p6_Lhb=$Lhb[5]/$Lhb_sum;
$p7_Lhb=$Lhb[6]/$Lhb_sum;
open (INFILE, $input);
open (OUTFILE, ">>$output");
print OUTFILE "Mutual Information with Lhb\nMI score\tProbe ID\n";
while ($line = <INFILE>)
{
  @linearr = split(/\t/, $line);
  $sum=$linearr[2]+$linearr[5]+$linearr[8]+$linearr[11]+$linearr[14]+$linearr[17]+$linearr[20];
  $p1=$linearr[2]/$sum;
  $p2=$linearr[5]/$sum;
  $p3=$linearr[8]/$sum;
  $p4=$linearr[11]/$sum;
  $p5=$linearr[14]/$sum;
  $p6=$linearr[17]/$sum;
  $p7=$linearr[20]/$sum;
  $p1_p1_Lhb=($linearr[2]+$Lhb[0])/($sum+$Lhb_sum);
  $p2_p2_Lhb=($linearr[5]+$Lhb[1])/($sum+$Lhb_sum);
  $p3_p3_Lhb=($linearr[8]+$Lhb[2])/($sum+$Lhb_sum);
  $p4_p4_Lhb=($linearr[11]+$Lhb[3])/($sum+$Lhb_sum);
  $p5_p5_Lhb=($linearr[14]+$Lhb[4])/($sum+$Lhb_sum);
  $p6_p6_Lhb=($linearr[17]+$Lhb[5])/($sum+$Lhb_sum);
  $p7_p7_Lhb=($linearr[20]+$Lhb[6])/($sum+$Lhb_sum);
  $MI=-(($p1_p1_Lhb*log($p1*$p1_Lhb/$p1_p1_Lhb))
       + ($p2_p2_Lhb*log($p2*$p2_Lhb/$p2_p2_Lhb))
       + ($p3_p3_Lhb*log($p3*$p3_Lhb/$p3_p3_Lhb))
       + ($p4_p4_Lhb*log($p4*$p4_Lhb/$p4_p4_Lhb))
       + ($p5_p5_Lhb*log($p5*$p5_Lhb/$p5_p5_Lhb))
       + ($p6_p6_Lhb*log($p6*$p6_Lhb/$p6_p6_Lhb))
       + ($p7_p7_Lhb*log($p7*$p7_Lhb/$p7_p7_Lhb)));
  print OUTFILE "$MI\t$linearr[0]\n";
}
close INFILE;
close OUTFILE;
