#!/usr/local/bin/perl -w
$input = "training_set.txt";
$output = "Mutual_Information_Itpr1.txt";
@Itpr1=(3.400796935,3.374803317,3.399379478,2.556784782,2.612041745,2.483872454,2.503109437);
$Itpr1_sum=$Itpr1[0]+$Itpr1[1]+$Itpr1[2]+$Itpr1[3]+$Itpr1[4]+$Itpr1[5]+$Itpr1[6];
$p1_Itpr1=$Itpr1[0]/$Itpr1_sum;
$p2_Itpr1=$Itpr1[1]/$Itpr1_sum;
$p3_Itpr1=$Itpr1[2]/$Itpr1_sum;
$p4_Itpr1=$Itpr1[3]/$Itpr1_sum;
$p5_Itpr1=$Itpr1[4]/$Itpr1_sum;
$p6_Itpr1=$Itpr1[5]/$Itpr1_sum;
$p7_Itpr1=$Itpr1[6]/$Itpr1_sum;
open (INFILE, $input);
open (OUTFILE, ">>$output");
print OUTFILE "Mutual Information with Itpr1\nMI score\tProbe ID\n";
while ($line = <INFILE>)
{
  @linearr = split(/\t/, $line);
  $sum=$linearr[2]+$linearr[5]+$linearr[8]+$linearr[11]+$linearr[14]+$linearr[17]+$linearr[20];
  $p1=$linearr[2]/$sum;
  $p2=$linearr[5]/$sum;
  $p3=$linearr[8]/$sum;
  $p4=$linearr[11]/$sum;
  $p5=$linearr[14]/$sum;
  $p6=$linearr[17]/$sum;
  $p7=$linearr[20]/$sum;
  $p1_p1_Itpr1=($linearr[2]+$Itpr1[0])/($sum+$Itpr1_sum);
  $p2_p2_Itpr1=($linearr[5]+$Itpr1[1])/($sum+$Itpr1_sum);
  $p3_p3_Itpr1=($linearr[8]+$Itpr1[2])/($sum+$Itpr1_sum);
  $p4_p4_Itpr1=($linearr[11]+$Itpr1[3])/($sum+$Itpr1_sum);
  $p5_p5_Itpr1=($linearr[14]+$Itpr1[4])/($sum+$Itpr1_sum);
  $p6_p6_Itpr1=($linearr[17]+$Itpr1[5])/($sum+$Itpr1_sum);
  $p7_p7_Itpr1=($linearr[20]+$Itpr1[6])/($sum+$Itpr1_sum);
  $MI=-(($p1_p1_Itpr1*log($p1*$p1_Itpr1/$p1_p1_Itpr1))
       + ($p2_p2_Itpr1*log($p2*$p2_Itpr1/$p2_p2_Itpr1))
       + ($p3_p3_Itpr1*log($p3*$p3_Itpr1/$p3_p3_Itpr1))
       + ($p4_p4_Itpr1*log($p4*$p4_Itpr1/$p4_p4_Itpr1))
       + ($p5_p5_Itpr1*log($p5*$p5_Itpr1/$p5_p5_Itpr1))
       + ($p6_p6_Itpr1*log($p6*$p6_Itpr1/$p6_p6_Itpr1))
       + ($p7_p7_Itpr1*log($p7*$p7_Itpr1/$p7_p7_Itpr1)));

  print OUTFILE "$MI\t$linearr[0]\n";
}
close INFILE;
close OUTFILE;
