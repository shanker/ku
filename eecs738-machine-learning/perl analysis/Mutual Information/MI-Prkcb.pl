#!/usr/local/bin/perl -w
$input = "training_set.txt";
$output = "Mutual_Information_Prkcb.txt";
@Prkcb=(2.325720858,2.39252109,2.415474168,3.211867761,3.131394082,3.136720567,3.070591932,2.412832426);
$Prkcb_sum=$Prkcb[0]+$Prkcb[1]+$Prkcb[2]+$Prkcb[3]+$Prkcb[4]+$Prkcb[5]+$Prkcb[6];
$p1_Prkcb=$Prkcb[0]/$Prkcb_sum;
$p2_Prkcb=$Prkcb[1]/$Prkcb_sum;
$p3_Prkcb=$Prkcb[2]/$Prkcb_sum;
$p4_Prkcb=$Prkcb[3]/$Prkcb_sum;
$p5_Prkcb=$Prkcb[4]/$Prkcb_sum;
$p6_Prkcb=$Prkcb[5]/$Prkcb_sum;
$p7_Prkcb=$Prkcb[6]/$Prkcb_sum;
open (INFILE, $input);
open (OUTFILE, ">>$output");
print OUTFILE "Mutual Information with Prkcb\nMI score\tProbe ID\n";
while ($line = <INFILE>)
{
  @linearr = split(/\t/, $line);
  $sum=$linearr[2]+$linearr[5]+$linearr[8]+$linearr[11]+$linearr[14]+$linearr[17]+$linearr[20];
  $p1=$linearr[2]/$sum;
  $p2=$linearr[5]/$sum;
  $p3=$linearr[8]/$sum;
  $p4=$linearr[11]/$sum;
  $p5=$linearr[14]/$sum;
  $p6=$linearr[17]/$sum;
  $p7=$linearr[20]/$sum;
  $p1_p1_Prkcb=($linearr[2]+$Prkcb[0])/($sum+$Prkcb_sum);
  $p2_p2_Prkcb=($linearr[5]+$Prkcb[1])/($sum+$Prkcb_sum);
  $p3_p3_Prkcb=($linearr[8]+$Prkcb[2])/($sum+$Prkcb_sum);
  $p4_p4_Prkcb=($linearr[11]+$Prkcb[3])/($sum+$Prkcb_sum);
  $p5_p5_Prkcb=($linearr[14]+$Prkcb[4])/($sum+$Prkcb_sum);
  $p6_p6_Prkcb=($linearr[17]+$Prkcb[5])/($sum+$Prkcb_sum);
  $p7_p7_Prkcb=($linearr[20]+$Prkcb[6])/($sum+$Prkcb_sum);
  $MI=-(($p1_p1_Prkcb*log($p1*$p1_Prkcb/$p1_p1_Prkcb))
       + ($p2_p2_Prkcb*log($p2*$p2_Prkcb/$p2_p2_Prkcb))
       + ($p3_p3_Prkcb*log($p3*$p3_Prkcb/$p3_p3_Prkcb))
       + ($p4_p4_Prkcb*log($p4*$p4_Prkcb/$p4_p4_Prkcb))
       + ($p5_p5_Prkcb*log($p5*$p5_Prkcb/$p5_p5_Prkcb))
       + ($p6_p6_Prkcb*log($p6*$p6_Prkcb/$p6_p6_Prkcb))
       + ($p7_p7_Prkcb*log($p7*$p7_Prkcb/$p7_p7_Prkcb)));
  print OUTFILE "$MI\t$linearr[0]\n";
}
close INFILE;
close OUTFILE;
