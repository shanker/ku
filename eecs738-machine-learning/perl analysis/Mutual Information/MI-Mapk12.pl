#!/usr/local/bin/perl -w
$input = "training_set.txt";
$output = "Mutual_Information_Mapk12.txt";
@Mapk12=(2.200029267,2.073351702,2.232233521,2.982813762,2.901512809,3.053424204,3.007449044);
$Mapk12_sum=$Mapk12[0]+$Mapk12[1]+$Mapk12[2]+$Mapk12[3]+$Mapk12[4]+$Mapk12[5]+$Mapk12[6];
$p1_Mapk12=$Mapk12[0]/$Mapk12_sum;
$p2_Mapk12=$Mapk12[1]/$Mapk12_sum;
$p3_Mapk12=$Mapk12[2]/$Mapk12_sum;
$p4_Mapk12=$Mapk12[3]/$Mapk12_sum;
$p5_Mapk12=$Mapk12[4]/$Mapk12_sum;
$p6_Mapk12=$Mapk12[5]/$Mapk12_sum;
$p7_Mapk12=$Mapk12[6]/$Mapk12_sum;
open (INFILE, $input);
open (OUTFILE, ">>$output");
print OUTFILE "Mutual Information with Mapk12\nMI score\tProbe ID\n";
while ($line = <INFILE>)
{
  @linearr = split(/\t/, $line);
  $sum=$linearr[2]+$linearr[5]+$linearr[8]+$linearr[11]+$linearr[14]+$linearr[17]+$linearr[20];
  $p1=$linearr[2]/$sum;
  $p2=$linearr[5]/$sum;
  $p3=$linearr[8]/$sum;
  $p4=$linearr[11]/$sum;
  $p5=$linearr[14]/$sum;
  $p6=$linearr[17]/$sum;
  $p7=$linearr[20]/$sum;
  $p1_p1_Mapk12=($linearr[2]+$Mapk12[0])/($sum+$Mapk12_sum);
  $p2_p2_Mapk12=($linearr[5]+$Mapk12[1])/($sum+$Mapk12_sum);
  $p3_p3_Mapk12=($linearr[8]+$Mapk12[2])/($sum+$Mapk12_sum);
  $p4_p4_Mapk12=($linearr[11]+$Mapk12[3])/($sum+$Mapk12_sum);
  $p5_p5_Mapk12=($linearr[14]+$Mapk12[4])/($sum+$Mapk12_sum);
  $p6_p6_Mapk12=($linearr[17]+$Mapk12[5])/($sum+$Mapk12_sum);
  $p7_p7_Mapk12=($linearr[20]+$Mapk12[6])/($sum+$Mapk12_sum);
  $MI=-(($p1_p1_Mapk12*log($p1*$p1_Mapk12/$p1_p1_Mapk12))
       + ($p2_p2_Mapk12*log($p2*$p2_Mapk12/$p2_p2_Mapk12))
       + ($p3_p3_Mapk12*log($p3*$p3_Mapk12/$p3_p3_Mapk12))
       + ($p4_p4_Mapk12*log($p4*$p4_Mapk12/$p4_p4_Mapk12))
       + ($p5_p5_Mapk12*log($p5*$p5_Mapk12/$p5_p5_Mapk12))
       + ($p6_p6_Mapk12*log($p6*$p6_Mapk12/$p6_p6_Mapk12))
       + ($p7_p7_Mapk12*log($p7*$p7_Mapk12/$p7_p7_Mapk12)));
  print OUTFILE "$MI\t$linearr[0]\n";
}
close INFILE;
close OUTFILE;
