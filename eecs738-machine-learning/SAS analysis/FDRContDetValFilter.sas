options nonumber nodate;
options nonumber nodate;
title1 'Microarray data analysis';
title2 'Data Filtering';
data FDR;
Z='09'X;
infile 'H:\Microarray data analysis\SAS analysis\ranked_by_cont_det_val.txt' firstobs=2 dlm=Z;
input ScanREF : $60. ACIcont1abs $ ACIcont1val ACIcont1det ACIcont2abs $ ACIcont2val ACIcont2det ACIcont3abs $ ACIcont3val ACIcont3det ACItrt1abs $ ACItrt1val ACItrt1det ACItrt2abs $ ACItrt2val ACItrt2det ACItrt3abs $ ACItrt3val ACItrt3det ACItrt4abs $ ACItrt4val ACItrt4det LogLowessNorm Meancontdet Control_RANK;
FDR=Control_RANK*0.05/2299;
if Meancontdet<FDR then output;
keep ScanREF ACIcont1abs ACIcont1val ACIcont1det ACIcont2abs ACIcont2val ACIcont2det ACIcont3abs ACIcont3val ACIcont3det ACItrt1abs ACItrt1val ACItrt1det ACItrt2abs ACItrt2val ACItrt2det ACItrt3abs ACItrt3val ACItrt3det ACItrt4abs ACItrt4val ACItrt4det LogLowessNorm Meancontdet Control_RANK;
run;
proc print data=FDR;
title 'FDR-filtered genes for average detection value of ACI-control replicates';
run;

