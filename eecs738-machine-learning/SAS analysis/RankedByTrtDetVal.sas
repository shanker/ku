options nonumber nodate;
title1 'Microarray data analysis';
title2 'Data preprocessing';
data FDR;
Z='09'X;
infile 'H:\Microarray data analysis\SAS analysis\FDR_filtered_by_cont_det_val.txt' firstobs=2 dlm=Z;
input ScanREF : $60. ACIcont1abs $ ACIcont1val ACIcont1det ACIcont2abs $ ACIcont2val ACIcont2det ACIcont3abs $ ACIcont3val ACIcont3det ACItrt1abs $ ACItrt1val ACItrt1det ACItrt2abs $ ACItrt2val ACItrt2det ACItrt3abs $ ACItrt3val ACItrt3det ACItrt4abs $ ACItrt4val ACItrt4det LogLowessNorm Meancontdet Control_RANK;
Meantrtdet=(ACItrt1det+ACItrt2det+ACItrt3det+ACItrt4det)/4;
run;
proc rank data=FDR out=ranking descending;
var Meantrtdet;
ranks Trt_RANK;
proc sort data=ranking;
by Trt_RANK;
proc print data=ranking;
var ScanREF ACIcont1abs ACIcont1val ACIcont1det ACIcont2abs ACIcont2val ACIcont2det ACIcont3abs ACIcont3val ACIcont3det ACItrt1abs ACItrt1val ACItrt1det ACItrt2abs ACItrt2val ACItrt2det ACItrt3abs ACItrt3val ACItrt3det ACItrt4abs ACItrt4val ACItrt4det LogLowessNorm Meancontdet Control_RANK Meantrtdet Trt_RANK;
title 'Ranked by average detection value of ACI-DES-treated replicates';
run;


