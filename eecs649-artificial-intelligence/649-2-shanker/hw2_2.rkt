#lang racket

;H. Shanker Rao
;EECS 649 HW-2 problem-2
;KUID 2495689

;TEST-CASES are in listed at the end

;resolution
(define (resolve s c)
  (define (equate-lit x y t)
    (define (equate x y e) (let ((t (unify x y e))) (if t (list t) '())))
    (cond[(and(~? x)(not(~? y))) (equate (cadr x) y t)]
         [(and(~? y)(not(~? x))) (equate x (cadr y) t)]
         [else '()])
    )
  (map (λ (th) (instantiate-clause (append (cdr s) (cdr c)) th)) (equate-lit (car c) (car s) '()))
  )
(define (~? x) (and (pair? x) (eq? (car x)'~)))

;Unification
(define (unify x y e)
  ;occur-check
  (define (occur v x)                                     
    (or (and (var? v) (eq? v x))
        (and (pair? x) (ormap (λ (y) (occur v y)) (cdr x)))
        )
    )
  ;extend-subst
  (define (extend-subst v x e)
    (cons (cons v x) (map (λ (p) (cons (apply-subst (cdr p) (list (cons v x))) (car p))) e))
    )
  (and e (let ([x (apply-subst x e)] [y (apply-subst y e)])
           (cond [(equal? x y) e]
                 [(var? x) (and (not (occur x y)) (extend-subst x y e))]
                 [(var? y) (and (not (occur y x)) (extend-subst y x e))]
                 [else (and (pair? x) (pair? y) (= (length x) (length y)) (equal? (car x) (car y)) (foldl unify e (cdr x) (cdr y)))]
                 )
           )
       )
  )
;apply-subst
(define (apply-subst x e)
  (cond [(var? x) (let ([p (assoc x e)]) (if p (cdr p) x))]
        [(pair? x) (cons (car x) (map (λ (y) (apply-subst y e)) (cdr x)))]
        [else x]
        )
  )
;variable
(define var? symbol?)
(define(instantiate-clause c e)(map(λ(p)(apply-subst p e))c))

"TEST-CASE"
"Resolution"
(resolve '((~(American x)) (~(Weapon y))) '((Criminal x) (~(American x)) (~(Weapon y))))
