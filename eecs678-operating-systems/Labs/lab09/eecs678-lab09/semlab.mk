
MY_LAST_NAME = "rao"

basic:
	./nachos/userprog/nachos -d sem -x ./nachos/test/basic_sem_free > basic.out

queue:
	./nachos/userprog/nachos -d sem -x ./nachos/test/queue_sem_free > queue.out

tar:
	mkdir $(MY_LAST_NAME)
	make clean -C nachos
	cp -r Makefile nachos $(MY_LAST_NAME)
	tar cvzf $(MY_LAST_NAME)-lab09.tar.gz $(MY_LAST_NAME)
	rm -rf $(MY_LAST_NAME)
