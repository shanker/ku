#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/wait.h>

#define R_FILE "/proc/meminfo"
#define GREP_EXEC "/bin/grep"
#define SORT_EXEC "/bin/sort"
#define HEAD_EXEC "/usr/bin/head"
#define BSIZE 256 


int main()
{
    int status;
    pid_t pid_1, pid_2, pid_3;

    int pipe1fd[2];
    int pipe2fd[2];
    pipe (pipe1fd);
    pipe (pipe2fd);

    pid_1 = fork();
    if (pid_1 < 0) {
      fprintf(stderr, "Fork failed\n");
      exit(-1);
    }
    else if (pid_1 == 0) {
        /* grep process */
      close(pipe1fd[0]);
      close(pipe2fd[0]);
      close(pipe2fd[1]);
      dup2(pipe1fd[1], 1);
      close(pipe1fd[1]);
      execl(GREP_EXEC, "grep", "-cr", "Thread", "nachos/", (char*) 0);
            return 0;
    }

    pid_2 = fork();
    if (pid_2 < 0) {
      fprintf(stderr, "Fork failed\n");
      exit(-1);
    }
    else if (pid_2 == 0) {
        /* sort process */
      close(pipe1fd[1]);
      close(pipe2fd[0]);
      dup2(pipe1fd[0], 0);
      dup2(pipe2fd[1], 1);
      close(pipe1fd[0]);
      close(pipe2fd[1]);
      execl(SORT_EXEC, "sort", "-t", ":", "+1.0", "-2.0", "--numeric", "--reverse", (char*) 0);
        return 0;
    }

    pid_3 = fork();
    if (pid_3 < 0) {
      fprintf(stderr, "Fork failed\n");
      exit(-1);
    }
    else if (pid_3 == 0) {
        /* head process */
      close(pipe1fd[0]);
      close(pipe1fd[1]);
      close(pipe2fd[1]);
      dup2(pipe2fd[0], 0);
      close(pipe2fd[0]);
      execl(HEAD_EXEC, "head", "--lines=5", (char*) 0);
        return 0;
    }

    /* shell process */
    /* Remember to close unused pipes! */
    close(pipe1fd[0]);
    close(pipe1fd[1]);
    close(pipe2fd[0]);
    close(pipe2fd[1]);

    close(STDIN_FILENO);
    close(STDOUT_FILENO);

    if ((waitpid(pid_1, &status, 0)) == -1) {
        fprintf(stderr, "Process 1 encountered an error. ERROR%d", errno);
        return EXIT_FAILURE;
    }

    if ((waitpid(pid_2, &status, 0)) == -1) {
        fprintf(stderr, "Process 2 encountered an error. ERROR%d", errno);
        return EXIT_FAILURE;
    }

    if ((waitpid(pid_3, &status, 0)) == -1) {
        fprintf(stderr, "Process 3 encountered an error. ERROR%d", errno);
        return EXIT_FAILURE;
    }
    return 0;
}

