// See thread_ip.h for overview

#pragma implementation "threads/thread_ip.h"

#include "thread_ip.h"


// for NUM_THREAD_STATES, Thread::Get_Priority(), and Thread::getStatus()
#include "thread.h"


/*
 * NOTE: thread.h isn't included by thread_ip.h because that would be
 * a cyclic header dependency
 */

// "fill-out" another forward declaration
#include "histogram.h"

#include "system.h" // for dp_hooks vars


//======================================================================
// BASIC INTERFACE - do not change the names/signatures
//======================================================================

//----------------------------------------------------------------------
// ThreadIP::ThreadIP
//       Simple constructor
//----------------------------------------------------------------------

ThreadIP::ThreadIP(Thread* pcb) : host( pcb )
{
  initializeInformation();
}


ThreadIP::~ThreadIP()
{ 
  cleanUpInformation();
}


//----------------------------------------------------------------------
// ThreadIP::recordInformation
//      This is called when the instrumentation point is to take a
//      sample.

//      This causes the IP to sample state and update the appropriate
//      episode tracker (inactive or active). If the thread is making
//      a transition between those two states, then the IP will record
//      a datum in the appropriate episode duration histogram.
//----------------------------------------------------------------------

void ThreadIP::recordInformation()
{

  updateMetrics();
}

//----------------------------------------------------------------------
// ThreadIP::showDEBUG
//	A possibly handy routine when debugging the IP-centric dynamic priority implementation.
//----------------------------------------------------------------------

void ThreadIP::showDEBUG(char* debug_category)
{
  char* dummy = debug_category; // hush the unused-parm warning
  dummy = dummy;
}

//======================================================================
// INTERFACE EXTENSION - add new public names/signatures here
//======================================================================


void ThreadIP::reportMetricsDEBUG( char* debug_category )
{
  // show some statistics on the DEBUG file descriptor
  DEBUG( (char *)debug_category ,
	 (char *)"Turnaround Time: %u\nWaiting Time: %u\nTime on CPU: %u" ,
	 turnaroundTime , waitingTime , cpuTime
	 );

  // FOR INACTIVITY
  // show extrema if available
  int v;
  if ( histoInactive->Maximum( v ) )
    DEBUG( (char *)debug_category , (char *)"\nMax Inactive Episode: %d" , v );

  if ( histoInactive->Minimum( v ) )
    DEBUG( (char *)debug_category , (char *)"\nMin Inactive Episode: %d" , v );

  // show total count if it did not overflow
  bool count_over = false;
  size_t c = histoInactive->Count( count_over );
  if ( ! count_over )
    {
      DEBUG( (char *)debug_category , (char *)"\nNum Inactive Episodes: %u" , c );
    }

  // show sum value if it did not overflow
  bool sum_over = false;
  v = histoInactive->Sum( sum_over );
  if ( ! sum_over )
    {
      DEBUG( (char *)debug_category , (char *)"\nTotal Inactive Time: %u" , v );
    }

  // show avg value if it is available
  if ( ! ( sum_over || count_over || 0 == c) )
    {
      DEBUG( (char *)debug_category , (char *)"\nMean Inactive Time: %f" , ((double) v) / ((double) c) );
    }


  // FOR ACTIVITY
  // show extrema if available
  if ( histoActive->Maximum( v ) )
    DEBUG( (char *)debug_category , (char *)"\nMax Active Episode: %d" , v );

  if ( histoActive->Minimum( v ) )
    DEBUG( (char *)debug_category , (char *)"\nMin Active Episode: %d" , v );

  // show total count if it did not overflow
  count_over = false;
  c = histoActive->Count( count_over );
  if ( ! count_over )
    {
      DEBUG( (char *)debug_category , (char *)"\nNum Active Episodes: %u" , c );
    }

  // show sum value if it did not overflow
  sum_over = false;
  v = histoActive->Sum( sum_over );
  if ( ! sum_over )
    {
      DEBUG( (char *)debug_category , (char *)"\nTotal Active Time: %u" , v );
    }

  // show avg value if it is available
  if ( ! ( sum_over || count_over || 0 == c ) )
    {
      DEBUG( (char *)debug_category , (char *)"\nMean Active Time: %f" , ((double) v) / ((double) c) );
    }
  
  DEBUG( (char *)debug_category , (char *)"\n" );
}

// called when any thread reaches exit
void ThreadIP::ThreadFinished( Thread* t )
{
  if ( t == host || DebugIsEnabled((char *) "reset_histos" ) )
    { // --> histogram is either finished or about to be cleared

      // because we are taking the last snapshot for this histogram
      // period, add in the most recent episode (which hasn't been
      // included yet)
      if ( 0 != currentInactiveDuration )
	{ // --> process was inactive at the last sample
	  histoInactive->RecordDatum( currentInactiveDuration );
	}
      if ( 0 != currentActiveDuration )
	{ // --> process was active at the last sample
	  histoActive->RecordDatum( currentActiveDuration );
	}
    }

  // show the metrics if this is the end of the host thread's
  // operation
  if ( t == host )
    {
      DEBUG( (char *)"metrics" , (char *)"\n<=== Thread %s metrics upon Exit:\n" , host->GetName() );
      reportMetricsDEBUG( (char *)"metrics" );
      DEBUG( (char *)"metrics" ,(char *) "===>\n" );

      dumpHistos(); // write histograms when host thread finishes
    }

  if ( ! host->HasReachedExit() )
    { // --> no point in continuing write histogram snapshots for a
      // --> thread couldn't have ran since the last snapshot was
      // --> output

      // losing a competitor significantly alters the behavior of this
      // thread, so save snapshots of the histograms at each
      // thread-finished event
      dumpHistos();

      // option for clearing the histogram at each thread-finish; this
      // offers chance to separate the observations of the various
      // environments' effects on the thread's activity
      if ( DebugIsEnabled( (char *)"reset_histos" ) )
	{
	  histoInactive->Reset();
	  histoActive->Reset();
	}
    }
}





//======================================================================
// BEHAVIOR DETAILS - implement functionality here ("behind the scenes")
//======================================================================

//----------------------------------------------------------------------
// ThreadIP::initializeInformation
//----------------------------------------------------------------------

void ThreadIP::initializeInformation()
{ 


  // METRICS
  // begin with empty values
  turnaroundTime = 0;
  waitingTime = 0;

  cpuTime = 0;

  // histograms
  histoInactive = new Histogram( Histogram::HistoN1 , Histogram::HistoWidth1 , Histogram::HistoMin1 );
  histoActive = new Histogram( Histogram::HistoN2 , Histogram::HistoWidth2 , Histogram::HistoMin2 );

  currentInactiveDuration = 0;
  currentActiveDuration = 0;
}

void ThreadIP::cleanUpInformation()
{

  // deallocate histograms
  delete histoInactive;
  delete histoActive;
}





void ThreadIP::updateMetrics()
{
  if ( ! host->HasReachedExit() )
    { // --> process has not finished executing
      ++ turnaroundTime;

      if ( READY == host->getStatus() )
	{ // --> process is ready to run, but has yet to receive the CPU
	  ++ waitingTime;
	}

      if ( RUNNING == host->getStatus() )
	{ // --> process is not inactive

	  ++ cpuTime;

	  ++ currentActiveDuration;

	  if ( 0 != currentInactiveDuration )
	    { // --> process was inactive at the last sample
	      histoInactive->RecordDatum( currentInactiveDuration );
	    }

	  currentInactiveDuration = 0;
	}
      else if ( ZOMBIE != host->getStatus() )
	{ // --> process is meaningfully inactive
	  ++ currentInactiveDuration;

	  if ( 0 != currentActiveDuration )
	    { // --> process was active at the last sample
	      histoActive->RecordDatum( currentActiveDuration );
	    }

	  currentActiveDuration = 0;
	}
    }
}


//----------------------------------------------------------------------
// ThreadIP::dumpHistos
//      Throughout execution, the histograms may be written multiple
//      times. This method accomplishes storing the current state of
//      the histogram to a file.
//----------------------------------------------------------------------

void ThreadIP::dumpHistos()
{
  if ( ! DebugIsEnabled( (char *)"metrics" ) )
    return;

  // build a filename that identifies the thread (via the thread's
  // name)
  char fname[MAXFILENAMELENGTH + 1];
  memset( fname , '\0' , MAXFILENAMELENGTH + 1 );

  strncat( fname , host->GetName() , MAXFILENAMELENGTH - 35 );
  // (" - 35" leaves room for the rest of the string in case the name
  // would have filled the buffer)

  // also add the NachOS's tick at which the file was created, in case
  // the same thread generates multiple files
  strcat( fname , "@tick" );
  sprintf( fname + strlen( fname ) , "%u" , stats->totalTicks );
  strcat( fname , ".inactive.histo" );

  // write the inactivity histogram to the file
  histoInactive->Write( fname , Histogram::OVERWRITE );


  // change the filename from *.inactive.histo to *.active.histo
  fname[ strlen( fname ) - strlen( ".inactive.histo" ) ] = '\0';
  strcat( fname , ".active.histo" );

  // write the activity histogram to the file
  histoActive->Write( fname , Histogram::OVERWRITE );
}



