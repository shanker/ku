// -*- C++ -*-
// thread_ip.h 
//	Data structure for instrumenting thread observations. Each thread's
//	PCB contains an object of this class.
//
//      There is a periodic interrupt that will trigger this class to
//      record instantaneous information about the thread. This
//      information may then be used for various evaluations of the
//      thread (e.g. dynamic priority).
//
// Implemented Mar 2006 by Nicolas Frisby, EECS 678 teaching assistant

#ifndef THREAD_IP_H
#define THREAD_IP_H

#pragma interface "threads/thread_ip.h"

#include "utility.h"
#include "defs.h"

#include "list.h"

// forward declaration; the Threads class is used only as a pointer in
// this class declaration, so the compiler does not yet need the
// details provided by "thread.h"--the implementation of the ThreadIP
// class in "thread_ip.cc" will need to include "thread.h" because it
// needs the interface to the thread class. If "thread.h" were
// included here, it would cause a cyclic dependency ("thread.h"
// includes "thread_ip.h"--this file).
class Thread;

class Histogram;

// The following class defines a thread instrumentation point. It has
// one principal public methods:
//    void recordInformation() 
// Do not change this methods; they are assumed to exist by the rest
// of NachOS.

class ThreadIP
{

 public:
  struct Episode {
    // Per-state credits used to calculate dynamic priority. These
    // counters are updated via the periodic instrumentation interrupt.
    unsigned int Credits[NUM_THREAD_STATES];
  };



//======================================================================
// BASIC INTERFACE - do not change the names/signatures
//======================================================================

 public:
  ThreadIP(Thread* pcb);
  ~ThreadIP();

  // invoked periodically to "sample" the thread state
  void recordInformation();

  // invoked to display a DEBUG string
  void showDEBUG(char* debug_category);

 private:
  // invoked upon construction to initialize the instrumentation information
  void initializeInformation();

  // invoked upon destruction to cleanup the instrumentation information
  void cleanUpInformation();

  // constant pointer to a non-constant Thread, i.e. ok to change the
  // thread actually referenced, but not ok to change which Thread is
  // being referenced
  Thread *const host;

//======================================================================
// INTERFACE EXTENSION - add new public names/signatures here
//======================================================================
  
 public:


  // prints metrics to the DEBUG file descriptor and records
  // histograms in files
  void reportMetricsDEBUG( char* debug_category );

  // called when any thread reaches exit
  void ThreadFinished( Thread* t );
  

//======================================================================
// BEHAVIOR DETAILS - implement functionality here ("behind the scenes")
//======================================================================

 private:


  // per-thread scheduling metrics
  // (from pg. 156 of "Operating System Concepts" 6th edition,
  // Silberschatz, Galvin, Gagne)
  unsigned int turnaroundTime;
  unsigned int waitingTime;

  unsigned int cpuTime;

  // response time is difficult to establish (without deeper hooks
  // into the console), so we will instead track inactivity as a third
  // metric
  // ( inactive == not running )
  Histogram *histoInactive;
  unsigned int currentInactiveDuration;

  // similar means to track activity
  Histogram *histoActive;
  unsigned int currentActiveDuration; // (possible optimization: could
				      // be combined with inactive
				      // duration as a signed
				      // quantity)

  void updateMetrics();
  void dumpHistos();

};

#endif // THREAD_IP_H

