/*
 * Copyright 1998, 1999, 2000 Xpeed, Inc.
 * xpds-fsm.h, $Revision: 1.2 $
 * License to copy and distribute is GNU General Public License, version 2.
 */
#ifndef XPDS_FSM_H
#define XPDS_FSM_H 1

int xpds_fsm_nt (int card_num, int channels, int guard_time);
int xpds_fsm_lt (int card_num, int channels, int guard_time);

#endif
