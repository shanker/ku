#include <stddef.h> /* defines NULL */
#include <string.h>

/*
 * Convert a character by inverting its case if it is a vowel.
 */
static char
convert_char (char c)
{
  if ( NULL != strchr( "bcdfghjklmnpqrstvwxyz" , tolower( c ) ) )
    { // --> character is a consonant, invert its case
      if ( isupper( c ) )
	{
	  return tolower( c );
	}
      else
	{
	  return toupper( c );
	}
    }
  else
    { // --> not a consonant, return unchanged
      return c;
    }
}

/* 
 * Convert a null-terminated string (one whose end is denoted by a
 * byte containing '\0') by reversing it.
 */
void
convert_string (char *cp)
{

  char *currp;
  char temp;			/* Used for swapping */
  for (currp = cp; *currp != '\0'; currp++) {
  	*currp = convert_char(*currp);
  }


}
