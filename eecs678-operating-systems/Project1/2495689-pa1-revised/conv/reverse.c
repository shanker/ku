#include <stddef.h> /* defines NULL */

/*
 * Convert a character by inverting its case if it is a vowel.
 */
static char
convert_char (char c)
{
  if ( NULL != strchr( "aeiou" , tolower( c ) ) )
    { // --> character is a vowel, invert its case
      if ( isupper( c ) )
	{
	  return tolower( c );
	}
      else
	{
	  return toupper( c );
	}
    }
  else
    { // --> not a vowel, return unchanged
      return c;
    }
}

/* 
 * Convert a null-terminated string (one whose end is denoted by a
 * byte containing '\0') by reversing it.
 */
void
convert_string (char *cp)
{
  unsigned int first, last;	/* Indexes of the next chars to be swapped */
  char temp;			/* Used for swapping */
  
  first = 0U, last = strlen(cp) - 1U;

  /* Don't transform the trailing newline */
  if ( '\n' == cp[last] && 0 < last ) --last;

  /* Move the two indexes inward from the string ends until they meet
     in the middle. */
  for (;
       first < last;
       first++, last--)
    {
      /* Swap cp[first] and cp[last] */
      temp = cp[first];
      cp[first] = cp[last];
      cp[last] = temp;

      /* Invert the case of vowels */
      cp[first] = convert_char ( cp[first] );
      cp[last] = convert_char ( cp[last] );
    }
}
