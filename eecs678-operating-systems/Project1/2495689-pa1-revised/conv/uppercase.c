/*
 * Convert a null-terminated sting (one whose end is denoted by a byte
 * containing '\0') to all upper case letters by starting at the
 * beginning and going until the null byte is found.
 */
void
convert_string (char *cp)
{
  char *currp;	/* pointer to current position in the input string */
  int   c;      /* an individual character is of type int for obscure
		   reasons */

  /*
   * Loop over the string pointed to by cp, examining each character in turn
   * until the null terminator is reached.  In the body of the loop, we
   * uppercase each character.  We could use cp itself, but we leave it
   * undisturbed as a debugging aid, since one can always print the
   * current string under GDB using "p *cp".  After each loop, we increment
   * currp, which takes the byte size of the character into account.
   */
  for (currp = cp; *currp != '\0'; currp++) {
    /*
     * The current pointer is not null, so convert the character pointed to
     * by currp to upper case and store it in the variable c.  The toupper()
     * function is declared to return int for obscure reasons, hence the
     * variable c.  Then we assign the result back to where it started,
     * casting to type "char" to keep the compiler happy.
     */
    c = toupper (*currp);
    *currp = (char) c;
  }
}
