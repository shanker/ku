/*
 * Simple subtition of numbers for given characters.
 */

 void
 convert_string (char *message)
 {
 	char *mP;
	for (mP = message; *mP != '\0'; mP++) {
		switch (*mP) {
			case 'e':
				*mP = '3';
				break;
			case 'l':
				*mP = '1';
				break;
			case 't':
				*mP = '7';
				break;
			case 'T':
				*mP = '7';
				break;
			default:
				break;
		}
	}
}

