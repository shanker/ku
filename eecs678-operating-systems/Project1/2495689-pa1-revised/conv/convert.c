/*
 * $Id: convert.c,v 1.1 2009/08/28 14:11:43 mjantz Exp $
 * 
 * A simple program reading standard in and writing to standard out.
 *
 * The reading and echoing loop terminates when a string starting
 * with the four characters "quit" is input.
 */

/*
 *
 * These defined constants control how various options are 
 * compiled into the program.
 *
 * The GIVE_PROMPT constant will make the program interactive
 * and make it easier to use from the command line, but is not
 * good for use in the child program.
 *
 * The FILE_POINTER_OUTPUT constant, when defined, makes the
 * output of the program go through the file pointer "stdout"
 * which must be flushed to avoid buffering in the process
 * address space.  When it is not defined, the Standard Out 
 * file descriptor, number 1, will be used with the regular 
 * write system call.  This approach is fine, but to avoid 
 * junk beyond the end of the string being sent to the
 * Standard Out file descriptor, we find the length of 
 * the string and wrote out only that many characters.
 *
 * To turn these off, just comment them out.
 */
/* #define GIVE_PROMPT 1 */
#define FILE_POINTER_OUTPUT 1

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>


#include "util.h"   /* defines BUFFER_SIZE */

/* Make GCC shut up about argc being unused */
#ifdef __GNUC__
#define UNUSED __attribute__((unused))
#else
#define UNUSED
#endif

/* Make GCC shut up about argv being unused when !GIVE_PROMPT */
#if defined(__GNUC__) && !defined(GIVE_PROMPT)
#define PROMPT_UNUSED UNUSED
#else
#define PROMPT_UNUSED
#endif

/*
 * The actual string conversion routine will be determined when
 * linking
 */
extern
void
convert_string (char *cp);

int
main(/*@unused@*/ int argc UNUSED, char *argv[] PROMPT_UNUSED) {
  char string_buffer[BUFFER_SIZE];

  /*
   * Keep looping until we determine it is time to stop by breaking out
   * of the loop.
   */
  for (;;) {
#ifdef GIVE_PROMPT
    /*
     * Give the prompt. The "fflush" ensures you see the prompt before
     * having to type. Without it the program can be waiting for your
     * input without the user seeing the prompt.
     */
    fprintf (stdout, "input> ");
    if (fflush (stdout) != 0) {
      fprintf (stderr, "\nError flushing stdout, ERROR#%d\n", errno);
      perror (argv[0]);
      return EXIT_FAILURE;
    }

#endif

    /*
     * The first argument to fgets is the address where it should put the
     * characters it reads.  The name of an array is the address of its first
     * element (i.e., the element with index zero).
     */
    if (fgets (string_buffer, BUFFER_SIZE, stdin) == NULL) {
      /* This probably means end-of-file (EOF).  In any case, we won't be
	 getting any more input from stdin, so go ahead and exit. */
      break;
    }

#ifdef GIVE_PROMPT
    /*
     * Look to see if the input is the string "quit".
     */
    if (strcmp (string_buffer, "quit\n") == 0)
      {
	break;
      }
#endif

    /*
     * Convert the string 
     */
    convert_string (string_buffer);

    /*
     * Write the string to standard out.
     * using either the file pointer, which should
     * be flushed, or the file descriptor, which 
     * means we need to know the length of the string
     */ 
#ifdef FILE_POINTER_OUTPUT
    fprintf (stdout, "%s", string_buffer);
    if (fflush (stdout) != 0) {
      fprintf (stderr, "\nError flushing stdout, ERROR#%d\n", errno);
      perror (argv[0]);
      return EXIT_FAILURE;
    }
#else
    /*
     * Find the length of the string, and then print it out
     * to the standard out file descriptor, witout using 
     * the file pointer interface to avoid buffering problems.
     */
    if (write (1, string_buffer, strlen (string_buffer)) < 0) {
      fprintf (stderr, "\nError writing to stdout, ERROR#%d\n", errno);
      perror (argv[0]);
      return EXIT_FAILURE;
    }
#endif
  }

  return EXIT_SUCCESS;
}
