/*
 * $Id: util.h,v 1.2 2007/02/04 18:59:46 jstaley Exp $
 *
 * This is a simple header file because it describes
 * the external interface of a simple set of utility
 * routines. It illustrates an important principle, 
 * however, which is that good programming practice
 * requires well structured interfaces. In this case
 * that means we place routines used by more than
 * one program into a separate source file, and 
 * publish the interface in a header file.
 */

#include <stdint.h> /* defines uint32_t */

extern uint32_t str2IpAddr(const char *anIpName);

/* This is the size of a communication buffer to be used to talk between the
   client and the server. */
#define BUFFER_SIZE 256

/*
 * This is the maximum size of a string holding a hostname, in bytes.
 */
#ifndef HOST_NAME_MAX
/* SUSv2 says that hostnames are at most 255 bytes plus the trailing 0 */
#define HOSTNAME_SIZE 256
#else /* POSIX */
/* POSIX lets the system designers specify the maximum hostname length */
#define HOSTNAME_SIZE (HOST_NAME_MAX + 1)
#endif

/*
 * Define debugging macros that expand to nothing when you don't want to
 * debug.
 *
 * This would be a lot easier to do with GCC's variadic macros, but then we
 * could not compile the code with any other compiler.
 */
#ifdef DEBUG
#define DEBUG0(str)                   fputs(str, stderr)
#define DEBUG1(str, arg1)             fprintf(stderr, str, arg1)
#define DEBUG2(str, arg1, arg2)       fprintf(stderr, str, arg1, arg2)
#define DEBUG3(str, arg1, arg2, arg3) fprintf(stderr, str, arg1, arg2, arg3)
#else
#define DEBUG0(str)
#define DEBUG1(str, arg1)
#define DEBUG2(str, arg1, arg2)
#define DEBUG3(str, arg1, arg2, arg3)
#endif
