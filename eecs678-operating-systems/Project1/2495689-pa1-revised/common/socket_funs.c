/*
 * These include files provide the definitions required to use the
 * standard IO functions, conversion functions, status constants, and
 * error messages.
 */
#include <stdio.h>		/* (f)printf() and friends */
#include <stdlib.h>		/* atoi(), EXIT_* */
#include <unistd.h>		/* read() and write() syscalls */
#include <string.h>		/* memset() */
#include <errno.h>		/* errno, what else? */

/*
 * These include files provide the definitions required to use
 * sockets. The first provides various variable and structure type
 * definitions used in the subsequent files.
 */
#include <sys/types.h>
#include <sys/socket.h>		/* Generic socket definitions */
#include <netinet/in.h>		/* IP-related definitions */

/*
 * We use the utility routines defined in util.c so include the simple
 * header file util.h describing the interfaces of the routines
 * provided in the file.
 * 
 * Note that the use of double quotes on the include line tells the
 * compiler to look in the local directory.
 */
#include "util.h"

int
createSocket( int *socketFd )
{
  *socketFd = -1;

  /*
   * Create the socket, catching an error return value less than
   * zero. If there is an error we print out the information about it
   * offered by the system.
   */
  *socketFd = socket(PF_INET, SOCK_STREAM, 0);

  if ( *socketFd < 0 )
    {
      fprintf(stderr, "\nError Opening Socket, ERROR#%d\n", errno);
      return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}

int
bindSocket( int socketFd , int portNumber )
{
  int syscall_result = 0;
  struct sockaddr_in ownAddress;
  char hostname[HOSTNAME_SIZE];

  /*
   * Zero out the socket structure in preparation for creation
   */
  memset(&ownAddress, 0, sizeof(ownAddress));

  /*
   * Find the name of our host, convert it to an integer on this
   * machine, and then convert it to an integer in network format.
   */
  if (gethostname(hostname, HOSTNAME_SIZE) < 0)
    {
      fprintf(stderr, "\nError determining hostname, ERROR#%d\n", errno);
      return EXIT_FAILURE;
    }

  /*
   * Select the network socket family, using the magic defined
   * constant "AF_INET". Also set the port value for this socket to a
   * short value of zero in the "network format". This almost
   * certainly sets it to exactly what it already contains, but this
   * adds robustness to our code.
   */
  ownAddress.sin_family = AF_INET;
  ownAddress.sin_port = htons( portNumber );
  ownAddress.sin_addr.s_addr = htonl(str2IpAddr(hostname));

  syscall_result = bind( socketFd
			 , (struct sockaddr *)&ownAddress
			 , sizeof(ownAddress)
			 );
  if ( syscall_result < 0)
    {
      fprintf(stderr, "\nError Binding Socket, ERROR#%d\n", errno);
      return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}

int
connectSocket( int socketFd , const char* serverName , int serverPort )
{
  int syscall_result = 0;
  struct sockaddr_in peerAddress;

  memset(&peerAddress, 0, sizeof(peerAddress));

  /*
   * Now we prepare the peerAddress structure for use as an argument
   * for our request to connect to the server
   *
   * First, convert the name of the server machine in string form to
   * an IP address in local integer form, and then into network
   * integer format. Then fill in the server's port number and the
   * AF_INET network type. Note that this port number MUST match that
   * used by the server to create its socket, or the connection cannot
   * be made.
   */
  peerAddress.sin_addr.s_addr = htonl(str2IpAddr(serverName));
  peerAddress.sin_port = htons(serverPort);
  peerAddress.sin_family = AF_INET;

  /*
   * This makes the request for the connection to the server
   * whose machine IP address and port number were written
   * into the peerAddress structure by the preceding few lines.
   */
  syscall_result = connect( socketFd
			    , (struct sockaddr *) &peerAddress
			    , (socklen_t) sizeof(peerAddress)
			    );
  if ( syscall_result < 0 )
    {
      fprintf(stderr, "\nError Connecting Sockets, ERROR#%d\n", errno);
      return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}

int
prepareHandshakeSocket( int socketFd , int queueSize , int reusePort )
{
  int syscall_result = 0;
  /*
   * The Listen call establishes how many pending requests 
   * can be waiting for attention. If the queue is full and 
   * another request for a connection arrives, then the 
   * client's "conect" call will fail. This is an important
   * reason to always check return values of system calls.
   */
  if ((listen (socketFd, queueSize)) < 0)
    {
      printf ("\nError Listening for Socket, ERROR#%d\n", errno);
      return EXIT_FAILURE;
    }

  /*
   * The setsockopt call sets various socket options.  This
   * one says that the user should be allowed to reuse a port
   * number right away after a server exit.  Otherwise, you
   * have to wait for up to a minute before the same port
   * number can be reused.
   */
  syscall_result = setsockopt( socketFd
			       , SOL_SOCKET, SO_REUSEADDR
			       , &reusePort, sizeof (reusePort)
			       );
  if ( syscall_result < 0)
    {
      printf ("\nError setting SO_REUSEADDR, ERROR#%d\n", errno);
      return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}

int
acceptOnSocket( int handshakeSocket , int *sessionSocket )
{
  /*
   * Here we accept the next pending request. Note that the server
   * blocks in this call if there are no pending requests. Also, note
   * that the NULLs in the call below correspond to a socket address
   * structure and a length, which would be filled in--if the pointers
   * were not NULL--with the coordinates of the client whose request
   * we have accepted when the "accept" returns. In this application,
   * we are not interested in those coordinates.
   */
  *sessionSocket = accept( handshakeSocket , NULL , NULL );
  if ( *sessionSocket < 0)
    {
      printf ("\nError Accepting Socket, ERROR#%d\n", errno);
      return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}


int
closeSocket( int socketFd )
{
  /*
   * It is hard to see how anything would go wrong
   * with this, but if it did we should know about it.
   * This is why it is good practice to check the return
   * value of EVERY system call.
   */
  if ( close(socketFd) < 0)
    {
      printf("\nError Closing Socket, ERROR#%d\n", errno);
      return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}
