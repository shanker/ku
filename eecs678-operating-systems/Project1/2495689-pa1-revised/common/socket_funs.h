#ifndef SOCKET_FUNS
#define SOCKET_FUNS

/*
 * This file prototypes 7 socket functions. Each function is a very
 * thin wrapper around a standard system call to "do the dirty work."
 */

int
createSocket( int *socketFd );

int
bindSocket( int socketFd , int portNumber );

int
connectSocket( int socketFd , const char* serverName , int serverPort );

int
prepareHandshakeSocket( int socketFd , int queueSize , int reusePort );

int
acceptOnSocket( int handshakeSocket , int *sessionSocket );

int
closeSocket( int socketFd );

#endif /* SOCKET_FUNS */
