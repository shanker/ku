#ifndef PA1_IO_MODULE
#define PA1_IO_MODULE

#include <stdio.h>

#include "util.h"   /* defines BUFFER_SIZE */

/*
 * We will be wrapping up strings during transit from the client to
 * the server--this is simply to avoid obfuscating the use of the
 * sockets with the overhead of managing variable-sized messages at
 * the application layer.
 *
 * These fixed-size messages are the "chunks." Each chunk can hold at
 * most CHUNK_MAX data characters.
 *
 * A "bad chunk" is one with a negative size.
 */

#define CHUNK_BAD (-1)

struct DataChunk_struct
{
  int size;
  char data[BUFFER_SIZE];
};

typedef struct DataChunk_struct DataChunk;


/*
 * We provide IO routines for reading and writing chunks to files.
 *
 * makeChunk builds a chunk from a null-terminated string.
 * readLineAsChunk reads a line from the file.
 * writeChunk writes a chunk to the file.
 */
void
makeChunk( char* s , DataChunk *buffer );

void
readLineAsChunk( int fd , DataChunk *buffer );

/*
 * writeChunk returns EXIT_SUCCESS if the chunk was written
 * successfully, EXIT_FAILURE otherwise
 */
int
writeChunk( int fd , DataChunk *buffer );


#endif /* PA1_IO_MODULE */
