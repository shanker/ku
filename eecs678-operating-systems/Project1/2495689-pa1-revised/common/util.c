/*
 * $Id: util.c,v 1.2 2008/02/27 03:48:30 jstaley Exp $
 *
 * These are some simple utility routines that are used by both
 * the example client and the example server programs. Placing them
 * in a separate file in this way illustrates an important point 
 * about software development - it is  particularly important 
 * to place routines used by multiple programs in a separate file
 * and to LINK it into each program. 
 *
 * It can be tempting to place code for routines such as these
 * in "include" files, but this is not a good method, because
 * it greatly obscures where the code is coming from. The short
 * answer is that it is an amateur technique, though in its 
 * form it causes no real harm. However, getting used to creating 
 * utility routines this way is much better preparation for 
 * learning to build utility libraries (archives, or ".a" files, as
 * well as shared objects, or ".so" files).
 */

/*
 * These #include lines provide the structure definitions and 
 * other information required to successfully compile this 
 * source file.  They provide the definitions required to 
 * do name lookups on IP networks.
 *
 * These file path names are all relative to /usr/include.
 * So, when you see a structure defined or used that you 
 * don't already understand, you can search for it in the 
 * files in that directory, or those under it.
 *
 * The command
 *       grep -R hostent /usr/include
 * for example, searches all the files in /usr/include and
 * its subdirectories for lines containing the string "hostent".
 * These, properly examined, can tell you a lot about what
 * the structure hostent is for, and will also show you the 
 * definition. It shows, for example, that the hostent
 * structure is defined in "netdb.h" but also used in "tcpd.h".
 */
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>

/*
 * This routine converts a host name in string form
 * to the numeric IP address. This is useful when
 * translating from machine addresses in human terms,
 * such as "socrates.ittc.ku.edu", to addresses in
 * machine terms, such as "129.237.125.122".
 */
uint32_t
str2IpAddr (const char *anIpName) 
{
  struct hostent *hostEntry;
  struct in_addr *scratch;

  /*
   * Fetch the host entry from the OS's data about hosts
   * using the host name as a search key
   */
  if ((hostEntry = gethostbyname (anIpName)) == NULL) {
    return 0;
  }

  /*
   * Copy the host address portion of this into
   * a scratch variable, convert this into a long in
   * "network" format, and then return that value from
   * this function call.
   */
  scratch = (struct in_addr *) hostEntry->h_addr;
  return (uint32_t) ntohl (scratch->s_addr);
}
