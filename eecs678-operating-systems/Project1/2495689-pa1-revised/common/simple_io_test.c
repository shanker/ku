#include "simple_io.h"

#include <stdio.h>

/*
 * This really simple program uses the simple IO package to copy stdin
 * to stdout while adding line number and length information
 */

int
main( int argc , char* argv[] )
{
  DataChunk a;
  int ln = 0;

  readLineAsChunk( 0 , & a );

  for ( ln = 1 ;
	CHUNK_BAD != a.size ;
	++ ln, readLineAsChunk( 0 , & a) )
    {
      printf( "%05d [%05d]: " , ln , a.size );
      fflush( stdout );
      writeChunk( 1 , & a);
    }

  return 0;
}
