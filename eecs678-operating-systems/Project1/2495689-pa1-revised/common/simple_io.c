#include "simple_io.h"

#include <stdlib.h> /* EXIT_* */
#include <unistd.h> /* includes the IO system calls */
#include <string.h> /* memset, memchr */


void
makeChunk( char* s , DataChunk *buffer )
{
  size_t len = 0;

  len = strlen( s );

  if ( len > sizeof( buffer->data ) )
    {
      buffer->size = CHUNK_BAD;
    }
  else
    {
      buffer->size = len;
      strncpy( buffer->data , s , len );
    }
}


void
readLineAsChunk( int fd , DataChunk *buffer )
{
  char *newline = NULL;
  int count = 1;
  int sum = 0;

  /* clear the destination chunk */
  buffer->size = CHUNK_BAD;
  memset( buffer->data , 0 , sizeof( buffer->data ) );

  /* read from the file into the chunk */
  for ( ; 0 < count
	  && sizeof( buffer->data ) > sum
	  && NULL == newline ;
	++sum
	)
    {
      count = read( fd , buffer->data + sum , 1 );

      if ( 1 == count
	   && '\n' == buffer->data[sum]
	   )
	{
	  newline = buffer->data + sum;
	}
    }

  /* was it a full line? */
  if ( 0 < count && NULL != newline )
    {
      buffer->size = sum;
    }
}

int
writeChunk( int fd , DataChunk *buffer )
{
  int len = 0;
  int ret = EXIT_FAILURE;

  /* is it a bad chunk? */
  if ( 0 > buffer->size )
    { /* --> bad chunk, report and fail */
      write( fd , "<BAD CHUNK>\n", 12 );
    }
  else
    { /* --> good chunk */
      len = write( fd , buffer->data , buffer->size );

      /* successful if entire chunk was output */
      if ( len == buffer->size )
	{
	  ret = EXIT_SUCCESS;
	}
    }

  return ret;
}
