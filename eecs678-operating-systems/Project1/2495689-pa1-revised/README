#
# $Id: README,v 1.6 2009/08/28 14:11:43 mjantz Exp $
#

==========================
INTRODUCTION
==========================

Thank you for reading the README file! That will always be a helpful
first step.

This project is an inter-process communication (IPC) puzzle. An
understanding of sockets, pipes, child processes, and file descriptors
along their related system calls is a prerequisite for solving this
puzzle. All of these topics have been discussed in lecture--now it's
time to put them to use.

In order to demonstrate the server/client usage of sockets, sample
code has been provided for a *very* simple server client
application. server.c and client.c are discussed below. The code
in these files is a good place to start when solving the puzzle;
however, the structure of the solution will be significantly different
due to the increased sophistication of the interaction (dealing with
an entire file with a variable number of lines instead of just one
message). So: Start with a copy of the example code, but get ready to
write some fresh lines.


==========================
SAMPLE CODE TOUR
==========================

The following steps will lead you through building this program. They
should work without error on any EECS Linux machine (e.g. Eno,
Wozniak, and the Self Commons and labs).



-------------
ONE
-------------
Read the Makefiles first.

Note that the conversion program (convert) is built from the template
convert.c and specific conversion function is provided by whichever name
(CONVFILE) is selected in the starter-code/conv/Makefile


-------------
TWO
-------------
To compile these example programs (in the you've untarred) do:

	bash>make

Notice the set of commands executed to compile everything.

-------------
THREE
-------------
Check out the conversion program convert. You can run it by:

	bash>cd conv
	bash>./convert < input_file > output_file

The < and > are shell redirection operators. The contents of
input_file are passed to convert as standard input (analogous to console
input). The standard output of convert (console output) is sent to
output_file instead of the console. This is the manner in which the
server will rely on the conversion program; that is, the server will
provide standard input to the conversion program and capture its
standard output.

Within the source code starter-code/conv/convert.c, there is a preprocessor
directive that defines a constant:

	/* #define GIVE_PROMPT 1 */

Uncommenting it and recompiling makes the convert program give a prompt
at which you type a line of text of modest length.  It is printed back
to you after being transformed

Note that for the project solution you will have to use a compilation
of convert that does not provide a prompt, so be sure to comment out the
constant definition. This will make the compiler ignore the code in
the "#ifdef GIVE_PROMPT" block.  Try compiling "convert" both ways.

You can always rebuild after changing the "convert.c" source file by (in
the starter-code/conv directory):

	bash> make 

or

	bash> make convert

Examples with and without the prompt are below, but note that I have a
long shell prompt string, which ends with "$".

Example "convert" use with prompt (where CONVFILE in
starter-code/conv/Makefile happens to be "reverse". Note that in the
code you are given the choice of CONVFILE might be different):
 
[niehaus@localhost starter-code/conv]$ ./convert
input> The first string.
.gnIrts tsrIf EhT
input> And the second.
.dnOcEs Eht dna
input> Third is longer but says little
ElttIl syAs tUb rEgnOl sI drIhT
input> quit
[niehaus@localhost starter-code/conv]$

Example "convert" use without prompt (standard input and output defaults
to the console):

[niehaus@localhost starter-code/conv]$ ./convert
The first string.
.gnIrts tsrIf EhT
And the second.
.dnOcEs Eht dna
Third is longer but says little
ElttIl syAs tUb rEgnOl sI drIhT
quit
tIUq
[niehaus@localhost starter-code/conv]$

Note that "convert" understands "quit" when GIVE_PROMPT is defined. When
GIVE_PROMPT is not defined, "convert" treats "quit" like any other
string--another reason to be remember that GIVE_PROMPT should not be
defined for the final solution attempt.

Another way to look at this assignment is that the whole of PA1 is a
puzzle requiring you to take the part that reads the original strings
and put it in the client code, while taking the conversion code and
putting it in the child process of the server.


-------------
FOUR
-------------
To run the client and server programs using sockets (in the
starter-code/example-code directory):

	bash> ./server (port#) &

	bash> ./client (hostname of server) (port# of server)

Note that you can get the name of the machine you are on using the
command:

	bash> hostname 

A particularly sneaky way to use this, when the client and server on
on the same machine, takes advantage of a feature of many *nix command
shells that will substitute the output of a command contained in
"backquotes". The backquote character, the one that is generally in
the upper left key on the keyboard, next to the "1".

	bash>  ./client `hostname` (port# of server)

An explanation of the files:

	starter-code/example-code
		server.c: Code for the server example

		client.c: Code for the client example

	starter-code/conv
		convert.c: Code for converting input lines read from standard
			input to transformed lines written on standard output.  Note that the
			Makefile uses this to create the "convert" executable. Also note that
			there is a hole--"convert_string" is used but not defined.

		{reverse,uppercase}.c: Code for filling in the hole in convert.c. Each defines
			"convert_string" as a different transformation.
	
	starter-code/common
		Contains re-usable packages. YOU ARE ENCOURAGED TO
		RELY ON THESE FOR YOUR SOLUTION. See
		starter-code/common/README and note these files'
		usage in the example client and server

To really see what is going one, run the client and server under GDB
in two different terminal windows. Using GDB with XEmacs is very powerful.
See the class website for notes on running GDB with XEmacs.

A note about the provided code files: It's definitely not the most
modular or organized way to achieve the task. The example code is
designed to make every step to socket usage very obvious and easy to
understand (hence the big comments). Once these steps are understood,
a focused programmer would likely organize the code differently.
