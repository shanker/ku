#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/time.h>
#include <time.h>
#include "socket_funs.h"
#include "util.h"
#include "simple_io.h"
#define STDERR 2

int main (int argc, char *argv[]) {
  char *progName;
  char *serverName;
  int serverPort;
  int sessionSocket;
  DataChunk buffer;
  buffer.size = 0;
  int rcvd = 0;
  progName = argv[0];
  if (argc != 3) {
    fprintf (stderr, "%s takes two arguments!!!\n", progName);
    fprintf (stderr, "%s: Usage = %s SERVER_IP_HOSTNAME SERVER_PORT#\n", progName, progName);
    fprintf (stderr, "...Exiting\n");
    return EXIT_FAILURE;
  }
  serverName = argv[1];
  serverPort = atoi(argv[2]);
  DEBUG0 ("Creating Client Socket... ");
  if ( EXIT_FAILURE == createSocket( &sessionSocket ) ) {
    perror( progName );
    return EXIT_FAILURE;
  }
  DEBUG0 ("Created\n");
#ifdef OPTIONAL_BIND
  DEBUG0 ("Binding Socket... ");
  if ( EXIT_FAILURE == bindSocket( sessionSocket , 0 ) ) {
    perror( progName );
    return EXIT_FAILURE;
  }
  DEBUG0 ("Bound\n");
#endif /* OPTIONAL_BIND */
  DEBUG1 ("Socket Connecting on Port #%d... ", serverPort);
  if ( EXIT_FAILURE == connectSocket( sessionSocket , serverName , serverPort ) ) {
    perror( progName );
    return EXIT_FAILURE;
  }
  DEBUG0 ("Connected\n");
  DEBUG0 ("Client Socket Reading in Data... ");

//STAGE 1 CODE begin  
/*
  struct timeval tp;
  struct timezone tzp;
  struct tm tmStruct; 
  char buffer_for_seconds[30];
  char buffer_for_time[30];
  time_t main_time;
  main_time = tp.tv_sec;
  strftime(buffer_for_time, sizeof (buffer_for_time), "\n%T\n\n", localtime(&main_time));
  strftime(buffer_for_seconds, sizeof (buffer_for_seconds), "\n%S\n\n", localtime(&main_time));  
  makeChunk(buffer_for_seconds, &buffer);
  DEBUG0 ("\nClient seconds from gettimeofday(): ");
  writeChunk(1, &buffer);
  makeChunk(buffer_for_time, &buffer);
  writeChunk(sessionSocket, &buffer);
  readLineAsChunk (sessionSocket, &buffer); 
  strptime(buffer.data, "%T\n", &tmStruct);
  strftime(buffer_for_seconds, 30, "%S\n", &tmStruct);
  makeChunk(buffer_for_seconds, &buffer);
  DEBUG0 ("Time in seconds from server... ");
  writeChunk( 1 , & buffer ); // write to the stdout 
*/
//STAGE 1 CODE ends
  DataChunk a;
  int ln;
  int retval;
  readLineAsChunk(0, &a);
  for (ln = 1; CHUNK_BAD != a.size; ++ ln, readLineAsChunk(0, &a)) {
    retval = writeChunk(sessionSocket, &a);
    readLineAsChunk(sessionSocket, &a);
    writeChunk(1, &a);
  }
  DEBUG0 ("Closing Client Socket... ");
  if (EXIT_FAILURE == closeSocket(sessionSocket)) {
    perror(progName);
    return EXIT_FAILURE;
  }
  DEBUG0 ("Closed\n");
  return EXIT_SUCCESS;
}
