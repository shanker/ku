#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/time.h>
#include <time.h>
#include "socket_funs.h"
#include "util.h"
#include "simple_io.h"
#define STDERR 2
#define CONV_EXEC "conv/convert"
int main (int argc, char *argv[]) {
  char *progName;
  int serverPort;
  int serverSocket;
  int sessionSocket;
  DataChunk buffer;
  char buffer_for_time[30];
  struct timeval tp;
  struct timezone tzp;
  time_t main_time;
  struct tm tmStruct;
  char msg[] = "This message is being sent via Socket!";
  int wrote = 0;
  progName = argv[0];
  if (argc != 2) {
    printf ("%s takes one argument!!!\n", progName);
    printf ("%s: Usage = %s SERVER_PORT#\n", progName, progName);
    printf ("...Exiting\n");
    return EXIT_FAILURE;
  }
  serverPort = atoi (argv[1]);
  DEBUG0 ("Creating Server Socket... ");
  if ( EXIT_FAILURE == createSocket( &serverSocket ) ) {
    perror( progName );
    return EXIT_FAILURE;
  }
  DEBUG0 ("Created\n");
  DEBUG0 ("Binding Server Socket... ");
  if ( EXIT_FAILURE == bindSocket( serverSocket , serverPort ) ) {
    perror( progName );
    return EXIT_FAILURE;
  }
  DEBUG0 ("Bound\n");
  DEBUG1 ("Server Socket Ready to Listen on Port #%d... ", serverPort);
  if ( EXIT_FAILURE == prepareHandshakeSocket( serverSocket , 5 , 1 ) ) {
    perror( progName );
    return EXIT_FAILURE;
  }
  DEBUG0 ("Listening and Accepting\n");
  if ( EXIT_FAILURE == acceptOnSocket( serverSocket , &sessionSocket ) ) {
    perror( progName );
    return EXIT_FAILURE;
  }
  DEBUG0 ("Accepted\n");
  DEBUG0 ("Server Closing Server Socket... ");
  if ( EXIT_FAILURE == closeSocket( serverSocket ) ) {
    perror( progName );
    return EXIT_FAILURE;
  }
  DEBUG0 ("Closed\n");
//STAGE 1 CODE begin
/*
  int i;
  for(i=0; i<1000000000; i++) {  }
  gettimeofday(&tp, &tzp);
  main_time = tp.tv_sec;
  strftime(buffer_for_time, sizeof (buffer_for_time), "%T\n", localtime(&main_time));
  makeChunk(buffer_for_time, &buffer);
  writeChunk(sessionSocket, &buffer);
*/
//STAGE1 CODE end
  DEBUG0 ("Server Writing out data:\n");
//STAGE2 CODE begin  
  int p2c[2];
  int c2p[2];
  int retval;
  pid_t pid;
  pipe(p2c);
  pipe(c2p);
  pid = fork();
  if(pid < 0) {
    fprintf(stderr, "Error Creating Child process\n");
  }
  if(pid == 0) {
    close(p2c[1]);
    close(c2p[0]);
    retval = closeSocket(sessionSocket);
    if ( retval == EXIT_FAILURE ) {
      perror( progName );
      return EXIT_FAILURE;
    }
    dup2(p2c[0],0);
    dup2(c2p[1],1);
    execl(CONV_EXEC, "convert_string", (char *) 0);
    return 0;
  }
  if(pid > 0) {
    close(p2c[0]);
    close(c2p[1]);
    dup2(p2c[1],1);
    dup2(c2p[0],0);
    readLineAsChunk(sessionSocket, &buffer);
    int ln;
    for (ln = 1; CHUNK_BAD != buffer.size; ++ ln, readLineAsChunk(sessionSocket, &buffer)) {
      writeChunk(1, &buffer);
      readLineAsChunk(0, &buffer);
      writeChunk(sessionSocket, &buffer);
      writeChunk(2, &buffer);
    }
  }
  close(p2c[0]);
  close(p2c[1]);
  close(c2p[0]);
  close(c2p[1]);
//STAGE 2 CODE end
  DEBUG0 ("Written\n");
  DEBUG0 ("Server Closing Session Socket... ");
  if ( EXIT_FAILURE == closeSocket( sessionSocket ) ) {
    perror( progName );
    return EXIT_FAILURE;
  }
  DEBUG0 ("Closed\n");
  return EXIT_SUCCESS;
}
