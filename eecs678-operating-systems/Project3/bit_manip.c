/*
 * File: bit_manip.c
 *
 * Purpose: To demonstrate various manipulations you can do to bits
 *
 * In normal programming, you deal with characters and integers.  Each
 * of these are really nothing more than a series of bits.  An ASCII
 * character is 8 bits long and an integer is the machine word size (32
 * bits for most of you).  If you wish to store values in registers,
 * manipulating bits might be easier than whole integers.
 *
 * Each bit is treated as either a logical high or low.  You can do all
 * of the standard logic operations on these bits, eg., AND, OR, NAND,
 * etc.  In C, there are bitwise AND (&), bitwise OR (|), and bitwise
 * XOR (^) already defined.  You can also do bit shifting (<< and >>).
 *
 * Everything in the program below is printed in hexadecimal notation so
 * that you can see clearly how the operations work.  In each of the
 * print statements, the format is "0x%02x".  The first '0x' is printed
 * to the screen.  The '%02x' tells the program to print it in
 * hexadecimal with a minimum of 2 positions and to fill empty positions
 * with zeroes.
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * Refer to bits by number.  Bit 0 is the rightmost (least significant)
 * bit.  This definition is equivalent to taking 2 to the power of the
 * parameter "bit".
 */
#define BIT(bit) ((unsigned char) (1 << (bit)))

/*
 * Define two masks for the high and low order nibbles
 */
#define HIGHMASK 0xf0
#define LOWMASK 0x0f

int
main () {

  /* value is declared as an unsigned char, but it could just as easily
   * be an unsigned int; the operations work in exactly the same way.
   *
   * Note that declaring value as a signed char leads to unexpected
   * results.  The problem is that printf is a varargs function.  This
   * means that all parameters passed to it undergo default promotions.
   * In particular, a signed char is cast to a signed int.  This casting
   * process involves "sign extension".  For example, if char contains
   * the value 0xfe, then:
   *
   * 0xfe = -2, so we pass the integer value -2 ... which is 0xfffffffe.
   *
   * Furthermore, printing with %x is an implicit promise to printf that
   * you have passed an unsigned value.  Passing a signed value can lead
   * to further surprises.
   */
  unsigned char value;

  /*
   * Test #1
   * Difference between signed and unsigned
   */
  value = (unsigned char) 0x9b;
  printf("TEST 1: Signed, 0x9b=%i; unsigned, 0x9b=%u\n", (char) value, value);

  /*
   * Test #2
   * See how the printing format works for a character
   */
  value = BIT(0);
  printf("TEST 2: bit 0 = 0x%02x\n", value);

  /*
   * Test #3
   * Shifting a single bit in the register
   * 0000 0001 : BIT(0)
   * 0000 1000 : BIT(0) << 3
   *
   * The whole sequence is shifted to the right, not just the bit.
   * 0's are filled in where empty spaces were.
   *
   *     | BIT(0) |
   * 0000000000010000
   *        | BIT(0) |
   */
  value = BIT(0) << 3;
  printf("TEST 3: bit 0 shifted 3 positions to the left = 0x%02x\n", value);

  /*
   * Test #4
   * Performing bitwise AND
   *
   * 0000 0001 : BIT(0)
   * 0000 0010 : BIT(1)
   * ==================
   * 0000 0000 : 0x00
   */
  value = BIT(0) & BIT(1);
  printf("TEST 4: bit 0 AND bit 1 = 0x%02x\n", value);

  /*
   * Test #5
   * Another test for bitwise AND
   *
   * 0111 1110 : 0x7e
   * 1010 0110 : 0xa6
   * ================
   * 0010 0110 : 0x26
   */
  value = (unsigned char) (0x7e & 0xa6);
  printf("TEST 5: Bitwise AND of 7e and a6 = 0x%02x\n", value);

  /*
   * Test #6
   * Performing bitwise OR
   *
   * 0000 0001 : BIT(0)
   * 0000 0010 : BIT(1)
   * ==================
   * 0000 0011 : 0x00
   */
  value = BIT(0) | BIT(1);
  printf("TEST 6: bit 0 OR bit 1 = 0x%02x\n", value);

  /*
   * Test #7
   * Another test for bitwise OR
   *
   * 0111 1110 : 0x7e
   * 1010 0110 : 0xa6
   * ================
   * 1111 1110 : 0xfe
   */
  value = (unsigned char) (0x7e | 0xa6);
  printf("TEST 7: Bitwise OR of 7e and a6 = 0x%02x\n", value);

  /*
   * Test #8
   * Using a Mask to extact multiple bits
   *
   * 0101 0101 : (bitwise OR's)
   * 0000 1111 : LOWMASK
   * ================ AND
   * 0000 0101 : 0x05
   */
  value = (BIT(0) | BIT(2) | BIT(4) | BIT(6)) & LOWMASK;
  printf("TEST 8: Using Bit Masks, value = 0x%02x\n", value);

  /*
   * Test #9
   * Using Everything Together
   *
   *     | 0xc9 |
   * 0000110010010000
   *
   *     | 0xf0 |
   * 0000111100000000
   * ================
   *     | VALU |
   * 0000110000000000
   * | VALU |
   */
  value = (0xc9 & HIGHMASK) >> 4;
  printf("TEST 9: Pulling off High Nibble of 0xc9 = 0x%02x\n", value);

  return EXIT_SUCCESS;
}
