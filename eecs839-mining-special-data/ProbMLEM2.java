import java.io.*;
import java.util.*;
class ProbMLEM2 {
  public static void main (String args[])
  {
		Scanner infile = new Scanner(System.in);
		System.out.print("Please enter name of your input file: ");
		String IN = infile.nextLine();
		File INFILE = new File(IN);

		Scanner Alp = new Scanner(System.in);
		System.out.print("Please enter value for parameter, alpha: ");
		double alpha = Alp.nextDouble();

		Scanner Elmt = new Scanner(System.in);
		System.out.print("Would you like to save elementary sets?\nEnter 'y' or 'n': ");
		String elementary = Elmt.nextLine();

		Scanner outfile = new Scanner(System.in);
		System.out.print("Please enter name of your output file: ");
		String OUT = outfile.nextLine();

		try {
			File OUTFILE = new File(OUT);
			if (!OUTFILE.exists()) {
				OUTFILE.createNewFile();
			}
			FileOutputStream fw = new FileOutputStream(OUTFILE.getAbsoluteFile());
			PrintWriter pw = new PrintWriter(fw);
//			String INFILE = "src/org/hello/eecs839/flu-rs.d";
			String caseArr[][] = file2table(INFILE);                                                 // import file into a 2D array - array of array of cases
			String attrArr[][] = transposeArr2D(caseArr);                                            // transpose 2D array - array of array of attributes
			List<Map<List<String>, List<Integer>>> blocksAVP = avpair(attrArr);                      // blocks of attribute-value pairs
			List<List<Integer>> KAsets = kasets(caseArr, blocksAVP);                                 // characteristics sets, KA-sets
			Map<List<String>, List<Float>> CondProb = condProb(caseArr, blocksAVP, KAsets);          // conditional probabilities for all concepts
			Map<List<String>, List<Integer>> SPapprox = subsetProbApprox(alpha, CondProb, KAsets);   // subset probabilistic approximations
			Map<List<List<String>>, List<String>> PMLEM2 = pmlem2(blocksAVP, SPapprox);
			for(Map.Entry<List<List<String>>, List<String>> m :PMLEM2.entrySet()) {
				pw.println(m.getKey()+" -> "+m.getValue());
			}
			pw.close();
			if (elementary.equals("y")) {
				Scanner elmt = new Scanner(System.in);
				System.out.print("Please enter name of your elementary file: ");
				String EOUT = elmt.nextLine();
				try {
					String content = "This is elementary file text";
					File ELMTFILE = new File(EOUT);
					if (!ELMTFILE.exists()) {
						ELMTFILE.createNewFile();
					}
					FileOutputStream elmtw = new FileOutputStream(ELMTFILE.getAbsoluteFile());
					PrintWriter epw = new PrintWriter(elmtw);
					for (int i=0; i<blocksAVP.size(); i++) {
						Map<List<String>, List<Integer>> BAVP = blocksAVP.get(i);
						for(Map.Entry<List<String>, List<Integer>> m :BAVP.entrySet()) {
							epw.println(m.getKey()+" = "+m.getValue());
						}
					}
					epw.close();
				}
				catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

// METHODS
	//import table into a 2D array
	static String[][] file2table(File FILE) {
		//get row and column count
		int nrow = dimFile(FILE)[0];
		int ncol = dimFile(FILE)[1];
		String Arr2D[][] = new String [nrow][ncol], strLine;
		int i=0;
		try {
			FileInputStream fstream = new FileInputStream(FILE);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			while ((strLine = br.readLine()) != null) {
				if (strLine.startsWith("<") || strLine.startsWith("!"))
					continue;
				String[] stln = strLine.split(" ");
				if (strLine.startsWith("["))
					for (int n=0; n<ncol; n++)
						Arr2D[i][n] = stln[n+1];
				else
					for (int n=0; n<ncol; n++)
						Arr2D[i][n] = stln[n];
				i++;
			}
			in.close();
		}
		catch (Exception e) {
			System.err.println("Error: " + e.getMessage());
		}
		return Arr2D;
	}

	// row and column count, i.e., file dimensions
	static int[] dimFile(File FILE) {
		String strLine;
		int nrow=0, ncol=0;
		try {
			FileInputStream fstream = new FileInputStream(FILE);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));			
			while ((strLine = br.readLine()) != null) {
				if (strLine.startsWith("<") || strLine.startsWith("!"))
					continue;
				nrow++;
				if (strLine.startsWith("["))
					ncol = strLine.split(" ").length - 2;
			}
			in.close();
		}
		catch (Exception e) {
			System.err.println("Error: " + e.getMessage());
		}
		return new int[] {nrow, ncol};
	}
	
	// transpose 2D array
	static String[][] transposeArr2D(String[][] Arr2D) {
		int nrow = Arr2D.length;
		int ncol = Arr2D[0].length;
		String tArr2D[][] = new String [ncol][nrow];
		for (int j=0; j<ncol; j++)
			for (int k=0; k<nrow; k++)
				tArr2D[j][k] = Arr2D[k][j];
		return tArr2D;
	}
	
	// blocks of attribute-value pairs
	static List<Map<List<String>, List<Integer>>> avpair(String[][] Arr2D) {
		List<Map<List<String>, List<Integer>>> blocksAVP = new ArrayList<Map<List<String>, List<Integer>>>();
		for (int i=0; i<Arr2D.length; i++) {                      // iterate through all attributes
			// numerical attribute
			if(Arr2D[i][1].matches("[0-9]+\\.?[0-9]*")) {       
				List<String> unique = uniqueElements(Arr2D[i]);   // retain unique elements
				int uSize = unique.size();
				unique = unique.subList(1, uSize);                // remove label
				uSize = unique.size();
				// convert string to integer
				double [] nums = new double[uSize];
				for (int j=0; j<uSize; j++)					
					nums[j] = Integer.parseInt(unique.get(j));
				Arrays.sort(nums);                               // sort
				// discretize
				Map<List<String>, List<Integer>> discr = new HashMap<List<String>, List<Integer>>();
				// split range across average and allocate values in two halves
				for (int j=0; j<uSize-1; j++) {
					double avg = (nums[j]+nums[j+1])/2;          // average of adjacent numeric values					
					ArrayList<Integer> first = new ArrayList<Integer>();
					ArrayList<Integer> second = new ArrayList<Integer>();
					for (int k=1; k<Arr2D[0].length; k++) {
						double attrVal = Integer.parseInt(Arr2D[i][k]);
						if ((attrVal >= nums[0]) & (attrVal < avg))
							first.add(k);
						if ((attrVal >= avg) & (attrVal <= nums[uSize-1]))
							second.add(k);
					}
					// construct range string and attribute label
					String t1 = nums[0] + ".." + avg;
					String t2 = avg + ".." + nums[uSize-1];
					ArrayList<String> label1 = new ArrayList<String>();
					ArrayList<String> label2 = new ArrayList<String>();
					label1.add(Arr2D[i][0]);
					label1.add(t1);
					label2.add(Arr2D[i][0]);
					label2.add(t2);
					// attribute value pair
					discr.put(label1, first);
					discr.put(label2, second);
				}
				blocksAVP.add(discr);
			}
			// symbolic attribute
			else {
		    List<String> uniArr = new ArrayList<String>();
		    int flag = 0;
		    for(int j=0; j<Arr2D[i].length; j++) {
		    	if(j == 0)
		    		uniArr.add(Arr2D[i][j]);
		    	else {
		    		for(int k=0; k<j; k++)
		    			if(Arr2D[i][j].equals(Arr2D[i][k]))
		    				flag = 1;
		    		if (flag == 0)
		    			uniArr.add(Arr2D[i][j]);
		    		flag = 0;
		    	}
		    }
		    List<List<String>> avp = new ArrayList<List<String>>();
		    for(int j=1; j<uniArr.size(); j++) {
		    	List<String> temp = new ArrayList<String>();
		    	temp.add(uniArr.get(0));
		    	temp.add(uniArr.get(j));
		    	avp.add(temp);
		    }
		    Map<List<String>, List<Integer>> avpairs = new HashMap<List<String>, List<Integer>>();
		    for(int j=0; j<avp.size(); j++) {
		    	List<Integer> temp1 = new ArrayList<Integer>();
		    	for(int k=1; k<Arr2D[i].length; k++)
		    		if(Arr2D[i][k].equals(avp.get(j).get(1)))
		    			temp1.add(k);
		    	avpairs.put(avp.get(j), temp1);
		    }
		    blocksAVP.add(avpairs);
		}}
		return blocksAVP;
	}

	// characteristics sets, KA-sets
	static List<List<Integer>> kasets(String[][] Arr2D, List<Map<List<String>, List<Integer>>> avpair) {
		int nrow = Arr2D.length;
		int ncol = Arr2D[0].length;
		List<List<List<Integer>>> KAset = new ArrayList<List<List<Integer>>>();
		for (int i=1; i<nrow; i++) {                           // iterate through attribute elements
			List<List<Integer>> valueList = new ArrayList<List<Integer>>();
			for (int j=0; j<ncol-1; j++) {
				List<String> keyList = new ArrayList<String>();	
				// key label and range e.g., in (flu, yes), label=flu and range=yes
				keyList.add(Arr2D[0][j]);                     // label
				if(!Arr2D[1][j].matches("[0-9]+\\.?[0-9]*"))
					keyList.add(Arr2D[i][j]);                 // symbolic range
				else {						                  // procedure for numeric range
					// select numeric attribute
					String [] elmt = new String[nrow];					
					for (int k=1; k<nrow; k++)
						for (int n=0; n<nrow; n++)
							elmt[n] = Arr2D[n][j];
					// process elements to find range (as done for AV-pairs)
					List<String> unique = uniqueElements(elmt); // retain unique elements
					int uSize = unique.size();
					unique = unique.subList(1, uSize);          // remove label
					uSize = unique.size();
					double [] nums = new double[uSize];
					for (int k=0; k<uSize; k++)					
						nums[k] = Integer.parseInt(unique.get(k)); // convert string to integer
					Arrays.sort(nums);                         // sort
					for (int k=0; k<uSize-1; k++) {            // find averages of adjacent elements
						if (keyList.size() > 1)
							keyList.remove(1);
						double avg = (nums[k]+nums[k+1])/2;
						String t1 = nums[0] + ".." + avg;
						String t2 = avg + ".." + nums[uSize-1];
						double attrVal = Integer.parseInt(Arr2D[i][j]);
						if(attrVal < avg)
							keyList.add(t1);
						else
							keyList.add(t2);
					}
				}
				// find values for the just constructed attribute value pair i.e., [(label,range)]
				valueList.add(avpair.get(j).get(keyList));
			}
			KAset.add(valueList);
		}		
		List<List<Integer>> KA = new ArrayList<List<Integer>>();
		for(int i=0; i<KAset.size(); i++) {
			List<Integer> temp = new ArrayList<Integer>();
			List<Integer> temp1 = new ArrayList<Integer>();
			List<Integer> temp2 = new ArrayList<Integer>();
			for(int j=0; j<KAset.get(i).size()-1; j++) {
				temp1.clear();
				temp2.clear();
				if(j==0)
					temp1.addAll(KAset.get(i).get(j));
				else {
					temp1.addAll(temp);
					temp.clear();
				}
				if (temp1.isEmpty())
					break;
				temp2.addAll(KAset.get(i).get(j+1));
				for(int k1=0; k1<temp1.size(); k1++) {
					int flag=0;
					for(int k2=0; k2<temp2.size(); k2++)
						if(temp1.get(k1).equals(temp2.get(k2)))
							flag = 1;
					if(flag == 1)
						temp.add(temp1.get(k1));
				}				
			}
			KA.add(temp);
		}
		return KA;
	}
	
	// conditional probabilities for all concepts
	static Map<List<String>, List<Float>> condProb(String[][] Arr2D, List<Map<List<String>, List<Integer>>> avpair, List<List<Integer>> kasets) {
		Map<List<String>, List<Float>> probability = new HashMap<List<String>, List<Float>>();
		List<String> concepts = new ArrayList<String>();
		List<Integer> l1 = new ArrayList<Integer>();
		List<Integer> l2 = new ArrayList<Integer>();
		int nrow = Arr2D.length;
		int ncol = Arr2D[0].length;
		for (int i=0; i<nrow; i++)
			if (!concepts.contains(Arr2D[i][ncol-1]))
				concepts.add(Arr2D[i][ncol-1]);
		for(int i = 0; i<avpair.get(avpair.size()-1).size(); i++) {
			List<String> keyList = new ArrayList<String>();
			List<Float> temp = new ArrayList<Float>();
			keyList.add(concepts.get(0));
			keyList.add(concepts.get(i+1));
			for (int j=0; j<kasets.size(); j++) {
				l1.clear();
				l2.clear();
				l1.addAll(avpair.get(avpair.size()-1).get(keyList));
				l2.addAll(kasets.get(j));
				int cd2 = l2.size();
				l1.retainAll(l2);
				int cd3 = l1.size();
				float pr = 0;
				if(cd2 != 0)
					pr = (float) cd3/cd2;			
				temp.add(pr);
			}
			probability.put(keyList, temp);
		}
		return probability;
	}
	
	// subset probabilistic approximations
	static Map<List<String>, List<Integer>> subsetProbApprox(double alpha, Map<List<String>, List<Float>> condProb, List<List<Integer>> kasets) {
		Map<List<String>, List<Integer>> spa = new HashMap<List<String>, List<Integer>>();
		for (List<String> KEY3 : condProb.keySet()) {
			List<Float> VAL3 = condProb.get(KEY3);
			List<Integer> temp = new ArrayList<Integer>();
			for(int j=0; j<VAL3.size(); j++)
				if(VAL3.get(j) >= alpha)
					if(kasets.get(j).size()>0) {
						for(int k=0; k<kasets.get(j).size(); k++)
							if (!temp.contains(kasets.get(j).get(k)))
								temp.add(kasets.get(j).get(k));
					}
					else
						break;
			Collections.sort(temp);
			spa.put(KEY3, temp);
		}
		 return spa;
	}
	
	// probabilistic version of MLEM2 algorithm
	static Map<List<List<String>>, List<String>> pmlem2(List<Map<List<String>, List<Integer>>> avpair, Map<List<String>, List<Integer>> CONCEPT) {
		// define variable to track previous visits in MLEM2 algorithm
		// count of total attribute-value pairs.
		Map<List<List<String>>, List<String>> ruleset = new HashMap<List<List<String>>, List<String>>();
		List<Integer> prvt = new ArrayList<Integer>();
		int count=0;
		for (int i=0; i<avpair.size(); i++)
			for (int j=0; j<avpair.get(i).size(); j++) {
				count++;
				prvt.add(count);				
			}
		// Iterate through for all concepts
		for (List<String> cnptKey : CONCEPT.keySet()) {                       // concept key
			List<Integer> D = CONCEPT.get(cnptKey);                           // concept value or goal
			Map<List<String>, List<Integer>> visit = new HashMap<List<String>, List<Integer>>(); // tracking visits
			Map<List<String>, List<Integer>> rules = new HashMap<List<String>, List<Integer>>(); // rule collection till satisfied by the goal
			List<Integer> goal = new ArrayList<Integer>();                    // subgoal
			goal.addAll(D);
			mlemIter(avpair, cnptKey, D, goal, prvt, visit, rules, ruleset);  // function call to compute rule-set for a single concept
		}
		return ruleset;
	}

//	subsetProbApprox
//	static Map<List<List<String>>, List<String>> mlemIter(List<Map<List<String>, List<Integer>>> avpair, List<String> cnptKey, List<Integer> D, List<Integer> goal, List<Integer> prvt, Map<List<String>, List<Integer>> visit, Map<List<String>, List<Integer>> rules, Map<List<List<String>>, List<String>> ruleSet) {
	static void mlemIter(List<Map<List<String>, List<Integer>>> avpair, List<String> cnptKey, List<Integer> D, List<Integer> goal, List<Integer> prvt, Map<List<String>, List<Integer>> visit, Map<List<String>, List<Integer>> rules, Map<List<List<String>>, List<String>> ruleSet) {
		Map<List<String>, List<Integer>> T = new HashMap<List<String>, List<Integer>>();
		Map<List<String>, List<Integer>> tempList = new HashMap<List<String>, List<Integer>>();		
		Map<List<String>, List<Integer>> tempList1 = new HashMap<List<String>, List<Integer>>();
		int size = 0;
		// Intersections of the AVpairs and a goal. Hash, tempList stores attribute-interaction pairs
		for(int i=0; i<avpair.size()-1; i++) {
			for (List<String> KEY : avpair.get(i).keySet()) {
				List<Integer> VAL = avpair.get(i).get(KEY);
				// skip keys that were already visited
				if(!(visit.containsKey(KEY))) {
					List<Integer> temp = new ArrayList<Integer>();
					temp.addAll(VAL);
					temp.retainAll(goal);
					// finding size of the largest intersection
					if(size < temp.size())
						size = temp.size();
					tempList.put(KEY, temp);
				}
			}
		}
		tempList1.putAll(tempList);
		// retain only the largest intersection
		final Iterator<Map.Entry<List<String>, List<Integer>>> mapIter = tempList.entrySet().iterator();
		while (mapIter.hasNext()) {
			final Map.Entry<List<String>, List<Integer>> entry = mapIter.next();
			if (entry.getValue().size() < size)
				mapIter.remove();
		}
		if (tempList.size() == 1)
			T.putAll(tempList);
		else {
			// if tie occur, select one whose corresponding attribute-value pair block is smallest
			// tempList2 will contains corresponding attribute-value pairs of largest intersections
			Map<List<String>, List<Integer>> tempList2 = new HashMap<List<String>, List<Integer>>();
			for (List<String> KEY1 : tempList.keySet()) {
				for(int i=0; i<avpair.size()-1; i++) {
					List<Integer> VAL1 = avpair.get(i).get(KEY1);
					if(VAL1 != null)
						tempList2.put(KEY1, VAL1);
				}
			}
			// find size of the smallest non-empty attribute in tempList2
			int size2 = 10000;  // set initial size to be impractically large 
			for (List<String> KEY2 : tempList2.keySet()) {
				List<Integer> VAL2 = tempList2.get(KEY2);
				if( (VAL2.size() > 0) & (VAL2.size() <= size2) )
					size2 = VAL2.size();
			}
			// store smallest attribute-value pair(s) in tempList3 
			Map<List<String>, List<Integer>> tempList3 = new HashMap<List<String>, List<Integer>>();
			for (List<String> KEY2 : tempList2.keySet()) {
				List<Integer> VAL2 = tempList2.get(KEY2);
				if(VAL2.size() == size2)
					tempList3.put(KEY2, VAL2);
			}
			if (tempList3.size() == 1)
				T.putAll(tempList3);
			else {
				// if another tie occur, select the first one
				// since HashMap data-structure is unordered, just iterate through hash, store key-value pairs in two lists
				// and finally select pair from list which were entered first
				List<List<String>> Klist = new ArrayList<List<String>>();
				List<List<Integer>> Vlist = new ArrayList<List<Integer>>();
				for (List<String> KEY3 : tempList3.keySet()) {
					List<Integer> VAL3 = tempList3.get(KEY3);
					Klist.add(KEY3);
					Vlist.add(VAL3);
				}
				T.put(Klist.get(Klist.size()-1), Vlist.get(Klist.size()-1));
			}
		}
		// T contains the finally selected intersection
		List<Integer> valT = new ArrayList<Integer>();
		List<String> keyT = new ArrayList<String>();
		for (List<String> KEY4 : T.keySet()) {
			valT = T.get(KEY4);
			keyT.addAll(KEY4);
			visit.put(KEY4, valT);                     // mark visited attribute-values and keep listed in a hash, visit
			rules.put(KEY4, valT);                     // collect pairs temporarily until satisfied by the goal
		}
		// track previous visits
		// intersection of previously visited and just visited pairs.
		// valT updates itself to contain intersection elements
		valT.retainAll(prvt);
		// check if goal, D is satisfied by the just computed intersection set, valT 
		if (D.containsAll(valT) == true) {             // subset test satisfied
			goal.removeAll(valT);                      // find new goal by subtracted intersection set from goal set i.e., D - valT
			// copy rules from temporarily stored in hashMap, rules to more permanent hashMap, ruleSet
			List<List<String>> tempR = new ArrayList<List<String>>();
			for (List<String> keyR : rules.keySet()) {
				tempR.add(keyR);
			}
			ruleSet.put(tempR, cnptKey);
			// if new goal is non-empty, clear and update temporary data-structures and repeat the procedure
			if(goal.size() > 1) {
				prvt.clear();
				visit.clear();
				rules.clear();
				int count=0;
				for (int i=0; i<avpair.size(); i++)
					for (int j=0; j<avpair.get(i).size(); j++) {
						count++;
						prvt.add(count);				
					}
				mlemIter(avpair, cnptKey, D, goal, prvt, visit, rules, ruleSet);
			}
		}
		else {
			// subset test not satisfied
			// next subgoal is the best intersection stored in tempList1
			// update next goal
			goal.clear();
			goal.addAll(tempList.get(keyT));
			// update previous visits
			prvt.retainAll(valT);
			// continue till there remains no more finite subgoals
			if (goal.size() > 0)
				mlemIter(avpair, cnptKey, D, goal, prvt, visit, rules, ruleSet);
		}
	}

	//print 2D array
	static void printArr2D(String[][] Arr2D) {
		int nrow = Arr2D.length;
		int ncol = Arr2D[0].length;
		for (int i=0; i<nrow; i++) {
			for (int j=0; j<ncol; j++)
				System.out.print(Arr2D[i][j] + " ");
			System.out.println();
		}
	}
	
	//unique array elements
	static List<String> uniqueElements(String[] arr) {
	    int flag = 0;
	    for(int i=0; i<arr.length; i++) {
	    	for(int j=0; j<i; j++)
	    		if(arr[i].equals(arr[j]))
	    			flag = 1;
	    	flag = 0;
	    }
	    List<String> uniArr = new ArrayList<String>();
	    for(int i=0; i<arr.length; i++) {
	    	if(i == 0)
	    		uniArr.add(arr[i]);
	    	else {
	    		for(int j=0; j<i; j++)
	    			if(arr[i].equals(arr[j]))
	    				flag = 1;
	    		if (flag == 0)
	    			uniArr.add(arr[i]);
	    		flag = 0;
	    	}
	    }
	    return uniArr;
	}	
}
