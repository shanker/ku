#include <iostream>
#include <cstdlib>
#include <cmath>

using namespace std;

struct triplet
{
  int size ;
  int* one = new int[size];
  int* two = new int[size];
  int* three = new int[size];
};

triplet trisplit(int *Arr, int sublistSize);
int* trimerge(int *arr1, int *arr2, int *arr3, int size);

int main() 
{
  cout << "====================================================" << endl;
  int min=1, max=5000, k=3;
  int listSize = pow(3,k);
  int* randArr = new int[listSize];
  cout << "Original unsorted list:" << endl;
  for(int i=0; i<listSize; i++)
  {
    randArr[i] = (rand()%(max-min))+min;
    cout << randArr[i] << " ";
  }
  cout << endl;
  triplet retArr1 = trisplit(randArr, listSize/3);
  cout << endl << "Merged sublists of size=1:" << endl;
  for(int p=0; p<listSize/3; p++)
    cout << retArr1.one[p] << " ";
  cout << endl;
  for(int q=0; q<listSize/3; q++)
    cout << retArr1.two[q] << " ";
  cout << endl;
  for(int r=0; r<listSize/3; r++)
    cout << retArr1.three[r] << " ";
  cout << endl;
  cout << endl << "Final sorted list:" << endl;
 int* retArr2 = new int[listSize];
  retArr2 = trimerge(retArr1.one, retArr1.two, retArr1.three, listSize/3);
  for(int a=0; a<listSize; a++)
    cout << retArr2[a] << " ";
  cout << endl;
  cout << "====================================================" << endl;
  return 0;
}

triplet trisplit(int *Arr, int sublistSize)
{
  int i=0, j=sublistSize, k=2*sublistSize;                                  // boundaries for 3 sublists
  triplet arr;
  arr.size = sublistSize;
  for(int x=0; x<arr.size; x++)                                             // putting list elements into appropriate sublists
  {
    arr.one[x] = Arr[i];
    arr.two[x] = Arr[j];
    arr.three[x] = Arr[k];
    i++;
    j++;
    k++;
  }
  if(arr.size == 1)
  {
    return arr;
  }
  else
  {
    triplet retArr1, retArr2, retArr3;
    retArr1 = trisplit(arr.one, arr.size/3);
    retArr2 = trisplit(arr.two, arr.size/3);
    retArr3 = trisplit(arr.three, arr.size/3);
    arr.one = trimerge(retArr1.one, retArr1.two, retArr1.three, arr.size/3);
    arr.two = trimerge(retArr2.one, retArr2.two, retArr2.three, arr.size/3);
    arr.three = trimerge(retArr3.one, retArr3.two, retArr3.three, arr.size/3);
    arr.size = arr.size;
    return arr;
  }
}

int* trimerge(int *arr1, int *arr2, int *arr3, int size)
{
  int i=0, j=0, k=0;
  int* arr = new int[9];
  for(int a=0; a<3*size; a++)
  {
    if(i<size and j<size and k<size)
    {
      if(arr1[i]<arr2[j] and arr1[i]<arr3[k])
      {
        arr[a] = arr1[i];
        i++;
      }
      else if(arr2[j]<arr1[i] and arr2[j]<arr3[k])
      {
        arr[a] = arr2[j];
        j++;
      }
      else if(arr3[k]<arr1[i] and arr3[k]<arr2[j])
      {
        arr[a] = arr3[k];
        k++;
      }
    }
    else if(i>=size and j<size and k<size)
    {
       if(arr2[j]<arr3[k])
       {
         arr[a] = arr2[j];
         j++;
       }
       else if(arr2[j]>arr3[k])
       {
         arr[a] = arr3[k];
         k++;
       }
    }
    else if(i<size and j>=size and k<size)
    {
       if(arr1[i]<arr3[k])
       {
         arr[a] = arr1[i];
         i++;
       }
       else
       {
         arr[a] = arr3[k];
         k++;
       }
    }
    else if(i<size and j<size and k>=size)
    {
       if(arr1[i]<arr2[j])
       {
         arr[a] = arr1[i];
         i++;
       }
       else
       {
         arr[a] = arr2[j];
         j++;
       }
    }
    else if(i<size and j>=size and k>=size)
    {
      arr[a] = arr1[i];
      i++;
    }
    else if(i>=size and j<size and k>=size)
    {
      arr[a] = arr2[j];
      j++;
    }
    else if(i>=size and j>=size and k<size)
    {
      arr[a] = arr3[k];
      k++;
    }
  }
  return arr;
}
