#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <vector>
#include <stdio.h>
#include <string.h>

#define INFILE "proj3_data.txt"							// define macro for input file name
#define OUTFILE "rao_proj3_test_results.txt"			// define macro for output file name

using namespace std;

int main() 
{
  cout << "====================================================" << endl;
  vector< vector<int> > node;
  string color[25] = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y"};
  int size, eof=1, p=1;
  string line;
  vector <int> temp;
  ifstream infile (INFILE);
  infile >> size;
  while (getline(infile, line))
  {
    if(line != "" and line != "-1")
    {
      stringstream ss(line);
      int s;
      temp.clear();
      temp.push_back(p);
      p++;
      while (ss >> s)
        temp.push_back(s);
      node.push_back(temp);
    }
    else if(line == "-1")
    {
      eof=-1;
      infile.close();
    }
  }
  string empty = "0";
  string mapcolor[size];
  int colorindex=0;
  for(int i=0; i<size; i++)
    mapcolor[i] = empty;
  int x=0;
  for(int a=0; a<node.size(); a++)
  {
    for(int b=0; b<node[a].size(); b++)
    {
      if(mapcolor[node[a][b]-1].compare(empty) == 0)                    // if node is not assigned any color
      {
        if(b==0)
        {
          x=0;
          mapcolor[node[a][b]-1] = color[x];
          if(colorindex < x)  colorindex = x;
          x++;
        }
        else
        {
          mapcolor[node[a][b]-1] = color[x];
          if(colorindex < x)  colorindex = x;
        }
      }
      else
      {
        if(b==0)
        {
          x=0;
          for(int i=node[a][b]-2; i>0; i--)
            for(int j=0; j<node[i].size(); j++)
              if(node[a][b]-2 == node[i][j]-2)
                for(int k=0; k<=colorindex; k++)
                {
                  if(mapcolor[node[i][0]-2] == color[x])
                  {
                    x++;
                    mapcolor[node[a][b]-2] = color[x];
                    if(colorindex < x)  colorindex = x;
                  }
                }
        }
        else if(b!=0)
        {
          x=0;
          for(int i=node[a][b]-1; i>0; i--)
          {
            for(int j=0; j<node[i].size(); j++)
            {
              if(node[a][b]-1 == node[i][j]-1)
              {
                for(int k=0; k<=colorindex; k++)
                {
                  if(mapcolor[node[i][0]-1] == color[x])
                  {
                    x++;
                    mapcolor[node[a][b]-1] = color[x];
                    if(colorindex < x)  colorindex = x;
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  cout << "OUTPUT IS SAVED IN FILE: rao_proj3_test_results.txt" << endl;
  ofstream outfile;
  outfile.open (OUTFILE);
  outfile << "--------------------------------------" << endl;
  outfile << "map color:" << endl;
  outfile << "Region\tColorID" << endl;
  for(int i=0; i<size; i++)
    outfile << i+1 << "\t" << mapcolor[i] << endl;
  outfile << "--------------------------------------" << endl;
  outfile << "ColorID\tRegion" << endl;
  for(int i=0; i<=colorindex; i++)
  {
    outfile << color[i] << "\t";
    for(int j=0; j<size; j++)
      if(color[i] == mapcolor[j])
        outfile << j+1 << " ";
    outfile << endl;
  }
  outfile << "--------------------------------------" << endl;
  outfile.close();
  cout << "====================================================" << endl;
  return 0;
}
