Part 1: Trimergesort
	cd to trimergesort -> make
	The program runs on small test dataset and prints on screen:
		- Original unsorted list
		- Merged sublists of size=1
		- Final sorted list

Part 2: Map coloring
	cd to mapcolor -> make
	input and output files are defined as macro in main.cpp
