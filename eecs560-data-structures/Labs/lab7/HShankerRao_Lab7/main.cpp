#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include "HashTable.h"
using namespace std;
#define DATA "lab7_data.txt"													// define macro for data file name
#define TEST "lab7W_test.txt"													// define macro for test file name

const int TABLE_SIZE = 29;

int main()
{
	HashTable LinearHash(TABLE_SIZE, false);				// insert elements using linear probing
	HashTable QuadraticHash(TABLE_SIZE, true);			// insert elements using quadratic probing
	// Insert
	string line;
	ifstream datafile (DATA);
	while(getline(datafile, line)) {
		int element = atoi(line.c_str());
		LinearHash.insert(element);
		QuadraticHash.insert(element);
	}
	datafile.close();
	cout << "=======================================================================================" << endl;
	cout << "After Linear Inserts:" << endl;
	LinearHash.print();
	cout << endl;
	ifstream testfile (TEST);
	while(getline(testfile, line)) {
		 if (line.substr (0,1).compare("p") == 0) {	// if line begins with letter p, print hash table
			cout << "print: ";
			LinearHash.print();
			cout << endl;
		}
		else {
			stringstream ss(line);							// tokenize line and put 3 pieces of information into a temporary array
			string s;
			string arr[2] = {};
			int i=0;
			while (getline(ss, s, ' ')) {
					arr[i] = s;
					i++;
			}
			if (arr[0] == "i") {								// if line begins with letter i, insert following number into the hash table
				int element = atoi(arr[1].c_str());
				cout << "insert " << element << endl;;
				LinearHash.insert(element);
			}
			else if (arr[0] == "d") {						// if line begins with letter d, delete following number into the hash table
				int element = atoi(arr[1].c_str());
				cout << "delete " << element << endl;
				LinearHash.remove(element);
			}
		}
	}
	testfile.close();
	cout << "=======================================================================================" << endl;
	cout << "After Quadratic Inserts:" << endl;
	QuadraticHash.print();
	cout << endl;
	ifstream testfile2 (TEST);
	while(getline(testfile2, line)) {
		if (line.substr (0,1).compare("p") == 0) {	// if line begins with letter p, print hash table
			cout << "print: ";
			QuadraticHash.print();
			cout << endl;
		}
		else {
			stringstream ss(line);							// tokenize line and put 2 pieces of information into a temporary array
			string s;
			string arr[2] = {};
			int i=0;
			while (getline(ss, s, ' ')) {
				arr[i] = s;
				i++;
			}
			if (arr[0] == "i") {								// if line begins with letter i, insert following number into the hash table
				int element = atoi(arr[1].c_str());
				cout << "insert " << element << endl;;
				QuadraticHash.insert(element);
			}
			else if (arr[0] == "d") {						// if line begins with letter d, delete following number into the hash table
				int element = atoi(arr[1].c_str());
				cout << "delete " << element << endl;;
				QuadraticHash.remove(element);
			}
		}
	}
	testfile2.close();
	cout << "=======================================================================================" << endl;
	return 0;
}
