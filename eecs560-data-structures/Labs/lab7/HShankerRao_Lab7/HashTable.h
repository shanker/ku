#ifndef HASHTABLE_H_
#define HASHTABLE_H_

class HashTable {
	private:
		int size;
		bool useQuadratic;
		int hash(int key);
		int* array;
		char* arr_status;
	public:
		HashTable();
		HashTable(int theSize, bool shouldUseQuadratic);
		void insert(int key);
		void remove(int k);
		void fun(int index, int key);
		void print();
		~HashTable();
};

#include "HashTable.cpp"
#endif /* HASHTABLE_H_ */
