#include <iostream>
#include <stdlib.h>
#include "HashTable.h"
using namespace std;

HashTable::HashTable()
{
	size = 0;
	useQuadratic = false;
	array = new int[0];
	arr_status = new char[0];
}

HashTable::HashTable(int theSize, bool shouldUseQuadratic)
{
	size = theSize;
	useQuadratic = shouldUseQuadratic;
	array = new int[theSize];
	arr_status = new char[theSize];
	for(int i=0; i<theSize; i++)
	{
		array[i] = 0;
		arr_status[i] = 'O';
	}
}
HashTable::~HashTable()
{
	size = 0;
	useQuadratic = false;
	array = new int[0];
	arr_status = new char[0];
}

void HashTable::insert(int key)
{
	fun(hash(key), key);
}

void HashTable::fun(int index, int key)
{
	if(arr_status[index] != 'U')
	{
		array[index] = key;
		arr_status[index] = 'U';
	}
	else
	{
		if(array[index] == key)
			return;
		else
		{
			if(useQuadratic)  
			{
				int i=0;
				while(arr_status[index] == 'U')
				{
					i++;
					arr_status[index] = arr_status[hash(index+(i*i))];
				}
				fun(hash(index+(i*i)), key);
			}
			else
				fun(hash(index+1), key);
		}
	}
}

int HashTable::hash(int key)
{
	return (key % size);
}

void HashTable::print()
{
	for(int i=0; i<size; i++)
		cout << array[i] << " ";
}

void HashTable::remove(int k)
{
	for(int i=0; i<size; i++)
	{
		if(array[i] == k)
			array[i] = -1;
	}
}
