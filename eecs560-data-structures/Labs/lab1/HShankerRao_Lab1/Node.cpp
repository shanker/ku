#include "Node.h"
// For NULL
#include <cstddef>

Node::Node(){
	value = 0;
	next = NULL;
}

Node::Node( int v, Node * n){
	value = v;
	next = n;
}

