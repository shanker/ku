#include <iostream>
#include <fstream>
#include <stdlib.h>
#include "List.h"
using namespace std;

List::List() {
  first = NULL;
  last = NULL;
}

List::~List() {
  delete first;
  delete last;
}

void List::push_back(int x) 
{
  Node* newNode = new Node();
  newNode -> setData(x);
  newNode -> setLink(NULL);
  Node* tmp = first;
  if (!tmp)
  {
    first = newNode;
  }
  else
  {
    while (tmp -> getLink())
      tmp = tmp -> getLink();
    tmp -> setLink(newNode);
  } 
}

void List::print() 
{
  Node* tmp = first;
  while (tmp -> getLink())
  {
    cout << tmp -> getData() << " ";
    tmp = tmp -> getLink();
  }
  cout << tmp -> getData();
}

void List::reverse()
{
  Node* cur = first;
  first = last;
  while (cur)
  {
    Node* tmp = cur;
    cur = cur -> next;
    tmp -> next = first;
    first = tmp;
  } 
}

