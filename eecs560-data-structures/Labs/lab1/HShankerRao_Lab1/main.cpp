#include <iostream>
#include <fstream>
#include <stdlib.h>
#include "List.h"
using namespace std;

int main() {
	
	List L;
	int x;
	ifstream dataFile ("data.txt");
	if (dataFile.is_open()) {
		while (dataFile >> x){
			L.push_back(x);
		}
	}
	dataFile.close();

	L.print();
	cout << endl;
	L.reverse();
	L.print();
	cout << endl;

	return 0;
}

