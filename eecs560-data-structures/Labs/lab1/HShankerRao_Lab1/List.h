#ifndef LIST_H_
#define LIST_H_

#include "Node.h"
#include <iostream>

class List {
public:
	private:
		Node* first;
		Node* last;
	
	public:
		List();
		void push_back(int x);
		void print();
		void reverse();
		~List();
};
#endif /* LIST_H_ */
