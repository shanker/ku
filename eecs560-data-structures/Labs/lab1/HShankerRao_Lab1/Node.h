#ifndef NODE_H_
#define NODE_H_

class Node {
	
	public:
		Node();
		Node(int v, Node * n);

		Node* getLink() {return next; }
		int getData() {return value; }
		void setData(int v) {value = v;}
		void setLink(Node* pointer) {next = pointer; }

		int value;
		Node* next;
};

#endif /* NODE_H_ */
