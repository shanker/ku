#include <iostream>
#include <fstream>
#include <sstream>
#include "BinarySearchTree.h"
#include "UnorderedArray.h"
#include <stdlib.h>
#include <cmath>
using namespace std;
#define FILE "capitals.txt"									// define macro for input file name
#define FILE1 "lab6_testW.txt"								// define macro for input file name
#define FILE2 "data6W.txt"										// define macro for input file name
void cities_in_DB();
void cities_within_Range();

int main() {
	BinarySearchTree<string> stringTree;
	UnorderedArray<string> stringArray;
	string line;
	cout << "=============================================" << endl;
	cout << "Data structure\t\tCount\tCity name" << endl;
	cout << "---------------------------------------------" << endl;
	int counter1, counter2, counter3, counter4, counter5, counter6,
		 counter7, counter8, counter9, counter10, counter11, counter12,
		 counter13, counter14, counter15, counter16, counter17, counter18,
		 counter19, counter20, counter21, counter22, counter23, counter24;
	// Insert operations using BST and Unordered array data structures
	cout << "BST insert\t\t";
	ifstream infile1 (FILE);
	while(getline(infile1, line)) {
		counter1 = stringTree.insert(line);
	}
	infile1.close();
	cout << counter1 << "\tAll" << endl;
	cout << "UnorderedArray insert\t";
	ifstream infile2 (FILE);
	while(getline(infile2, line)) {
		counter2 = stringArray.insert(line);
	}
	infile2.close();
	cout << counter2 << "\tAll" << endl;
	// 10 successive delete operations using BST and Unordered array data structures
	cout << "BST remove\t\t";
	counter3 = stringTree.remove("SantoDomingo");
	cout << counter3-counter1 << "\t" << "SantoDomingo" << endl;
	cout << "UnorderedArray remove\t";
	counter4 = stringArray.remove("SantoDomingo");
	cout << counter4-counter2 << "\t" << "SantoDomingo" << endl;
	cout << "BST remove\t\t";
	counter5 = stringTree.remove("NewDelhi");
	cout << counter5-counter3 << "\t" << "NewDelhi" << endl;
	cout << "UnorderedArray remove\t";
	counter6 = stringArray.remove("NewDelhi");
	cout << counter6-counter4 << "\t" << "NewDelhi" << endl;
	cout << "BST remove\t\t";
	counter7 = stringTree.remove("Washington");
	cout << counter7-counter5 << "\t" << "Washington" << endl;
	cout << "UnorderedArray remove\t";
	counter8 = stringArray.remove("Washington");
	cout << counter8-counter6 << "\t" << "Washington" << endl;
	cout << "BST remove\t\t";
	counter9 = stringTree.remove("Kabul");
	cout << counter9-counter7 << "\t" << "Kabul" << endl;
	cout << "UnorderedArray remove\t";
	counter10 = stringArray.remove("Kabul");
	cout << counter10-counter8 << "\t" << "Kabul" << endl;
	cout << "BST remove\t\t";
	counter11 = stringTree.remove("Canberra");
	cout << counter11-counter9 << "\t" << "Canberra" << endl;
	cout << "UnorderedArray remove\t";
	counter12 = stringArray.remove("Canberra");
	cout << counter12-counter10 << "\t" << "Canberra" << endl;
	cout << "BST remove\t\t";
	counter13 = stringTree.remove("Vienna");
	cout << counter13-counter11 << "\t" << "Vienna" << endl;
	cout << "UnorderedArray remove\t";
	counter14 = stringArray.remove("Vienna");
	cout << counter14-counter12 << "\t" << "Vienna" << endl;
	cout << "BST remove\t\t";
	counter15 = stringTree.remove("Thimphu");
	cout << counter15-counter13 << "\t" << "Thimphu" << endl;
	cout << "UnorderedArray remove\t";
	counter16 = stringArray.remove("Thimphu");
	cout << counter16-counter14 << "\t" << "Thimphu" << endl;
	cout << "BST remove\t\t";
	counter17 = stringTree.remove("Ottawa");
	cout << counter17-counter15 << "\t" << "Ottawa" << endl;
	cout << "UnorderedArray remove\t";
	counter18 = stringArray.remove("Ottawa");
	cout << counter18-counter16 << "\t" << "Ottawa" << endl;
	cout << "BST remove\t\t";
	counter19 = stringTree.remove("Beijing");
	cout << counter19-counter17 << "\t" << "Beijing" << endl;
	cout << "UnorderedArray remove\t";
	counter20 = stringArray.remove("Beijing");
	cout << counter20-counter18 << "\t" << "Beijing" << endl;
	cout << "BST remove\t\t";
	counter21 = stringTree.remove("Havana");
	cout << counter21-counter19 << "\t" << "Havana" << endl;
	cout << "UnorderedArray remove\t";
	counter22 = stringArray.remove("Havana");
	cout << counter22-counter20 << "\t" << "Havana" << endl;
	cout << "=============================================" << endl << endl;
	//Lab6 implementations
	cities_in_DB();				//Searching cities by coordinates
	cities_within_Range();		//Distance function
	return 0;
}

void cities_in_DB()
{
	string line1, x1, y1, d, line2, city, x2, y2;
	cout << "==============================================" << endl;
	cout << "Associated cities in the database" << endl << endl;
	cout << "x1-y1-d\t\tCity name" << endl;
	cout << "----------------------------------------------" << endl;
	ifstream infile1 (FILE1);
	while(getline(infile1, line1)) {
		stringstream token1(line1);
		token1 >> x1 >> y1 >> d;
		cout << x1 << "-" << y1 << "-" << d;
		double X1=atof(x1.c_str()), Y1=atof(y1.c_str()), D=atof(d.c_str());
		ifstream infile2 (FILE2);
		while(getline(infile2, line2)) {
			stringstream token2(line2);
			token2 >> city >> x2 >> y2;
			double X2=atof(x2.c_str()), Y2=atof(y2.c_str());
			if(X1==X2 and Y1==Y2)
				cout << "\t\t" << city;
		}
		cout << endl;
		infile2.close();
	}
	infile1.close();
	cout << "==============================================" << endl << endl;

}
void cities_within_Range()
{
	string line1, x1, y1, d, line2, city, x2, y2;
	double distance;
	cout << "==============================================" << endl;
	cout << "Cities with respective range" << endl << endl;
	cout << "x1-y1-d\t\tDistance\tCity name" << endl;
	cout << "----------------------------------------------" << endl;
	ifstream infile1 (FILE1);
	while(getline(infile1, line1)) {
		stringstream token1(line1);
		token1 >> x1 >> y1 >> d;
		double X1=atof(x1.c_str()), Y1=atof(y1.c_str()), D=atof(d.c_str());
		ifstream infile2 (FILE2);
		while(getline(infile2, line2)) {
			stringstream token2(line2);
			token2 >> city >> x2 >> y2;
			double X2=atof(x2.c_str()), Y2=atof(y2.c_str());
			distance = sqrt(pow(X2-X1, 2) + pow(Y2-Y1, 2));
			if (distance <= D)
				cout << x1 << "-" << y1 << "-" << D << "\t\t" << distance << "\t\t" << city << endl;
		}
		infile2.close();
		cout << "----------------------------------------------" << endl;
	}
	infile1.close();
	cout << "==============================================" << endl;

}
