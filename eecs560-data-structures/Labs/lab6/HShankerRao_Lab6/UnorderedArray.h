#ifndef UNORDEREDARRAY_H_
#define UNORDEREDARRAY_H_
#include "TNode.h"

template <typename T>
class UnorderedArray {
	private:
		TNode<T> *root;
	public:
		UnorderedArray();
		int counter;
		int insert(T v);
		int insert(T v1, T v2, T v3, TNode<T>* &t);
		void inOrderTraversal();
		void inOrderTraversal(TNode<T>* &t);
		void makeEmpty();
		void makeEmpty(TNode<T>* &t);
		int remove(T v);
		int remove(T v, TNode<T>* &t);
		void remove_by_x(T x);
		void remove_by_x(T x, TNode<T>* &t);
		void remove_by_y(T y);
		void remove_by_y(T y, TNode<T>* &t);
		T* findLast();
		T* findLast(TNode<T>* t);
		~UnorderedArray();
};
#include "UnorderedArray.cpp"
#endif /* UNORDEREDARRAY_H_ */
