#ifndef TNODE_H_
#define TNODE_H_

template <typename T>
class TNode
{
	public:
		T value;
		T value1;
		T value2;
		T value3;
		TNode<T>* left;
		TNode<T>* right;
		TNode(T v);
		TNode(T v1, T v2, T v3, TNode<T>* lt, TNode<T>* rt);
//		void print();
};
#include "TNode.cpp"
#endif /* TNODE_H_ */
