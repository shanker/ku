#include <iostream>
#include <fstream>
#include <stdlib.h>
#include "BinarySearchTree.h"
using namespace std;

template <typename T>
BinarySearchTree<T>::BinarySearchTree()
{
  root = NULL;
  ROOT = NULL;
  max = 0;
  count = 0;
}

template <typename T>
BinarySearchTree<T>::~BinarySearchTree()
{
  makeEmpty();
}

template <typename T>
void BinarySearchTree<T>::insert(T v)
{
  insert(v, root);
}

template <typename T>
void BinarySearchTree<T>::insert(T v, TNode<T>* &t)
{
  if( t == NULL )
  {
    root = new TNode<T>(v, false, NULL, NULL);
    ROOT = root;
  }
  else 
  {
    while(!(t->visit))
    {
      if( t == NULL )
        return;
      else if(t->left == NULL)
      {
        t->left = new TNode<T>(v, false, NULL, NULL);
        return;
      }
      else if(t->right == NULL)
      {
        t->right = new TNode<T>(v, false, NULL, NULL);
        return;
      }
      else if(t->left)
        t = t->left;
      else if(t->right)
        t = t->right;
      else
        t->visit = true;
    }
  }
}

template <typename T>
void BinarySearchTree<T>::inOrderTraversal()
{
  inOrderTraversal(ROOT);
}

template <typename T>
void BinarySearchTree<T>::inOrderTraversal(TNode<T>* &t)
{
  if(t != NULL)
  {
    if(t->left)
      inOrderTraversal(t->left);
    if(t->value)
      cout << t->value << " ";
    if(t->right)
      inOrderTraversal(t->right);
  }
}

template <typename T>
int BinarySearchTree<T>::findMax()
{
  return findMax(ROOT);
}

template <typename T>
int BinarySearchTree<T>::findMax(TNode<T>* &t)
{
  bool truth = false;
  while(!truth)
  {
    if(t->value > max)
      max = t->value;
    if(t->left and t->left->value)
      findMax(t->left);
    if(t->right and t->right->value)
      findMax(t->right);
    truth = true;
  }
  return max;
}

template <typename T>
int BinarySearchTree<T>::countLeaf()
{
  return countLeaf(ROOT);
}

template <typename T>
int BinarySearchTree<T>::countLeaf(TNode<T>* &t)
{
  bool truth = false;
  while(!truth)
  {
    if(t->left and t->right and !(t->left->value) and !(t->right->value))
      count++;
    if(t->left)
      countLeaf(t->left);
    if(t->right)
      countLeaf(t->right);
    truth = true;
  }
  return count;
}

template <typename T>
void BinarySearchTree<T>::makeEmpty()
{
  makeEmpty(root);
}

template <typename T>
void BinarySearchTree<T>::makeEmpty(TNode<T>* &t)
{
  if(t != NULL)
  {
    makeEmpty(t->left);
    makeEmpty(t->right);
    delete t;
  }
  t = NULL;
  max = 0;
  count = 0;
}
