#ifndef TNODE_H_
#define TNODE_H_

template <typename T>
class TNode
{
  public:
    T value;
    bool visit;
    TNode<T>* left;
    TNode<T>* right;
    TNode(T v);
    TNode(T v, bool vis, TNode<T>* lt, TNode<T>* rt);
    ~TNode();
};

#include "TNode.cpp"
#endif /* TNODE_H_ */
