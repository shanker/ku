#include <iostream>
#include <fstream>
#include "BinarySearchTree.h"
using namespace std;

int main() 
{
  BinarySearchTree<int> intTree;
  int i;
  ifstream intFile ("data2.txt");
  if (intFile.is_open()) 
  {
    while (intFile >> i)
    {
      intTree.insert(i);
    }
  }
  intFile.close();
  cout << endl << "Inorder traversal: ";
  intTree.inOrderTraversal();
  cout << endl;
  cout << "Largest element: " << intTree.findMax() << endl;
  cout << "Leaf count: " << intTree.countLeaf() << endl << endl;
  return 0;
}
