#include "TNode.h"
#include <iostream>
using namespace std;

template <typename T>
TNode<T>::TNode(T v)
{
  value = v;
  visit = false;
  left = NULL;
  right = NULL;
}

template <typename T>
TNode<T>::TNode(T v, bool vis, TNode<T>* lt, TNode<T>* rt )
{
  value = v;
  visit = vis;
  left = lt;
  right = rt;
}

template <typename T>
TNode<T>::~TNode()
{
  left = NULL;
  right = NULL;
}
