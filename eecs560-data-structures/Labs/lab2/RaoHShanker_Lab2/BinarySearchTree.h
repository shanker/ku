#ifndef BINARYSEARCHTREE_H_
#define BINARYSEARCHTREE_H_

#include "TNode.h"

template <typename T>
class BinarySearchTree
{
  private:
    TNode<T> *root;
    TNode<T> *ROOT;
    int max;
    int count;
  public:
    BinarySearchTree();
    void insert(T v);
    void insert(T v, TNode<T>* &t);
    void inOrderTraversal();
    void inOrderTraversal(TNode<T>* &t);
    int findMax();
    int findMax(TNode<T>* &t);
    int countLeaf();
    int countLeaf(TNode<T>* &t);
    void makeEmpty();
    void makeEmpty(TNode<T>* &t);
    ~BinarySearchTree();
};

#include "BinarySearchTree.cpp"
#endif /* BINARYSEARCHTREE_H_ */
