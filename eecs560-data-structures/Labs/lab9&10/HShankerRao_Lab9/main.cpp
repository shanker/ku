#include <iostream>
#include <stdlib.h>
#include "Heap.h"
using namespace std;

int main()
{
// Generating random list
  int Size = 15;
  int pheap[Size];
  srand(1);
  for(int i=0; i<Size; i++)
  {
    pheap[i] = rand() % 50;
  }
  cout << "Test array:";
  for(int j=0; j<Size; j++)
    cout << " " << pheap[j];
  cout << endl << "Pairing-heap:" << endl;
// Build pairing heap by inserting array elements
  Heap binaryHeap;
  for(int j=0; j<Size; j++) {
    binaryHeap.insert(pheap[j]);
  }
// Printing elements
  binaryHeap.print();
  return 0;
}
