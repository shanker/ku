#include "TNode.h"
using namespace std;

TNode::TNode(int v)
{
  value = v;
  previous = NULL;
  leftChild = NULL;
  nextSibling = NULL;
}

TNode::TNode(int v, TNode* pr, TNode* ltCh, TNode* nxSb )
{
  value = v;
  previous = pr;
  leftChild = ltCh;
  nextSibling = nxSb;
}

TNode::~TNode()
{
  previous = NULL;
  leftChild = NULL;
  nextSibling = NULL;
}
