using namespace std;

Heap::Heap()
{
  root = NULL;
}

Heap::~Heap()
{
  root = NULL;
}

void Heap::insert(int x)
{
  PairingHeap(x, root);
}

void Heap::PairingHeap(int x, TNode* &t)
{
  if( t == NULL )
    root = new TNode(x, NULL, NULL, NULL);
  else if(x > t->value)
  {
    if(t->leftChild == NULL)
      t->leftChild = new TNode(x, t, NULL, NULL);
    else
      t->leftChild = new TNode(x, t, NULL, t->leftChild);
  }
  else if(x < t->value)
    root = new TNode(x, NULL, t, NULL);
}

void Heap::print()
{
  print(root);
}

void Heap::print(TNode* &t)
{
  if(t != NULL)
  {
      cout << t->value << " ";
    if(t->nextSibling)
    {
      print(t->nextSibling);
      if(t->leftChild)
        print(t->leftChild);
    }
    else
      cout << endl;
    if(t->leftChild)
      print(t->leftChild);
  }
}
