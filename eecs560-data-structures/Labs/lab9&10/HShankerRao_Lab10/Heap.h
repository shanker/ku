#ifndef HEAP_H_
#define HEAP_H_

#include "TNode.h"

//template <typename T>
class Heap {
	private:
		TNode* root;
		int count;
	public:
		Heap();
		~Heap();
		void insert(int x);
		void PairingHeap(int x, TNode* &t);
		void print();
		void print(TNode* &t);
		void deleteMin();
		void counter(TNode* &t);
		TNode* merge(TNode* &t1, TNode* &t2);
};

#include "Heap.cpp"
#endif /* HEAP_H_ */
