#include <iostream>
#include <fstream>
#include <cstring>
#include "BinarySearchTree.h"
#include "UnorderedArray.h"
using namespace std;
#define FILE "capitals.txt"									// define macro for input file name
#define city "NewDelhi"											// define macro for city name
#define coord_x "41s17"											// define macro for x-coordinate
#define coord_y "69e10"											// define macro for y-coordinate

int main() {
	BinarySearchTree<string> stringTree;
	UnorderedArray<string> stringArray;
	string line;
	// Binaray search tree
	cout << "Binary search tree implementation" << endl << endl;
	cout << "=================================" << endl;
	cout << "BST after insert" << endl;									// BST insert
	ifstream infile1 (FILE);
	while(getline(infile1, line)) {
		stringTree.insert(line);
	}
	infile1.close();
	stringTree.inOrderTraversal();
	cout << endl;
	cout << endl << "BST after remove by city name" << endl;		// BST delete
	stringTree.remove(city);
	stringTree.inOrderTraversal();
	cout << endl << "BST after remove by x-coordinate" << endl;
	stringTree.remove_by_x(coord_x);
	stringTree.inOrderTraversal();
	cout << endl << "BST after remove by y-coordinate" << endl;
	stringTree.remove_by_y(coord_y);
	stringTree.inOrderTraversal();

	// Unordered array
	cout << endl << "Unordered array implementation" << endl;
	cout << "==============================" << endl;
	cout << "Unordered array after insert" << endl;					// Unordered array insert
	ifstream infile2 (FILE);
	while(getline(infile2, line)) {
		stringArray.insert(line);
	}
	infile2.close();
	stringArray.inOrderTraversal();
	cout << endl << "List after remove by city name" << endl;	// Unordered array delete
	stringArray.remove(city);
	stringArray.inOrderTraversal();
	cout << endl << "List after remove by x-coordinate" << endl;
	stringArray.remove_by_x(coord_x);
	stringArray.inOrderTraversal();
	cout << endl << "List after remove by y-coordinate" << endl;
	stringArray.remove_by_y(coord_y);
	stringArray.inOrderTraversal();
	return 0;
}
