#include "TNode.h"
#include <iostream>
using namespace std;

template <typename T>
TNode<T>::TNode(T v) {
	value = v;
	left = NULL;
	right = NULL;
}

template <typename T>
TNode<T>::TNode(T v1, T v2, T v3, TNode<T>* lt, TNode<T>* rt ) {
	value1 = v1;
	value2 = v2;
	value3 = v3;
	left = lt;
	right = rt;
}
/*
template <typename T>
void TNode<T>::print() {
	cout << value << " ";
}*/
