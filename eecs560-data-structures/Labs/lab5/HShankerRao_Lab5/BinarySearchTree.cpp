#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>
#include <stdlib.h>
#include "BinarySearchTree.h"
using namespace std;

template <typename T>
BinarySearchTree<T>::BinarySearchTree()
{
	root = NULL;
}

template <typename T>
BinarySearchTree<T>::~BinarySearchTree()
{
	makeEmpty();
}

template <typename T>
void BinarySearchTree<T>::insert(T v)
{
	stringstream line(v);
	T city_name;
	T x_coordinate;
	T y_coordinate;
	line >> city_name >> x_coordinate >> y_coordinate;
	insert(city_name, x_coordinate, y_coordinate, root);
}

template <typename T>
void BinarySearchTree<T>::insert(T v1, T v2, T v3, TNode<T>* &t)
{
	if( t == NULL )
		t = new TNode<T>(v1, v2, v3, NULL, NULL);
	else if( v1 <= t->value1 )
		insert( v1, v2, v3, t->left );
	else if( t->value1 < v1 )
		insert( v1, v2, v3, t->right );
}

template <typename T>
void BinarySearchTree<T>::inOrderTraversal()
{
	inOrderTraversal(root);
}

template <typename T>
void BinarySearchTree<T>::inOrderTraversal(TNode<T>* &t)
{
	if(t != NULL) {
		inOrderTraversal(t->left);
		cout << t->value1 << " " << t->value2 << " " << t->value3 << endl;
		inOrderTraversal(t->right);
	}
}

template <typename T>
void BinarySearchTree<T>::makeEmpty()
{
	makeEmpty(root);
}

template <typename T>
void BinarySearchTree<T>::makeEmpty(TNode<T>* &t)
{
	if(t != NULL) {
		makeEmpty(t->left);
		makeEmpty(t->right);
		delete t;
	}
	t = NULL;
}

template <typename T>
T* BinarySearchTree<T>::findMinValue()
{
	findMinValue(root);
}

template <typename T>
T* BinarySearchTree<T>::findMinValue(TNode<T>* t)
{
	if(t->left == NULL) {
		T* items = new T[3];
		items[0] = t->value1;
		items[1] = t->value2;
		items[2] = t->value3;
		return items;
	}
	else
		findMinValue(t->left);
}

template <typename T>
void BinarySearchTree<T>::remove(T v)
{
	remove(v, root);
}

template <typename T>
void BinarySearchTree<T>::remove(T v, TNode<T>* &t)
{
	if(t == NULL)
		return;
	if(v < t->value1)
		remove(v, t->left);
	else if(v > t->value1)
		remove(v, t->right);
	else if(t->left != NULL && t->right != NULL) {
		T* items = new T[3];
		items = findMinValue(t->right);
		t->value1 = items[0];
		t->value2 = items[1];
		t->value3 = items[2];
		remove(t->value1, t->right);
	}
	else {
		TNode<T>* oldNode = t;
		if(t->left != NULL)
			t = t->left;
		else
			t = t->right;
		delete oldNode;
	}
}

template <typename T>
void BinarySearchTree<T>::remove_by_x(T x)
{
	remove_by_x(x, root);
}

template <typename T>
void BinarySearchTree<T>::remove_by_x(T x, TNode<T>* &t)
{
	if(t == NULL)
		return;
	if(x < t->value2)
		remove_by_x(x, t->left);
	else if(x > t->value2)
		remove_by_x(x, t->right);
	else if(t->left != NULL && t->right != NULL) {
		T* items = new T[3];
		items = findMinValue(t->right);
		t->value1 = items[0];
		t->value2 = items[1];
		t->value3 = items[2];
		remove_by_x(t->value2, t->right);
	}
	else {
		TNode<T>* oldNode = t;
		if(t->left != NULL)
			t = t->left;
		else
			t = t->right;
		delete oldNode;
	}
}

template <typename T>
void BinarySearchTree<T>::remove_by_y(T y)
{
	remove_by_y(y, root);
}

template <typename T>
void BinarySearchTree<T>::remove_by_y(T y, TNode<T>* &t)
{
	if(t == NULL)
		return;
	if(y < t->value3)
		remove_by_y(y, t->left);
	else if(y > t->value3)
		remove_by_y(y, t->right);
	else if(t->left != NULL && t->right != NULL) {
		T* items = new T[3];
		items = findMinValue(t->right);
		t->value1 = items[0];
		t->value2 = items[1];
		t->value3 = items[2];
		remove_by_y(t->value3, t->right);
	}
	else {
		TNode<T>* oldNode = t;
		if(t->left != NULL)
			t = t->left;
		else
			t = t->right;
		delete oldNode;
	}
}
