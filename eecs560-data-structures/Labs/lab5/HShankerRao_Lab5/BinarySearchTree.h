#ifndef BINARYSEARCHTREE_H_
#define BINARYSEARCHTREE_H_
#include "TNode.h"
template <typename T>
class BinarySearchTree {
	private:
		TNode<T> *root;
	public:
		BinarySearchTree();
		void insert(T v);
		void insert(T v1, T v2, T v3, TNode<T>* &t);
		void inOrderTraversal();
		void inOrderTraversal(TNode<T>* &t);
		void makeEmpty();
		void makeEmpty(TNode<T>* &t);
		void remove(T v);
		void remove(T v, TNode<T>* &t);
		void remove_by_x(T x);
		void remove_by_x(T x, TNode<T>* &t);
		void remove_by_y(T y);
		void remove_by_y(T y, TNode<T>* &t);
		T* findMinValue();
		T* findMinValue(TNode<T>* t);
		~BinarySearchTree();
};
#include "BinarySearchTree.cpp"
#endif /* BINARYSEARCHTREE_H_ */
