#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>
#include <stdlib.h>
#include "UnorderedArray.h"
using namespace std;

template <typename T>
UnorderedArray<T>::UnorderedArray()
{
	root = NULL;
}

template <typename T>
UnorderedArray<T>::~UnorderedArray()
{
	makeEmpty();
}

template <typename T>
void UnorderedArray<T>::insert(T v)
{
	stringstream line(v);
	T city_name;
	T x_coordinate;
	T y_coordinate;
	line >> city_name >> x_coordinate >> y_coordinate;
	insert(city_name, x_coordinate, y_coordinate, root);
}

template <typename T>
void UnorderedArray<T>::insert(T v1, T v2, T v3, TNode<T>* &t)
{
	if( t == NULL )
		t = new TNode<T>(v1, v2, v3, NULL, NULL);
	else
		insert( v1, v2, v3, t->right );
}

template <typename T>
void UnorderedArray<T>::inOrderTraversal()
{
	inOrderTraversal(root);
}

template <typename T>
void UnorderedArray<T>::inOrderTraversal(TNode<T>* &t)
{
	if(t != NULL) {
		cout << t->value1 << " " << t->value2 << " " << t->value3 << endl;
		inOrderTraversal(t->right);
	}
}

template <typename T>
void UnorderedArray<T>::makeEmpty()
{
	makeEmpty(root);
}

template <typename T>
void UnorderedArray<T>::makeEmpty(TNode<T>* &t)
{
	if(t != NULL) {
		makeEmpty(t->right);
		delete t;
	}
	t = NULL;
}

template <typename T>
T* UnorderedArray<T>::findLast()
{
	findLast(root);
}

template <typename T>
T* UnorderedArray<T>::findLast(TNode<T>* t)
{
	if(t->right == NULL)
	{
		T* items = new T[3];
		items[0] = t->value1;
		items[1] = t->value2;
		items[2] = t->value3;
		return items;
	}
	else
		findLast(t->right);
}

template <typename T>
void UnorderedArray<T>::remove(T v)
{
	remove(v, root);
}

template <typename T>
void UnorderedArray<T>::remove(T v, TNode<T>* &t)
{
	if(t == NULL)
		return;
	if(v != t->value1)
		remove(v, t->right);
	else if(t->right != NULL) {
		T* items = new T[3];
		items = findLast(t->right);
		t->value1 = items[0];
		t->value2 = items[1];
		t->value3 = items[2];
		remove(t->value1, t->right);
	}
	else {
		TNode<T>* oldNode = t;
		t = t->right;
		delete oldNode;
	}
}

template <typename T>
void UnorderedArray<T>::remove_by_x(T x)
{
	remove_by_x(x, root);
}

template <typename T>
void UnorderedArray<T>::remove_by_x(T x, TNode<T>* &t)
{
	if(t == NULL)
		return;
	if(x != t->value2)
		remove(x, t->right);
	else if(t->right != NULL) {
		T* items = new T[3];
		items = findLast(t->right);
		t->value1 = items[0];
		t->value2 = items[1];
		t->value3 = items[2];
		remove(t->value2, t->right);
	}
	else {
		TNode<T>* oldNode = t;
		t = t->right;
		delete oldNode;
	}
}

template <typename T>
void UnorderedArray<T>::remove_by_y(T y)
{
	remove_by_y(y, root);
}

template <typename T>
void UnorderedArray<T>::remove_by_y(T y, TNode<T>* &t)
{
	if(t == NULL)
		return;
	if(y != t->value3)
		remove(y, t->right);
	else if(t->right != NULL) {
		T* items = new T[3];
		items = findLast(t->right);
		t->value1 = items[0];
		t->value2 = items[1];
		t->value3 = items[2];
		remove(t->value3, t->right);
	}
	else {
		TNode<T>* oldNode = t;
		t = t->right;
		delete oldNode;
	}
}
