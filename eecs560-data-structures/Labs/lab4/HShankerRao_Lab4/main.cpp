#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include "timetest.h"
using namespace std;

// Function declarations
void hw1_6_rev(int array[], int size);
void tests(int n, int numberOfTests);

int main()
{
  int x, n=0, pos=0;
  int* Arr;
// Counting number of elments in the datafile
  ifstream countFile ("data3.txt");
  if (countFile.is_open()) {
    while (countFile >> x) {
      n++;
    }
  }
  countFile.close();
  Arr = new int[n];
// Pushing file elements into the dynamically allocated array
  ifstream readFile ("data3.txt");
  if (readFile.is_open()) {
    while (readFile >> x){
      Arr[pos] = x;
      pos++;
    }
  }
  readFile.close();
// Printing unique element count
  hw1_6_rev(Arr, n);               //  cout << "Revised HW-1.6 count = " << countRev << endl;
// Random number generation and tesing with our program
  srand((unsigned)time(0));
  cout << "n\tAverage time\n";
  n = 20000;
  for(int i=0; i<5; i++) {
    tests(n, 10);
    n = 2*n;
  }
  return 0;
}

void tests(int n, int numberOfTests) {
// Create an array to hold the test times
  double* hw1_6_Times = new double[numberOfTests];
  for(int i = 0; i < numberOfTests; i++) {
    int* data = generateRandomData(n);                   // Generate a new random array of size n
    hw1_6_Times[i] = timeTest(hw1_6_rev, data, n);       // Time test the algorithm and store the time
    delete [] data;
  }
  cout << n << "\t";
  displayTimes("hw1_6_rev", hw1_6_Times, numberOfTests); // Print out the times
  delete [] hw1_6_Times;
}

// Removing duplicates and counting remaining elements in the array
void hw1_6_rev(int array[], int size)
{
  int count=0, flag=0;
  int arr[size];
  arr[count]=array[count];
  for(int i=1; i<=size; i++) {
    int flag=0;
    for(int j=0; j<i; j++)
      if(array[i]==array[j])
        flag=1;
    if(flag==0)
    {  
      count++;
      arr[count]=array[i];
    }
  }
}
