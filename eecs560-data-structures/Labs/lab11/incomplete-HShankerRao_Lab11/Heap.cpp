using namespace std;

Heap::Heap()
{
  root = NULL;
  count = 0;
}

Heap::~Heap()
{
  root = NULL;
  count = 0;
}

void Heap::insert(int x)
{
  PairingHeap(x, root);
}

void Heap::PairingHeap(int x, TNode* &t)
{
  if( t == NULL )
    root = new TNode(x, NULL, NULL, NULL);
  else if(x > t->value)
  {
    if(t->leftChild == NULL)
      t->leftChild = new TNode(x, t, NULL, NULL);
    else
      t->leftChild = new TNode(x, t, NULL, t->leftChild);
  }
  else if(x < t->value)
    root = new TNode(x, NULL, t, NULL);
}

void Heap::print()
{
  print(root);
}

void Heap::print(TNode* &t)
{
  if(t != NULL)
  {
    cout << t->value << " ";
    if(t->nextSibling)
      print(t->nextSibling);
    if(t->leftChild)
    {
      cout << endl << t->value << ": ";
      print(t->leftChild);
    }
  }
}

void Heap::deleteMin()
{
  if(root->leftChild)
  {
    count++;
    counter(root->leftChild);
  }
  TNode *ChildHeaps[count], *firstpass[count/2], *secondpass;
  TNode* childheap = root->leftChild;
  for(int i=0; i<count; i++) {
    ChildHeaps[i] = childheap;
    childheap = ChildHeaps[i]->nextSibling;
  }
//First-pass merge: left-to-right
  int j=0;
  for(int i=0; i<count/2; i++)
  {
    firstpass[i] = merge(ChildHeaps[j], ChildHeaps[j+1]);
    j = j+2;
  }
  if(count % 2)
    firstpass[count/2-1] = merge(firstpass[count/2-1], ChildHeaps[count-1]);
//Second-pass merge: right-to-left
  secondpass = firstpass[count/2-1];
  if(count/2-1 > 1)
    for(int i=count/2-1; i>0; i--)
      secondpass = merge(firstpass[i-1], secondpass);
  root = secondpass;
  secondpass = NULL;
}

void Heap::counter(TNode* &t)
{
  if(t->nextSibling)
  {
    count++;
    counter(t->nextSibling);
  }
}

TNode* Heap::merge(TNode* &t1, TNode* &t2)
{
  TNode *smaller, *bigger;
  if(t1->value < t2->value)
  {
    if(t1->leftChild == NULL)
      bigger = new TNode(t2->value, t1, t2->leftChild, NULL);
    else
      bigger = new TNode(t2->value, t1, t2->leftChild, t1->leftChild);
    smaller = new TNode(t1->value, NULL, bigger, NULL);
  }
  else
  {
    if(t2->leftChild == NULL)
      bigger = new TNode(t1->value, t2, t1->leftChild, NULL);
    else
      bigger = new TNode(t1->value, t2, t1->leftChild, t2->leftChild);
    smaller = new TNode(t2->value, NULL, bigger, NULL);
  }
  return smaller;
}
