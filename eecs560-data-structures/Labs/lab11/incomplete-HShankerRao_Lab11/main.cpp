#include <iostream>
#include <fstream>
#include <stdlib.h>
#include "Heap.h"
using namespace std;
#define FILE "input2.txt"									// define macro for input file name
//input.txt = 6 8 4 5 2 10

int main()
{
  cout << "---------------------------------------------------------------" << endl;
  cout << "Pairing-heap:" << endl;
// Build pairing heap by inserting elements
  Heap binaryHeap;
  int i;
  ifstream infile (FILE);
  while(infile >> i)
    binaryHeap.insert(i);
  infile.close();
  binaryHeap.print();
  cout << endl;
// deleteMin
  cout << "Heap after deleteMin:" << endl;
  binaryHeap.deleteMin(); 
  binaryHeap.print();
  cout << endl;
  cout << "---------------------------------------------------------------" << endl;
  return 0;
}
