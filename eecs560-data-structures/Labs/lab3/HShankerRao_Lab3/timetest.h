#ifndef TIMETEST_H_
#define TIMETEST_H_
#include <stdio.h>
#include <stdlib.h>
#include "Timer.cpp"
using namespace std;

// Globally accessible Timer
Timer timer = Timer();
int generateRandomInteger(int range);
int* generateRandomData(int n);
double timeTest(void (*function)(int*, int), int* data, int n);
void displayTimes(const char functionName[], double* times, int numberOfTests);

int generateRandomInteger(int range)
{
  return (rand() % range);
}

int* generateRandomData(int n)
{
  int* a;
  int range=40000;
  a = new int[n];
  for(int i=0; i<n; i++)
    a[i] = generateRandomInteger(range);
  return a;
}

double timeTest(void (*function)(int*, int), int* data, int n)
{
  timer.start();
  function(data, n);
  double duration = timer.stop();
  return duration;
}

void displayTimes(const char functionName[], double* times, int numberOfTests)
{
  cout << functionName << endl;
  for(int i=0; i<numberOfTests; i++)
    cout << times[i] << endl;
}
#endif /* TIMETEST_H_ */
