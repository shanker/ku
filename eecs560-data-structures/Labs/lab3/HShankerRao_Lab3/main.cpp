#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include "timetest.h"
using namespace std;

// Function declarations
void hw1_6_orig(int array[], int size);
void hw1_6_rev(int array[], int size);
void tests(int n, int numberOfTests);

int main()
{
  int x, n=0, pos=0;
  int* Arr;
// Counting number of elments in the datafile
  ifstream countFile ("data3.txt");
  if (countFile.is_open()) {
    while (countFile >> x) {
      n++;
    }
  }
  countFile.close();
  Arr = new int[n];
// Pushing file elements into the dynamically allocated array
  ifstream readFile ("data3.txt");
  if (readFile.is_open()) {
    while (readFile >> x){
      Arr[pos] = x;
      pos++;
    }
  }
  readFile.close();
// Printing unique element count
  hw1_6_orig(Arr, n);              //  cout << "Original HW-1.6 count = " << countOrig << endl;
  hw1_6_rev(Arr, n);               //  cout << "Revised HW-1.6 count = " << countRev << endl;
// Random number generation and tesing with our program
  srand((unsigned)time(0));
  tests(200000, 10);
  return 0;
}

void tests(int n, int numberOfTests) {
// Create an array to hold the test times
  double* hw1_6_Times = new double[numberOfTests];
  for(int i = 0; i < numberOfTests; i++) {
    int* data = generateRandomData(n);                   // Generate a new random array of size n
    hw1_6_Times[i] = timeTest(hw1_6_rev, data, n);       // Time test the algorithm and store the time
    delete [] data;
  }
  cout << "[n = " << n << "]" << endl;                   // Display the size of array tested
  displayTimes("hw1_6_rev", hw1_6_Times, numberOfTests); // Print out the times
  delete [] hw1_6_Times;
}

// Removing duplicates and counting remaining elements in the array
void hw1_6_orig(int array[], int size)
{
  int count=0, flag=0;
  int arr[size];
  arr[0]=array[0];
  for(int i=1; i<size; i++) {
    for(int j=0; j<(i-1); j++) {
      int flag=0;
      if(array[i]==array[j])
        flag=1;
      else
        count++;
    }
    if(flag==0)
      arr[count]=array[i];
  }
  ofstream outstream;
  outstream.open("Rao_HShanker_lab3_orig_results");
  outstream << "array without duplicates:" << endl;
  for(int b=0; b<count; b++)
    outstream << arr[b] << " ";
  outstream << endl << endl << "Last index: " << count << endl;
}

void hw1_6_rev(int array[], int size)
{
  int count=0, flag=0;
  int arr[size];
  arr[count]=array[count];
  for(int i=1; i<=size; i++) {
    int flag=0;
    for(int j=0; j<i; j++)
      if(array[i]==array[j])
        flag=1;
    if(flag==0)
    {  
      count++;
      arr[count]=array[i];
    }
  }
  ofstream outstream;
  outstream.open("Rao_HShanker_lab3_rev_results");
  outstream << "array without duplicates:" << endl;
  for(int b=0; b<count; b++)
    outstream << arr[b] << " ";
  outstream << endl << endl << "Last index: " << count << endl;
}
