Type following commands:
make			# to compile program and make executable
./lab3			# to run the executable
make clean		# to remove executable and object files after completion

Times_10 : This file contains time values from 10 tests
Rao_HShanker_orig_results : This file prints array contents and last element index from the original algorithm implementation
Rao_HShanker_rev_results : This file prints array contents and last element index from the revised algorithm implementation
