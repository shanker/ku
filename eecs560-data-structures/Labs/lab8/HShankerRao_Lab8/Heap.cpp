#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <cmath>
#include "Heap.h"
using namespace std;

Heap::Heap()
{
	size = 0;
	array = new int[size];
	index = 0;
}

Heap::Heap(int theSize)
{
	size = theSize;
	array = new int[size];
	index = 0;
}

Heap::Heap(int* data, int n)
{
	size = n;
	array = new int[size];
	index = 0;
	for(int j=0; j<size; j++)
	  array[j] = data[j];
}

Heap::~Heap()
{
	size = 0;
	delete [] array;
	index = 0;
}

void Heap::insert(int x)
{
	int position = index;
	percolateUp(position, x);
	index++;
}

void Heap::percolateUp(int position, int x)
{
	if(x < array[position/2])
	{
	  if(position == 0)
	    array[position] = x;
	  else
	  {
	    array[position] = array[position/2];
	    percolateUp(position/2, x);
	  }
	}
	else
	  array[position] = x;
}

void Heap::percolateDown(int position)
{
  if(2*position <= size)
  {
    int newposition;
    if((array[2*position-1] < array[2*position]) and (array[2*position-1] < array[position-1]))
    {
	   int temp = array[position-1];
	   array[position-1] = array[2*position-1];
	   array[2*position-1] = temp;
	   newposition = 2*position;
		percolateDown(newposition);
    }
    else if((array[2*position] < array[2*position-1]) and (array[2*position] < array[position-1]))
    {
      int temp = array[position-1];
      array[position-1] = array[2*position];
      array[2*position] = temp;
      newposition = 2*position+1;
      percolateDown(newposition);
    }
  }
}

void Heap::print()
{
	for(int i=1; i<=size; i++)
	{
	  int j=1;
	  while(j<=i)
	  {
	    if(i == pow(2,j))
	      cout << endl;
	    j++;
	  }
	  if(array[i-1])
	    cout << " " << array[i-1];
	}
}
