#ifndef HEAP_H_
#define HEAP_H_

class Heap {
	private:
		int size;
		int* array;
		int index;
	public:
		Heap();
		Heap(int theSize);
		Heap(int* data, int n);
		~Heap();
		void insert(int x);
		void percolateUp(int position, int x);
		int deleteMin();
		void percolateDown(int position);
		void print();
};

#include "Heap.cpp"
#endif /* HEAP_H_ */
