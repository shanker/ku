#include <iostream>
#include <cstdlib>
#include <fstream>
#include <cmath>
#include "Heap.h"
#include "Timer.h"
#include <iomanip>
using namespace std;

Timer timer = Timer();

int main()
{
  // Initial testing of 2 heap building algorithms
  int size = 25;
  int method1[size], method2[size];
  for(int i=0; i<size; i++)
  {
    method1[i] = rand() % 100;
    method2[i] = rand() % 100;
  }
  Heap binHeap(method1, size), binaryHeap(size);
  cout << "Verification Method1: buildheap algorithm using percolate_down" << endl;
  cout << "Test array:";
  for(int j=0; j<size; j++)
    cout << " " << method1[j];
  cout << endl << "Heap:" << endl;
  for(int i=size; i>=1; i--)
    binHeap.percolateDown(i);
  binHeap.print();
  cout << endl << endl;
  cout << "Verification Method2: percolate_up" << endl;
  cout << "Test array:";
  for(int j=0; j<size; j++)
    cout << " " << method2[j];
  cout << endl << "Heap:" << endl;
  for(int j=0; j<size; j++)
    binaryHeap.insert(method2[j]);
  binaryHeap.print();
  cout << endl << endl;

  //Experimental part
  //n=250000
  cout << "Experimental part: Estimating times for each of the methods" << endl << "n = 250000" << endl;
  int n=250000, range=2*n;
  int method1a[n], method1b[n], method1c[n], method1d[n], method1e[n],
      method2a[n], method2b[n], method2c[n], method2d[n], method2e[n];
  for(int i=0; i<n; i++)
  {
    method1a[i] = rand() % range;
    method1b[i] = rand() % range;
    method1c[i] = rand() % range;
    method1d[i] = rand() % range;
    method1e[i] = rand() % range;
    method2a[i] = rand() % range;
    method2b[i] = rand() % range;
    method2c[i] = rand() % range;
    method2d[i] = rand() % range;
    method2e[i] = rand() % range;
  }
  Heap binHeap1(method1a, n), binHeap2(method1b, n), binHeap3(method1c, n), binHeap4(method1e, n), binHeap5(method1e, n),
       binaryHeap1(n*5), binaryHeap2(n*5), binaryHeap3(n*5), binaryHeap4(n*5), binaryHeap5(n*5);
  double duration1a[5], duration1b[5], duration1c[5], duration1d[5], duration1e[5],
         duration2a[5], duration2b[5], duration2c[5], duration2d[5], duration2e[5];
  for(int i=0; i<5; i++) {
    //Method1: buildheap algorithm with percolate down
    timer.start();
    for(int j=n; j>=1; j--)
      binHeap1.percolateDown(j);
    duration1a[i] = timer.stop();
    timer.start();
    for(int j=n; j>=1; j--)
      binHeap2.percolateDown(j);
    duration1b[i] = timer.stop();
    timer.start();
    for(int j=n; j>=1; j--)
      binHeap3.percolateDown(j);
    duration1c[i] = timer.stop();
    timer.start();
    for(int j=n; j>=1; j--)
      binHeap4.percolateDown(j);
    duration1d[i] = timer.stop();
    timer.start();
    for(int j=n; j>=1; j--)
      binHeap5.percolateDown(j);
    duration1e[i] = timer.stop();
    //Method2: insertion with percolate up
    timer.start();
    for(int j=0; j<n; j++)
      binaryHeap1.insert(method2a[j]);
    duration2a[i] = timer.stop();
    timer.start();
    for(int j=0; j<n; j++)
      binaryHeap2.insert(method2b[j]);
    duration2b[i] = timer.stop();
    timer.start();
    for(int j=0; j<n; j++)
      binaryHeap3.insert(method2c[j]);
    duration2c[i] = timer.stop();
    timer.start();
    for(int j=0; j<n; j++)
      binaryHeap4.insert(method2d[j]);
    duration2d[i] = timer.stop();
    timer.start();
    for(int j=0; j<n; j++)
      binaryHeap5.insert(method2e[j]);
    duration2e[i] = timer.stop();
  }
  double sum1a=0, sum1b=0, sum1c=0, sum1d=0, sum1e=0, sum2a=0, sum2b=0, sum2c=0, sum2d=0, sum2e=0;
  cout << "\t\tMETH_1a  METH_1b  METH_1c  METH_1d  METH_1e\t\tMETH_2a  METH_2b  METH_2c  METH_2d  METH_2e" << endl;
  for(int i=0; i<5; i++)
  {
    sum1a = sum1a+duration1a[i];
    sum1b = sum1b+duration1b[i];
    sum1c = sum1c+duration1c[i];
    sum1d = sum1d+duration1d[i];
    sum1e = sum1e+duration1e[i];
    sum2a = sum1a+duration2a[i];
    sum2b = sum1b+duration2b[i];
    sum2c = sum1c+duration2c[i];
    sum2d = sum1d+duration2d[i];
    sum2e = sum1e+duration2e[i];
    cout << fixed << setprecision(5) << "\t\t"
      << duration1a[i] << "  " << duration1b[i] << "  " << duration1c[i] << "  " << duration1d[i] << "  " << duration1e[i] << "\t\t"
      << duration2a[i] << "  " << duration2b[i] << "  " << duration2c[i] << "  " << duration2d[i] << "  " << duration2e[i] << endl;
  }
  cout << endl << "AVERAGES:" << "\t";
  cout << fixed << setprecision(5)
    << sum1a/5 << "  " << sum1b/5 << "  " << sum1c/5 << "  " << sum1d/5 << "  " << sum1e/5 << "\t\t"
    << sum2a/5 << "  " << sum2b/5 << "  " << sum2c/5 << "  " << sum2d/5 << "  " << sum2e/5 << endl << endl;
  //n=500000
  cout << "n = 500000" << endl;
  int n2=500000, range2=2*n2;
  int method1a2[n2], method1b2[n2], method1c2[n2], method1d2[n2], method1e2[n2],
      method2a2[n2], method2b2[n2], method2c2[n2], method2d2[n2], method2e2[n2];
  for(int i=0; i<n2; i++) {
    method1a2[i] = rand() % range2;
    method1b2[i] = rand() % range2;
    method1c2[i] = rand() % range2;
    method1d2[i] = rand() % range2;
    method1e2[i] = rand() % range2;
    method2a2[i] = rand() % range2;
    method2b2[i] = rand() % range2;
    method2c2[i] = rand() % range2;
    method2d2[i] = rand() % range2;
    method2e2[i] = rand() % range2;
  }
  Heap binHeap6(method1a2, n2), binHeap7(method1b2, n2), binHeap8(method1c2, n2), binHeap9(method1d2, n2), binHeap10(method1e2, n2),
       binaryHeap6(n2*5), binaryHeap7(n2*5), binaryHeap8(n2*5), binaryHeap9(n2*5), binaryHeap10(n2*5);
  double duration1a2[5], duration1b2[5], duration1c2[5], duration1d2[5], duration1e2[5],
         duration2a2[5], duration2b2[5], duration2c2[5], duration2d2[5], duration2e2[5];
  for(int i=0; i<5; i++) {
  //Method1: buildheap algorithm with percolate down
    timer.start();
    for(int j=n2; j>=1; j--)
      binHeap6.percolateDown(j);
    duration1a2[i] = timer.stop();
    timer.start();
    for(int j=n2; j>=1; j--)
      binHeap7.percolateDown(j);
    duration1b2[i] = timer.stop();
    timer.start();
    for(int j=n2; j>=1; j--)
      binHeap8.percolateDown(j);
    duration1c2[i] = timer.stop();
    timer.start();
    for(int j=n2; j>=1; j--)
      binHeap9.percolateDown(j);
    duration1d2[i] = timer.stop();
    timer.start();
    for(int j=n2; j>=1; j--)
      binHeap10.percolateDown(j);
    duration1e2[i] = timer.stop();
    //Method2: insertion with percolate up
    timer.start();
    for(int j=0; j<n2; j++)
      binaryHeap6.insert(method2a2[j]);
    duration2a2[i] = timer.stop();
    timer.start();
    for(int j=0; j<n2; j++)
      binaryHeap7.insert(method2b2[j]);
    duration2b2[i] = timer.stop();
    timer.start();
    for(int j=0; j<n2; j++)
      binaryHeap8.insert(method2c2[j]);
    duration2c2[i] = timer.stop();
    timer.start();
    for(int j=0; j<n2; j++)
      binaryHeap9.insert(method2d2[j]);
    duration2d2[i] = timer.stop();
    timer.start();
    for(int j=0; j<n2; j++)
      binaryHeap10.insert(method2e2[j]);
    duration2e2[i] = timer.stop();
  }
  double sum1a2=0, sum1b2=0, sum1c2=0, sum1d2=0, sum1e2=0, sum2a2=0, sum2b2=0, sum2c2=0, sum2d2=0, sum2e2=0;
  cout << "\t\tMETH_1a  METH_1b  METH_1c  METH_1d  METH_1e\t\tMETH_2a  METH_2b  METH_2c  METH_2d  METH_2e" << endl;
  for(int i=0; i<5; i++)
  {
    sum1a2 = sum1a2+duration1a2[i];
    sum1b2 = sum1b2+duration1b2[i];
    sum1c2 = sum1c2+duration1c2[i];
    sum1d2 = sum1d2+duration1d2[i];
    sum1e2 = sum1e2+duration1e2[i];
    sum2a2 = sum1a2+duration2a2[i];
    sum2b2 = sum1b2+duration2b2[i];
    sum2c2 = sum1c2+duration2c2[i];
    sum2d2 = sum1d2+duration2d2[i];
    sum2e2 = sum1e2+duration2e2[i];
    cout << fixed << setprecision(5) << "\t\t"
      << duration1a2[i] << "  " << duration1b2[i] << "  " << duration1c2[i] << "  " << duration1d2[i] << "  " << duration1e2[i] << "\t\t"
      << duration2a2[i] << "  " << duration2b2[i] << "  " << duration2c2[i] << "  " << duration2d2[i] << "  " << duration2e2[i] << endl;
  }
  cout << endl << "AVERAGES:" << "\t";
  cout << fixed << setprecision(5)
    << sum1a2/5 << "  " << sum1b2/5 << "  " << sum1c2/5 << "  " << sum1d2/5 << "  " << sum1e2/5 << "\t\t"
    << sum2a2/5 << "  " << sum2b2/5 << "  " << sum2c2/5 << "  " << sum2d2/5 << "  " << sum2e2/5 << endl << endl;
  return 0;
}
