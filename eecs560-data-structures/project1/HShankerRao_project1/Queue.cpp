#include <iostream>
#include <sstream>
#include <cstring>
using namespace std;

template <typename T>
Queue<T>::Queue()
{
	first = last = new Node<T>();
}

template <typename T>
Queue<T>::~Queue()
{
	while(first->next != NULL) {
		last = first;
		delete last;
	}
	last = first;
	delete first;
}

template <typename T>
void Queue<T>::enqueue(T x)
{
	Node<T>* n = new Node<T>();
	n->value = x;
	last->next = n; 
	last = last->next;
}

template <typename T>
T Queue<T>::dequeue()
{
	T result;
	if (first==NULL && last==NULL)
		cout << "empty queue" << endl;
	else if (first != last) {
		result = first->next->value;
		Node<T>* discard = first->next;
		first->next = discard->next;
		if (first == last) last = first;
		delete discard;
	}
	return result;
}
