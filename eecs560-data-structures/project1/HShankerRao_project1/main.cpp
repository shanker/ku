#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stdlib.h>
#include "Node.h"
#include "Queue.h"
#include <ctime>
using namespace std;
#define FILE "proj1data.txt"													// define macro for input file name

//Declaring global variables
Queue<string> Q_high, Q_medium, Q_low, Q_high_wait, Q_low_wait;	// declare queues
int size_H=0, size_M=0, size_L=0, size_HW=0, size_LW=0;				// initial queue sizes

//Function declaration
void readFile();																	// file i/o operation
void Q_fill(string line, string priority);								// pushing items into appropriate queue
void schedular(Queue<string> &Q);											// preempt queue schedular

//Main
int main() {
	cout << "Job-ID\tEnter-time\tStart-time\t\tEnd-time\n";
	readFile();
	while(size_H > 0 or size_M > 0 or size_L > 0) {
		if (size_H > 0)	schedular(Q_high);
		else if(size_M > 0)	schedular(Q_medium);
		else if(size_L > 0)	schedular(Q_low);
	}
	return 0;
}

//Function definitions
void readFile()
{
	char buff[20];
	time_t enter;
	string line;
	ifstream infile (FILE);
	while(getline(infile, line)) {			// read file and get one line at a time
		enter = time (NULL);
		strftime(buff, 20, "%H:%M:%S", localtime(&enter));
		if (size_H == 2 and size_L == 8) {	// break if high and low queues are full
			schedular(Q_high);
		}
		stringstream ss(line);					// tokenize line and put 3 pieces of information into a temporary array
		string s;
		string arr[3] = {};
		int i=0;
		while (getline(ss, s, ' ')) {
			arr[i] = s;
			i++;
		}
		cout << arr[0] << "\t" << buff << endl;
		Q_fill(line, arr[1].c_str());			// check priorities and fill appropriate queues
	}
	infile.close();
}

void Q_fill(string line, string priority)
{
		if(priority == "0") {
			if(size_H >= 0 and size_H < 2) {
				Q_high.enqueue(line);
				size_H++;
			}
			else if(size_H == 2) {
				Q_high_wait.enqueue(line);
				size_HW++;
			}
		}
		if(priority == "3") {
			if(size_L >= 0 and size_L < 8) {
				Q_low.enqueue(line);
				size_L++;
			}
			else {
				Q_low_wait.enqueue(line);
				size_LW++;
			}
		}
		if(priority == "1" or priority == "2") {
			if(size_H >= 0 and size_H < 2) {
				Q_high.enqueue(line);
				size_H++;
			}
			else if(size_H == 2) {
				Q_high_wait.enqueue(line);
				size_HW++;
			}
		}
}

/***********************************************************
*	int slice_p0 = always in high
*	int slice_p1 = 2*tH = 800 -> move to medium
*	int slice_p2 = 1*tH + 1*tM = 400+250 = 650 -> move to low
*	int slice_p3 = always in low
************************************************************/

void schedular(Queue<string> &Q) {
	char buff1[20], buff2[20];
	time_t start, end;
	start = time (NULL);									// start time
	strftime(buff1, 20, "%H:%M:%S.%s", localtime(&start));		// convert times into hh:mm:ss.milliseconds format
	stringstream ssss(Q.dequeue());					// tokenize line and put 3 pieces of information into a temporary array
	string str;
	string arr[3] = {};
	int i=0;
	while (getline(ssss, str, ' ')) {
		arr[i] = str;
		i++;
	}
	string id = arr[0];									// job ID
	string priority = arr[1];							// job priority
	int exec_time = atoi(arr[2].c_str());			// job execution time
// Things to do if priority is 0
	if(priority == "0") {
		size_H--;
		if(size_HW > 0) {									// immediately refill high queue either from
			Q_high.enqueue(Q_high_wait.dequeue());	// waiting queue or by reading file
			size_HW--;
			size_H++;
		}
		else {
			readFile();
		}
		end = time (NULL);								// end time
		if(exec_time <= 400 and size_H > 0) {		// if job finishes in less than alloted time slice
			schedular(Q_high);							// assign another job from the same queue
		}
		else {												// otherwise assign job from medium/low queue
			if(size_M > 0)
				schedular(Q_medium);
			else if(size_L > 0)
				schedular(Q_low);
		}
		strftime(buff2, 20, "%H:%M:%S.%s", localtime(&end));
		cout << id << "\t\t\t" << buff1 << "\t" << buff2 << endl;	// print times
	}
// Things to do if priority is 1
	if(priority == "1") {
		if(exec_time <= 800) {							// job gets 2 time slices of high queue
			size_H--;
			if(size_HW > 0) {
				Q_high.enqueue(Q_high_wait.dequeue());
				size_HW--;
				size_H++;
			}
			else {
				readFile();
			}
		}
		else if(exec_time > 800) {						// if not finished, move to medium queue
			size_H--;
			if(size_M < 5) {
				Q_medium.enqueue(str);
				size_M++;
			}
			else {											// if medium queue is not empty, put in wait queue
				Q_high_wait.enqueue(str);
				size_HW++;
			}
			if(size_HW > 0) {								// refill high queue
				Q_high.enqueue(Q_high_wait.dequeue());
				size_HW--;
				size_H++;
			}
			else {
				readFile();
			}
			schedular(Q_medium);
		}
		end = time (NULL);
		strftime(buff2, 20, "%H:%M:%S.%s", localtime(&end));
		cout << id << "\t\t\t" << buff1 << "\t" << buff2 << endl;	// print times
	}
// Things to do if priority is 2
	if(priority == "2") {
		if(exec_time <= 400) {							// job gets one time slice in high queue
			size_H--;
			if(size_HW > 0) {
				Q_high.enqueue(Q_high_wait.dequeue());
				size_HW--;
				size_H++;
			}
			else {
				readFile();
			}
		}
		else if(exec_time > 400) {						// another time slice ine medium queue
			size_H--;
			if(size_M < 5) {
				Q_medium.enqueue(str);
				size_M++;
			}
			else {
				Q_high_wait.enqueue(str);
				size_HW++;
			}
			if(size_HW > 0) {
				Q_high.enqueue(Q_high_wait.dequeue());
				size_HW--;
				size_H++;
			}
			else {
				readFile();
			}
			if (exec_time <= 650) {
				schedular(Q_medium);
			}
			else {											// if still not finished, move to low queue
				size_M--;
				if(size_L < 8) {
					Q_low.enqueue(str);
					size_L++;
				}
				else {
					Q_low_wait.enqueue(str);
					size_HW++;
				}
				schedular(Q_low);
			}
		}
		end = time (NULL);
		strftime(buff2, 20, "%H:%M:%S.%s", localtime(&end));
		cout << id << "\t\t\t" << buff1 << "\t" << buff2 << endl;	// print times
	}
// Things to do if priority is 3
	if(priority == "3") {
		size_L--;
		if(size_LW > 0) {									// always in low queue
			Q_high.enqueue(Q_high_wait.dequeue());
			size_LW--;
			size_L++;
		}
		else {
			readFile();
		}
		end = time (NULL);
		strftime(buff2, 20, "%H:%M:%S.%s", localtime(&end));
		cout << id << "\t\t\t" << buff1 << "\t" << buff2 << endl;	// print times
	}
}
