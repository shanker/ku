#ifndef NODE_H_
#define NODE_H_

template <typename T>
class Node {
	public:
		T value;
		Node<T>* next;
};

#endif /* NODE_H_ */
