#ifndef QUEUE_H_
#define QUEUE_H_

template <typename T>
class Queue {
	private:
		Node<T>* first;
		Node<T>* last;
	public:
		Queue();
		~Queue();
		void enqueue(T x);
		T dequeue();
};

#include "Queue.cpp"
#endif /* QUEUE_H_ */


