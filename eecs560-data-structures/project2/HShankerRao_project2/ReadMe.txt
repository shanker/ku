Various data structure implementations are organised in separate directories.

1st level directories:
  1. verifyStructure : contains implementation that uses small data-set to display correctness of code. It prints list of elements used to generate data structure followed by results after each of the operation tested.
  2. timetests: actual implementation of project2.

2nd level directories:
  1. 3heap : implementation of 3-heaps.
  2. leftistHeapWB : implementation of weight-balanced leftist heap.
  3. pairingHeap : implementation of pairing heap.

Running instruction: To test code, cd to respective 2nd level directory and type "make".
