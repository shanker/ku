#include <iostream>
#include <stdlib.h>
#include "pairingHeap.h"
using namespace std;

int main()
{
  cout << "---------------------------------------------------------------" << endl;
// Generating random list
  int Size = 15;
  int pheap[Size];
  srand(2);
  for(int i=0; i<Size; i++)
  {
    pheap[i] = rand() % 50;
  }
  cout << "Build structure by inserting:" << endl;
  for(int j=0; j<Size; j++)
    cout << pheap[j] << " ";
  cout << endl << "Pairing-heap:" << endl;
// Build pairing heap by inserting array elements
  pairingHeap binaryHeap;
  for(int j=0; j<Size; j++)
    binaryHeap.insert(pheap[j]);
  binaryHeap.print();
  cout << endl;
// deleteMin
  cout << "Heap after deleteMin:" << endl;
  binaryHeap.deleteMin(); 
  binaryHeap.print();
  cout << endl;
  cout << "---------------------------------------------------------------" << endl;
  return 0;
}
