#include <iostream>
#include <stdlib.h>
#include "leftistHeapWB.h"
using namespace std;

int main()
{
  cout << "---------------------------------------------------------------" << endl;
// Generating random list
  int Size = 50;
  int pheap[Size];
//  srand(2);
  for(int i=0; i<Size; i++)
  {
    pheap[i] = rand() % 50;
  }
  cout << "Build structure by inserting: ";
  for(int j=0; j<Size; j++)
    cout << pheap[j] << " ";
  cout << endl << "Weight balanced leftist heap:" << endl;
// Build pairing heap by inserting array elements
  leftistHeapWB leftistHeap;
  for(int j=0; j<Size; j++) {
    leftistHeap.insert(pheap[j]);
  }
  leftistHeap.print();
  cout << endl;
// deleteMin
  cout << "Min value = " << leftistHeap.deleteMin() << endl; 
  cout << "Heap after deleteMin:" << endl;
  leftistHeap.print();
  cout << "---------------------------------------------------------------" << endl;
  return 0;
}
