#ifndef HEAP_H_
#define HEAP_H_

#include "TNode.h"

class leftistHeapWB {
	private:
		TNode* root;
		int wt;
	public:
		leftistHeapWB();
		~leftistHeapWB();
		void insert(int x);
		void LeftistHeapWB(TNode* &t1, TNode* &t2);
		void print();
		void print(TNode* &t);
		int deleteMin();
		int weight1(TNode* &t);
		void weight2(TNode* &t);
		void adjustChildren(TNode* &t);
};

#include "leftistHeapWB.cpp"
#endif /* HEAP_H_ */
