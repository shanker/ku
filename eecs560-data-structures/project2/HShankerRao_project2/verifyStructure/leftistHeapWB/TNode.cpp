#include "TNode.h"
using namespace std;

TNode::TNode(int v)
{
  value = v;
  parent = NULL;
  leftChild = NULL;
  rightChild = NULL;
}

TNode::TNode(int v, TNode* pr, TNode* ltCh, TNode* nxSb )
{
  value = v;
  parent = pr;
  leftChild = ltCh;
  rightChild = nxSb;
}

TNode::~TNode()
{
  parent = NULL;
  leftChild = NULL;
  rightChild = NULL;
}
