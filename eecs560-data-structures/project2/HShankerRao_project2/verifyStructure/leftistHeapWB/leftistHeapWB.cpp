using namespace std;

leftistHeapWB::leftistHeapWB()
{
  root = NULL;
}

leftistHeapWB::~leftistHeapWB()
{
  delete root;
}

void leftistHeapWB::insert(int x)
{
  TNode *t1=NULL, *t2=NULL;
  if(root == NULL)
  {
    root = new TNode(x, NULL, NULL, NULL);
    return;
  }
  else
  {
    t1 = root;
    t2 = new TNode(x, NULL, NULL, NULL);
  }
  LeftistHeapWB(t1, t2);
}

void leftistHeapWB::LeftistHeapWB(TNode* &t1, TNode* &t2)
{
  TNode *smaller;
  TNode *bigger;
  if(t1->value <= t2->value)
  {
    smaller = t1;
    bigger = t2;
  }
  else if(t1->value > t2->value)
  {
    smaller = t2;
    bigger = t1;
  }
  if(smaller->leftChild == NULL)
  {
    smaller->leftChild = bigger;
    bigger->parent = smaller;
    adjustChildren(smaller);
  }
  else if(smaller->rightChild == NULL)
  {
    smaller->rightChild = bigger;
    bigger->parent = smaller;
    adjustChildren(smaller);
  }
  else
  {
    if(smaller->rightChild->value <= bigger->value)
    {
      LeftistHeapWB(smaller->rightChild, bigger);
    }
    else
    {
      TNode* temp = smaller->rightChild;
      smaller->rightChild = bigger;
      bigger->parent = smaller;
      LeftistHeapWB(temp, bigger);
      temp = NULL;
    }
  }
  root = smaller;
}

void leftistHeapWB::adjustChildren(TNode* &t)
{
  if(t->leftChild and t->rightChild and weight1(t->leftChild) < weight1(t->rightChild))
  {
    TNode* temp = t->leftChild;
    t->leftChild = t->rightChild;
    t->rightChild = temp;
  }
  if(t->parent)
    adjustChildren(t->parent);
}

int leftistHeapWB::weight1(TNode* &t)
{
  wt=1;
  weight2(t);
  return wt;
}

void leftistHeapWB::weight2(TNode* &t)
{
  if(t->leftChild)
  {
    wt++;
    weight2(t->leftChild);
  }
  if(t->rightChild)
  {
    wt++;
    weight2(t->rightChild);
  }
}

int leftistHeapWB::deleteMin()
{
  int min = root->value;
  if(root->rightChild)
    LeftistHeapWB(root->leftChild, root->rightChild);
  else
    root = root->leftChild;
  return min;
}

void leftistHeapWB::print()
{
  print(root);
}

void leftistHeapWB::print(TNode* &t)
{
  if(t != NULL)
  {
    cout << t->value << " ";
    print(t->leftChild);
    print(t->rightChild);
    if(!t->leftChild and !t->rightChild)
      cout << endl;
  }
}
