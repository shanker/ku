#ifndef ternaryHeap_H_
#define ternaryHeap_H_

class ternaryHeap {
	private:
		int size;
		int* array;
		int index;
	public:
		ternaryHeap();
		ternaryHeap(int theSize);
		~ternaryHeap();
		void insert(int x);
		void percolateUp(int position, int x);
		int deleteMin();
		void percolateDown(int position);
		void print();
};

#include "ternaryHeap.cpp"
#endif /* ternaryHeap_H_ */
