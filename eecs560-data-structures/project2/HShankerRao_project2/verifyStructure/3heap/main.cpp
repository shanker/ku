#include <iostream>
#include <cstdlib>
#include <fstream>
#include <cmath>
#include "ternaryHeap.h"
#include <iomanip>
using namespace std;

int main()
{
  int size = 50;
  int elements[size];
  for(int i=0; i<size; i++)
    elements[i] = rand() % 100;
  ternaryHeap Heap3(size);
  cout << "------------------------------------" << endl;
  cout << "Verification: 3-heap insert" << endl;
  cout << "Test array:";
  for(int j=0; j<size; j++)
    cout << " " << elements[j];
  cout << endl << "Heap:" << endl;
  for(int j=0; j<size; j++)
    Heap3.insert(elements[j]);
  Heap3.print();
  cout << endl;
  cout << "------------------------------------" << endl;
  cout << "Verification: 3-heap deleteMin" << endl;
  int min = Heap3.deleteMin();
  cout << "Minimum = " << min << endl;
  cout << "Heap after deleteMin:" << endl;
  Heap3.print();
  cout << endl;
  cout << "------------------------------------" << endl;
  return 0;
}
