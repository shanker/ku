#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <cmath>
#include "ternaryHeap.h"
using namespace std;

ternaryHeap::ternaryHeap()
{
  size = 0;
  array = new int[size];
  index = 0;
}

ternaryHeap::ternaryHeap(int theSize)
{
  size = theSize;
  array = new int[size];
  index = 0;
}

ternaryHeap::~ternaryHeap()
{
  size = 0;
  delete [] array;
  index = 0;
}

void ternaryHeap::insert(int x)
{
  int position = index+1;
   percolateUp(position, x);
  index++;
}

void ternaryHeap::percolateUp(int position, int x)
{
  if(x < array[(position+1)/3-1])
  {
    if(position-1 == 0)
      array[position-1] = x;
    else
    {
      array[position-1] = array[(position+1)/3-1];
      percolateUp((position+1)/3, x);
    }
  }
  else
    array[position-1] = x;
}

int ternaryHeap::deleteMin()
{
  int min = array[0];
  int hole = 1;
  percolateDown(hole);
  size--;
  return min;
}

void ternaryHeap::percolateDown(int hole)
{
  int newHole;
  if(3*hole-1 < size)
  {
    if(array[3*hole-2] < array[3*hole-1] and array[3*hole-2] < array[3*hole])
    {
      if(array[3*hole-2] < array[size-1])
      {
        array[hole-1] = array[3*hole-2];
        newHole = 3*hole-1;
        percolateDown(newHole);
      }
      else
      {
        array[hole-1] = array[size-1];
      }
   }
   else if(array[3*hole-1] < array[3*hole-2] and array[3*hole-1] < array[3*hole])
   {
      if(array[3*hole-1] < array[size-1])
      {
        array[hole-1] = array[3*hole-1];
        newHole = 3*hole;
        percolateDown(newHole);
      }
      else
      {
        array[hole-1] = array[size-1];
      }
   }
   else if(array[3*hole] < array[3*hole-1] and array[3*hole] < array[3*hole-2])
   {
      if(array[3*hole] < array[size-1])
      {
        array[hole-1] = array[3*hole];
        newHole = 3*hole+1;
        percolateDown(newHole);
      }
      else
      {
        array[hole-1] = array[size-1];
      }
   }
  }
  else
  array[hole-1] = array[size-1];
}

void ternaryHeap::print()
{
   int k=1;
  for(int i=0; i<size; i++)
  {
     k++;
     int j=0;
    while(j<=i)
    {
      if(k == pow(3,j)) {
        cout << endl;
         k=k+pow(3,j);
       }
      j++;
    }
    if(array[i])
      cout << " " << array[i];
  }
}
