#ifndef TNODE_H_
#define TNODE_H_

class TNode
{
  public:
    int value;
    TNode* parent;
    TNode* leftChild;
    TNode* rightChild;
    TNode(int v);
    TNode(int v, TNode* pr, TNode* ltCh, TNode* nxSb);
    ~TNode();
};

#include "TNode.cpp"
#endif /* TNODE_H_ */
