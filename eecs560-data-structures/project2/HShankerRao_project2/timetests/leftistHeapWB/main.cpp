#include <iostream>
#include <stdlib.h>
#include "leftistHeapWB.h"
#include "Timer.h"
#include <iomanip>
using namespace std;

Timer timer = Timer();

void timetest(int n);

int main()
{
  cout << "time\t\tdelmin\t\tinsert\t\tM" << endl;
  srand(1);
  int n=50000;
  cout << "n = " << n << endl;
  for(int i=0; i<10; i++)
    timetest(n);
  n=100000;
  cout << "n = " << n << endl;
  for(int i=0; i<10; i++)
    timetest(n);
  n=200000;
  cout << "n = " << n << endl;
  for(int i=0; i<10; i++)
    timetest(n);
  n=400000;
  cout << "n = " << n << endl;
  for(int i=0; i<10; i++)
    timetest(n);
  return 0;
}

void timetest(int n)
{
// random number generator
  int min=1, max=4*n;
  int Size = n-5;
  int randArr[Size];
  for(int i=0; i<Size; i++)
    randArr[i] = (rand()%(max-min))+min;
// step 1 - random integer between 2n and 5n
  min=2*n, max=5*n;
  int M = (rand()%(max-min))+min;
  // initial heap structure
  leftistHeapWB leftistHeap;
  for(int i=0; i<Size; i++)
    leftistHeap.insert(randArr[i]);
// step 2 - start timer
  timer.start();
// step 3 - perform steps M times
  min=1, max=4*n;
  int countI=0, countD=0;
  for(int i=0; i<M; i++)
  {
    min=0, max=100;
    int X = (rand()%(max-min))+min;
    double x = (double) X/100;
    if(x>=0 and x<0.5)
    {
      // perform deleteMin()
      countD++;
      leftistHeap.deleteMin();
    }
    else
    {
      //perform insert()
      countI++;
      int y = (rand()%(max-min))+min;
      leftistHeap.insert(y);
    }
  }
// step 4 - stop timer
  double duration = timer.stop();
  cout << fixed << setprecision(5) << duration << "\t\t" << countD << "\t\t" << countI << "\t\t" << M << endl;
}
