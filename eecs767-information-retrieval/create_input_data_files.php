<?php

/*Store crawled document names into array*/
  $documents=`ls ./fetched_files`;
  $docName=split("\n", $documents);
  sort($docName);

  $docCount=count($docName)-1;
  $docTerms=array();
  $docSize=array();

/*Preprocessing documents and storing terms into array*/
  for($infile=1; $infile<=$docCount; $infile++)
  {
    $doc="./fetched_files/$docName[$infile]";
    $terms=array();
    $docPointer=fopen($doc, "r");
    $lineArray=file($doc);
    fclose($docPointer);
    foreach($lineArray as $line)
    {
      
      /*Removing html tags, punctuation marks and spaces*/
      $PreProc=trim(ereg_replace("[[:space:]]{1,}", " ", ereg_replace("[[:punct:]]{1,}", " ", strtolower(strip_tags($line)))));
      
      /*Removing blank lines*/
      $newline="^([[:space:]]*)$";
      if(!(eregi($newline, $PreProc)))
      {
        array_push($terms, $PreProc);
      }
    }
    
    /*Merge lines from all documents and then split into tokenized terms*/
    $terms=explode(" ", implode(" ", $terms));
    array_push($docSize, count($terms));
    array_push($docTerms, $terms);
  }

 

 /*Keeping track of term-document relation with common indecies*/
  $docArray=array();
  $termArray=array();
  for($i=0; $i<$docCount; $i++)
  {
      for($j=0; $j<$docSize[$i]; $j++)
     {
      array_push($docArray, $i);
      array_push($termArray, $docTerms[$i][$j]);
     }
  }
  


  asort($termArray);  //sort by values, while maintaining correlation b/w key and value
  
/*2D array, $docTerm tracks document with $k index and term with $l index*/
  $docTerm=array();
  $k=0;
  $l=0;
  foreach($termArray as $t)
  {
    if($l==0 || strcmp($docTerm[$k][0], $t)==0)
    {
      $docTerm[$k][$l] = $t;
      $l++;
    }
    elseif($l==0 || strcmp($docTerm[$k][0], $t)!=0)
    {
      $k++;
      $l=0;
      $docTerm[$k][$l] = $t;
      $l++;
    }
    elseif(strcmp($docTerm[$k][0], $t)!=0)
    {
      $k++;
      $l=0;
      $docTerm[$k][$l] = $t;
      $l++;
    }
  }


/*Creating dictionary and posting list*/
  $termUnique=array();          //Removing duplicate terms
  $invDocFreq=array();          //Inverse document frequencies
  $posting=array();             /*Posting list of all unique terms - Array with strings as "Document-ID=>Term Frequency of the term in that  
                                  document".This string is split(with split function) in later code to get the term frequency of the term 
                                  needed in computing the weight of the document.*/

  $tUniqueDocID=array();        // Holds the unique doc-ids, in which the particular term is present
  $uniqueDocID=array();         // Holds the unique doc-id arrays of all the unique terms

  for($m=0; $m<count($docTerm); $m++)
  {
    array_push($termUnique, $docTerm[$m][0]);
    
    $docID=array_keys($termArray, $docTerm[$m][0]);   //Find all instances of unique term in full array, $termArray
    sort($docID);
    
    $dt=array();
    $DT=array();
    
    foreach($docID as $T) {                           // Loop to create the array with the doc-ids(repetitive) the term is present in
      array_push($dt,$docArray[$T]);
    }
    $DT=array_count_values($dt);
    array_push($posting, $DT);
   
    $tUniqueDocID=array_unique($dt);
    
    $Udfr=count($tUniqueDocID);
     
    
    array_push($uniqueDocID,$tUniqueDocID);
    array_push($invDocFreq,log10(187/$Udfr));       // 187 is the number of crawled documents being processed. Can be changed with respect to the 
                                                    // documents being processed.
  }


/* Code to calculate the length of each document*/

$docUniqueTerms=array();
$docVectorLength=array();
$tPosting=array();


for($i=0;$i<$docCount;$i++)
{
$sum=0;

for($j=0;$j<$docSize[$i];$j++)
	
	{
    	$termKey=array_search($docTerms[$i][$j],$termUnique);
      	$tPosting=$posting[$termKey];
    
        foreach($tPosting as $key=>$value)
        {       

          if($key==$i)
          $tf=$value;
         }
        
        $idf=$invDocFreq[$termKey];

        $tdWeight=$tf*$idf;

        $sum=$sum+($tdWeight*$tdWeight);

        }

  	$dvLength=sqrt($sum);
  	array_push($docVectorLength,$dvLength);

 }


/* Data file for termUnique array(used in Query_processing.php)*/

  $TU="./outfile/termUnique";
  $TUPointer=fopen($TU, "a+");
  foreach($termUnique as $tu) {
    fwrite($TUPointer, "$tu\n");
  }
  fclose($TUPointer);


/* Data file for posting array(used in Query_processing.php)*/  

  $PS="./outfile/posting";
  $PSPointer=fopen($PS, "a+");
  
  foreach($posting as $pL) {
    foreach($pL as $key=>$value) 
      fwrite($PSPointer, "$key=>$value ");
      fwrite($PSPointer, "\n");
      }
  fclose($PSPointer);



/* Serialize the data in the data file  - "./outfile/posting" */

$linearray=file($PS);
file_put_contents("./outfile/posting_serialized_new",serialize($linearray));  // Serialized file "./outfile/posting_serialized_new" created
                                                                              // in this step is actually unserialized in Query_processing.php 
                                                                              // script and then the data is populated into the corresponding 
                                                                              // array



  
/* Data file for invDocFreq array(used in Query_processing.php)*/


 $idF="./outfile/invDocFreq";
  $idFfd=fopen($idF, "a+");
  foreach($invDocFreq as $idf) {
    fwrite($idFfd, "$idf\n");
  }
  fclose($idFfd);


/* Data file for uniqueDocID array(used in Query_processing.php)*/


 $uniDocID="./outfile/uniqueDocID";
  $uniDocIDfd=fopen($uniDocID, "a+");
  foreach($uniqueDocID as $uD) {
  foreach($uD as $doc)
    fwrite($uniDocIDfd,"$doc ");
   fwrite($uniDocIDfd,"\n");
  }
  fclose($uniDocIDfd);


/* Serialize the data in the data file  - "./outfile/uniqueDocID" */

$linearray=file($uniDocID);
file_put_contents("./outfile/uniqueDocID_serialized_new",serialize($linearray)); // Serialized file "./outfile/uniqueDocID_serialized_new" created
                                                                                 // in this step is actually unserialized in Query_processing.php 
                                                                                 // script and then the data is populated into the corresponding 
                                                                                 // array



/* Data file for docLengths array(used in Query_processing.php)*/


$DVLfp="./outfile/docLengths";
$DVLfd=fopen($DVLfp, "a+");

foreach($docVectorLength as $dleng)
{
fwrite($DVLfd,"$dleng\n");
print "$dleng\n";
}

fclose($DVLfd);

?>
