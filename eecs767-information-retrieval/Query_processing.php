
<?php

/*
 ---------------------------
 Pre-Processing Query String
 ---------------------------
 */


 /* Fetch the query entered by user in the html page */

 $query=$_POST["QueryBox"];


   $qTerms=array();
   $qTermFreq=array();
   $qUniqueTerm=array();


 /*Removing punctuation marks and spaces from query*/

   $PreProc=trim(ereg_replace("[[:space:]]{1,}", " ", ereg_replace("[[:punct:]]{1,}", " ", strtolower($query))));


 /*Removing blank lines*/

       $newline="^([[:space:]]*)$";
       if(!(eregi($newline, $PreProc)))
       {
         array_push($qTerms, $PreProc);
       }


 /*Split into tokenized query terms*/

     $qTerms=explode(" ", implode(" ", $qTerms));
     $qTermFreq=array_count_values($qTerms);
     $qUniqueTerm=array_keys($qTermFreq);
     sort($qUniqueTerm);
     $qSize=count($qUniqueTerm);


	echo "<strong>"."Query: ".$query."</strong>";
	echo "<br />";
	echo "<br />";


 /*
 ------------------------------------------------------------------------------------
 Populate arrays needed for processing , from data files created from earlier script.
 The crawled documents and the data files would be the input files of this script
 ------------------------------------------------------------------------------------
 */



  $termUnique=array();          //Contains all unique terms from all crawled documents
  $invDocFreq=array();          //Inverse document frequencies of each of the terms
  $postings=array();            /*Posting list of all unique terms - Array with strings as "Document-ID=>Term Frequency of the term in that
                                  document".This string is split(with split function) in later code to get the term frequency of the term
                                  needed in computing the weight of the document.*/

  $uniqueDocID=array();         // Holds the unique doc-id arrays of all the unique terms
  $tUniqueDocID=array();        // Temporary array holding the unique doc-ids, in which the particular term is present
  $linearray=array();           // Temporary array holding contents of data files



/* termUnique */


$tUniq= "./outfile/termUnique";
$tUniqfd=fopen($tUniq, "r");

$linearray=file($tUniq);
foreach($linearray as $line)
{
$l=trim($line);
array_push($termUnique,$l);
}

fclose($tUniqfd);

unset($linearray);

sort($termUnique);
$qTermIndex=array();

foreach($qUniqueTerm as $qT)
{
$qTKey=array_search($qT,$termUnique);
array_push($qTermIndex,$qTKey);
}

unset($termUnique);



/* invDocFreq */


$idFr="./outfile/invDocFreq";
$idFrfd=fopen($idFr, "r");

$linearray=file($idFr);
foreach($linearray as $line)
{
$l=trim($line);
array_push($invDocFreq,$l);
}

fclose($idFrfd);

unset($linearray);



$qIDFA=array();

foreach($qTermIndex as $qTI)
{
$iDFq=$invDocFreq[$qTI];
array_push($qIDFA,$iDFq);
}

unset($invDocFreq);


$qTermIDFA=array();
$qTermIDFA=array_combine($qUniqueTerm,$qIDFA);

unset($qIDFA);


/* uniqueDocID */

$uniDocIDfp="./outfile/uniqueDocID_serialized_new";
$uniqueDocID=unserialize(trim(file_get_contents($uniDocIDfp)));

$qUDid=array();
$linearray=array();
foreach($qTermIndex as $qTI)
{
$uDoID=$uniqueDocID[$qTI];
$linearray=split(" ",$uDoID);
array_push($qUDid,$linearray);
}

unset($linearray);
unset($uniqueDocID);

$qTermUDid=array();
$qTermUDid=array_combine($qUniqueTerm,$qUDid);

unset($qUDid);


/* postings */


$pstfp="./outfile/posting_serialized_new";
$postings=unserialize(trim(file_get_contents($pstfp)));

$qPost=array();
$linearray=array();
foreach($qTermIndex as $qTI)
{
$pstng=$postings[$qTI];
$linearray=split(" ",$pstng);
array_push($qPost,$linearray);
}

unset($linearray);
unset($postings);



$qTermPost=array();
$qTermPost=array_combine($qUniqueTerm,$qPost);

unset($qPost);



/*
 -----------------
 Query Processing
 -----------------
*/



/*Store crawled document names into array*/

  $documents=`ls ./fetched_files`;
  $docName=split("\n", $documents);

  $docCount=count($docName)-1;



 /* Code to calculate length of query */
  $sum=0;

  foreach($qTermFreq as $key=>$value)
  {
  $qtf=$value;
  $qidf=$qTermIDFA[$key];

  $qtermWt=$qtf*$qidf;

  $sum=$sum+($qtermWt*$qtermWt);
  }

  $qvLength=sqrt($sum);




  /* Code to get the relevant documents, having one or more query terms */



 if($qSize == 1)
  {
    $relDocIDS=$qTermUDid[$qUniqueTerm[0]];
  }

  elseif($qSize ==2)
  {
  $relDocIDS=array_merge($qTermUDid[$qUniqueTerm[0]],$qTermUDid[$qUniqueTerm[0]]);
  }
  else
  {

$tUniqueDids=array();
foreach($qUniqueTerm as $qUT)
 {

    $value= $qTermUDid[$qUT];
    array_push($tUniqueDids,$value);              // $tUniqueDids would have all the uniqueDocID arrays of all query terms(multi-dimensional)
 }


  $relDocIDS=array_merge($tUniqueDids[0],$tUniqueDids[1]);
  	for($i=2;$i<$qSize;$i++)
  		{
  			 $relDocIDS=array_merge($relDocIDS,$tUniqueDids[$i]);
  		}
  	}


$relUDtmp=array();
$relUDtmp =array_unique($relDocIDS);

$relUniqueDocIDS=array_filter($relUDtmp,"is_numeric");

sort($relUniqueDocIDS);


unset($relDocIDS);
unset($tUniqueDids);


/* Code to get the corresponding matching doc-ids, with the relevant doc-ids from
   the posting lists of each of the query term*/


   $termRelDocID=array();
   $relDocIDPost=array();

   for($i=0;$i<$qSize;$i++)
   {
   $tmp=array_intersect($relUniqueDocIDS,$qTermUDid[$qUniqueTerm[$i]]);
   array_push($relDocIDPost,$tmp);
   }

   $termRelDocID=array_combine($qUniqueTerm,$relDocIDPost);




 /* Populate array with lengths of all documents. Populated just before calculation of
    cosine similarity to ensure there is no space crunch when loading the input data
    files to create the required arrays */

$docVectorLength=array();
$dLenfp="./outfile/docLengths";

$dLenfd=fopen($dLenfp, "r");

$linearray=file($dLenfp);
foreach($linearray as $line)
{

$l=trim($line);
array_push($docVectorLength,$l);
}

fclose($dLenfd);
unset($linearray);


/* Code to calculate cosine similarity */

$dqCosSim=array();
$allCosSim=array();


for($i=0;$i<count($relUniqueDocIDS);$i++)
{
	$sum=0;

	for($j=0;$j<$qSize;$j++)
	{

        $relDocIDPost=$termRelDocID[$qUniqueTerm[$j]];
        $dkey=array_search($relUniqueDocIDS[$i],$relDocIDPost);


        if(isset($dkey))
        {


	        $qtf=$qTermFreq[$qUniqueTerm[$j]];
		$idf=$qTermIDFA[$qUniqueTerm[$j]];

                $qtermWt=$qtf*$idf;

               foreach($qTermPost as $key=>$value)
               {
                foreach($value as $entry)
                {

                 $docIDTF=split("=>",$entry);


                  if(($key==$qUniqueTerm[$j]) && ($docIDTF[0]==$relUniqueDocIDS[$i]))
		   {
		     $tf=$docIDTF[1];
                     $dtermWt=$tf*$idf;

                     break 2;
                   }

                  }
                 }

		$sum=$sum+($dtermWt*$qtermWt);

	  }


        }

         $dvLength=$docVectorLength[$relUniqueDocIDS[$i]];

	 $cossim= ($sum)/($dvLength*$qvLength);
	 array_push($allCosSim,$cossim);

 }


unset($qTermIndex);
unset($qIDFA);
unset($qTermUDid);
unset($qTermPost);
unset($docVectorLength);
unset($termUnique);
unset($relDocIDPost);



 $dqCosSim=array_combine($relUniqueDocIDS,$allCosSim);

unset($allCosSim);
unset($relUniqueDocIDS);

arsort($dqCosSim); // Reverse sort to have the entries in the descending order of the cossim values.



/* Print the URL's & cossim values*/

echo "Query Results:"."<br />";
echo "------------------------<br />";
echo "<br />";



foreach($dqCosSim as $key=>$value){
echo "<a href=\"http://localhost/fetched_files/$docName[$key]\">".$docName[$key]."</a>  --   ".$value;
echo "<br />";

}

unset($dqCosSim);


?>



