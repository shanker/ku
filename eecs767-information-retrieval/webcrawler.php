<?php
  for($i=0; $i<5; $i++) {
    $url_frontier=array();
    `wget --wait=1 --tries=2 --adjust-extension --no-cookies --no-clobber --force-directories --directory-prefix=/home/shanker/Documents/EECS767/Project/wget --input-file=/home/shanker/Documents/EECS767/Project/frontier`;
    `rm /home/shanker/Documents/EECS767/Project/frontier`;
    $fetched=`ls /home/shanker/Documents/EECS767/Project/wget/`;
    $fetched_url=split("\n", $fetched);
    array_pop($fetched_url);
    foreach($fetched_url as $f_url) {
      $infile=`ls /home/shanker/Documents/EECS767/Project/wget/$f_url/`;
      $infile=split("\n", $infile);
      array_pop($infile);
      foreach($infile as $in) {
        $IN="/home/shanker/Documents/EECS767/Project/wget/$f_url/$in";
        $inPointer=fopen($IN, "r");
        $inArray=file($IN);
        fclose($inPointer);
        $filename=preg_replace("/\./","_",$f_url);
        `mv --no-clobber /home/shanker/Documents/EECS767/Project/wget/$f_url/$in /home/shanker/Documents/EECS767/Project/fetched_files/$filename"_"$in`;
        foreach($inArray as $str) {
          $line=preg_split("(\s*<|>\s*)", $str);
          foreach($line as $urlLine) {
            if(eregi("^(a[[:space:]]href=\"http://.*((linux)|(oracle)).*)", $urlLine)) {
              $A=preg_split("/[[:space:]]/", $urlLine);
              $A=preg_split("/\"/", $A[1]);
              foreach($A as $a) {
                if($a and !eregi("^(href=)", $a)) {
                  array_push($url_frontier, $a);
                }
              }
            }
          }
        }
      }
    }
    $OUT1="/home/shanker/Documents/EECS767/Project/frontier";
    $outPointer1=fopen($OUT1, "a+");
    $url_frontier=array_unique($url_frontier);
    sort($url_frontier);
    foreach($url_frontier as $uf) {
      fwrite($outPointer1, $uf."\n");
    }
    fclose($outPointer1);
    `rm -r /home/shanker/Documents/EECS767/Project/wget/*`;
    `rm -r /home/shanker/Documents/EECS767/Project/fetched_files/*/*`;
    `rmdir --ignore-fail-on-non-empty /home/shanker/Documents/EECS767/Project/fetched_files/*`;
  }
  $url_repository=`ls /home/shanker/Documents/EECS767/Project/fetched_files`;
  $url_repository=split("\n", $url_repository);
  array_pop($url_repository);
  sort($url_repository);
  $OUT2="/home/shanker/Documents/EECS767/Project/repository";
  $outPointer2=fopen($OUT2, "w");
  foreach($url_repository as $r_url) {
    fwrite($outPointer2, "$r_url\n");
  }
  fclose($outPointer2);
?>
