################################################
#
#		H. Shanker Rao
#		KU ID 2495689
#		EECS 837 Programming project
#		Due November 29, 2011
#
################################################

#!/usr/local/bin/perl
#use strict;

#READING DATA FROM FILE
print "Please enter name of the input data file: ";
my $infile = <STDIN>;
print "Would you like to induce certain or possible rules (Enter \"certain\" or \"possible\"): ";
my $Q1 = <STDIN>;
chomp ($Q1);
print "Would you like to save characteristic sets (Enter \"yes\" or \"no\"): ";
my $Q2 = <STDIN>;
chomp ($Q2);
print "Would you like to save concept approximations of all concepts (Enter \"yes\" or \"no\"): ";
my $Q3 = <STDIN>;
chomp ($Q3);
print "Please enter name of the output file: ";
my $outfile = <STDIN>;

open(INPUT, $infile) || die "cannot open \"$infile\"";
open(OUTPUT, ">$outfile") || die "cannot open \"$outfile\"";
my @header, my @table;
my $size;
while(my $line = <INPUT>)
{
	if($line =~ /^\[/)
	{
		my @headerLine = split(/\s/, $line);
		$size = @headerLine-2;
		for(my $i=0; $i<$size; $i++)
		{ @header[$i] = @headerLine[$i+1]; }
	}
	elsif (!($line =~ /^</))
	{
		chomp($line);
		my @row = split(/\s/, $line);
		push(@table, [@row]);
	}
}
my $noCases = @table;
close INPUT;

#TRANSPOSE TABLE
my @Column;
for(my $i=0; $i<$size; $i++)
{
	my @temp;
	foreach my $x (@table)
	{ push(@temp, $x->[$i]);}
	push(@Column, [@temp]);
}

#UNIQUE ATTRIBUTE VALUES
my @attrVal;
foreach my $x (@Column)
{
	my @uniqArr;
	foreach my $y (@{$x})
	{
		if(($y ne '*') and ($y ne '?') and ($y ne '-'))
		{
			if (!grep(/$y/, @uniqArr))
			{ push(@uniqArr, $y); }
		}
	}
	push(@attrVal, [@uniqArr]);
}

#ATTRIBUTE-VALUE PAIR INDEXING
my @attIndex=();
my @valIndex=();
for(my $i=0; $i<@header-1; $i++)
{
	for(my $j=0; $j<@attrVal; $j++)
	{
		if($attrVal[$i][$j])
		{
			push(@attIndex, $header[$i]);
			push(@valIndex, $attrVal[$i][$j]);
		}
	}
}

#HANDLING MISSING VALUES & ELEMENTARY SETS
my @AVset;
my @AVpair;
my $i=0;
foreach my $valArr (@attrVal)
{	foreach my $val (@{$valArr})
	{
		for(my $j=0; $j<$noCases; $j++)
		{
			if($Column[$i][$j] eq $val)	{	push(@AVpair, $j+1);	}
			elsif($Column[$i][$j] eq "*")	{	push(@AVpair, $j+1);	}
			elsif($Column[$i][$j] eq "?")	{	;	}
			elsif($Column[$i][$j] eq "-")
			{
				for(my $a=0; $a<$noCases; $a++)
				{
					if(($Column[$size-1][$j] eq $Column[$size-1][$a]) and ($Column[$i][$a] eq $val))
					{	push(@AVpair, $j+1);	}
				}
			}
		}
		if(@AVpair!=())	{  push(@AVset, [@AVpair]); }
		@AVpair=();
	}
	$i++;
}
push(my @concept1, @{pop(@AVset)});
push(my @concept2, @{pop(@AVset)});

=pod
print "ELEMENTARY SETS\n";
foreach my $x (@AVset)
{	foreach my $y (@{$x})
	{ print $y." ";}
	print "\n";
}
print "-------\n";
print "Concepts:\n";
foreach my $c1 (@concept1) {print $c1." ";}	print "\n";
foreach my $c2 (@concept2) {print $c2." ";}	print "\n";
print "-------\n";
=cut

#CHARACTERISTIC SETS
my @Kset;
my @Ki;
my @intersection;
my $flag;
for(my $i=1; $i<=$noCases; $i++)
{
	@Ki=();
	foreach my $x (@AVset)
	{	foreach my $y (@{$x})
		{
			if($y == $i)	{	push(@Ki, [@{$x}]);	}
		}
	}
	foreach my $a (@{$Ki[0]})
	{
		$flag=0;
		foreach my $b (@Ki)
		{	foreach my $c (@{$b})
			{
				if($c == $a)	{	$flag++;	}
			}
		}
		if($flag == @Ki)	{	push(@intersection, $a);	}
	}
	push(@Kset, [@intersection]);
	@intersection=();
}

if($Q3 eq "yes")
{
	print OUTPUT "CHARACTERISTIC SETS\n";
	foreach my $x (@Kset)
	{
		foreach my $y (@{$x})
		{ print OUTPUT $y." ";}
		print OUTPUT "\n";
	}
	print OUTPUT "-------\n\n";
}

#LOWER AND UPPER APPROXIMATIONS
my @loAprx1;
my @loAprx2;
my @upAprx1;
my @upAprx2;
my @tmp = ();
foreach my $cnpt (@concept1)
{
	foreach my $x (@Kset)
	{
		$flag=0;
		foreach my $y (@{$x})
		{
			if($y == $cnpt)	{	$flag=1;	}
		}
		if($flag)	{	push(@tmp, [@{$x}]);	}
	}
}
foreach my $a (@tmp)
{	foreach my $b (@{$a})
	{
		$flag=0;
		foreach my $cnpt (@concept1)
		{
			if($b == $cnpt)	{$flag=1;}
		}
		if($flag==0)	{	shift(@tmp);	}
		else	{
					my @first = @tmp[0];
					shift(@tmp);
					splice(@tmp, @first);
				}
	}
}
foreach my $x (@tmp)	
{	foreach my $y (@{$x})
	{
		push(@loAprx1, $y);
	}
}
@tmp = ();
foreach my $cnpt (@concept2)
{
	foreach my $x (@Kset)
	{
		$flag=0;
		foreach my $y (@{$x})
		{
			if($y == $cnpt)	{	$flag=1;	}
		}
		if($flag)	{	push(@loAprx2, [@{$x}]);	}
	}
}
foreach my $a (@loAprx2)
{	foreach my $b (@{$a})
	{
		$flag=0;
		foreach my $cnpt (@concept2)
		{
			if($b == $cnpt)	{$flag=1;}
		}
		if($flag==0)	{	shift(@loAprx2);	}
		else	{
					my @first = @loAprx2[0];
					shift(@loAprx2);
					splice(@loAprx2, @first);
				}
	}
}
foreach my $cnpt (@concept1)
{
	foreach my $x (@Kset)
	{
		$flag=0;
		foreach my $y (@{$x})
		{
			if($y == $cnpt)	{	$flag=1;	}
		}
		if($flag)	{	push(@tmp, [@{$x}]);	}
	}
	foreach $a (@tmp)
	{	foreach $b (@{$a})
		{
			if (!grep(/$b/, @upAprx1))
			{ push(@upAprx1, $b); }
		}
	}
	@tmp = ();
}
foreach my $cnpt (@concept2)
{
	foreach my $x (@Kset)
	{
		$flag=0;
		foreach my $y (@{$x})
		{
			if($y == $cnpt)	{	$flag=1;	}
		}
		if($flag)	{	push(@tmp, [@{$x}]);	}
	}
	foreach $a (@tmp)
	{	foreach $b (@{$a})
		{
			if (!grep(/$b/, @upAprx2))
			{ push(@upAprx2, $b); }
		}
	}
	@tmp = ();
}
@loAprx1=sort(@loAprx1);
@loAprx2=sort(@loAprx2);
@upAprx1=sort(@upAprx1);
@upAprx2=sort(@upAprx2);

if($Q2 eq "yes")
{
	print OUTPUT "APPROXIMATIONS\n";
	print OUTPUT "Concept-1\nLower approx.:\t";
	foreach my $x (@loAprx1)	{	print OUTPUT $x." ";}	print OUTPUT "\n";
	print OUTPUT "Upper approx.:\t";
	foreach my $x (@upAprx1)	{	print OUTPUT $x." ";}	print OUTPUT "\n\n";

	print OUTPUT "Concept-2\nLower approx.:\t";
	foreach my $x (@loAprx2)	{	print OUTPUT $y." ";}	print OUTPUT "\n";
	print OUTPUT "Upper approx.:\t";
	foreach my $x (@upAprx2)	{	print OUTPUT $x." ";}	print OUTPUT "\n";
	print OUTPUT "-------\n\n";
}

#RULE INDUCTION LEM2 ALGORITHM
#CONCEPT-1 CERTAIN RULES WITH LOWER APPROX
if($Q1 eq "certain")
{
	print OUTPUT "RULES\n";
	my $certain1 = lem2 (\@attIndex, \@valIndex, \$header[-1], \$attrVal[-1][0], \@AVset, \@loAprx1);
	my $certain2 = lem2 (\@attIndex, \@valIndex, \$header[-1], \$attrVal[-1][1], \@AVset, \@loAprx2);
}
elsif($Q1 eq "possible")
{
	print OUTPUT "RULES\n";
	my $possible1 = lem2 (\@attIndex, \@valIndex, \$header[-1], \$attrVal[-1][0], \@AVset, \@upAprx1);
	my $possible2 = lem2 (\@attIndex, \@valIndex, \$header[-1], \$attrVal[-1][1], \@AVset, \@upAprx2);
}
else	{	print OUTPUT "Error: Please enter either \"certain\" or \"possible\"!\n";}

sub lem2
{
	my ($ref_attIndex, $ref_valIndex, $ref_header, $ref_attrVal, $ref_AVset, $ref_loAprx1) = @_;
	my @attIndex = @{$ref_attIndex};
	my @valIndex = @{$ref_valIndex};
	my $header = ${$ref_header};
	my $attrVal = ${$ref_attrVal};
	my @AVset = @{$ref_AVset};
	my @goal = @{$ref_loAprx1};

	my @IntSec=();
	my @IntSecselect=();
	my @IntSecselect1st=();
	my @AVselect=();
	my @AVselect1st=();
	my @AVindex=();
	my @AVindex1st=();

	while(@goal)
	{
		foreach my $x (@AVset)
		{	foreach my $y (@{$x})
			{
				foreach my $z (@goal)
				{
					if($y==$z)	{	push(@tmp, $y);	}
				}
			}
			push(@IntSec, [@tmp]);
			@tmp = ();
		}
		my @large = ();
		my $largest = @large;
		foreach my $x (@IntSec)
		{
			if(@{$x} > $largest) {	@large = @{$x};	}
		}
		my $largestRev = @large;
		my @AVcandidate = ();
		for(my $i=0; $i<$noCases; $i++)
		{
			if($largestRev == @{$IntSec[$i]})
			{
				push(@AVcandidate, [@{$AVset[$i]}]);
			}
		}
		my @AVsmall = @{$AVcandidate[0]};
		my $AVsmallest = @AVsmall;
		foreach my $x (@AVcandidate)
		{
			if(@{$x} < $AVsmallest) {	@AVsmall = @{$x};	}
		}
		my $cardinality = @AVsmall;
		for(my $i=0; $i<$noCases; $i++)
		{
			if(($largestRev == @{$IntSec[$i]}) and ($cardinality == @{$AVset[$i]}))
			{
				if (!grep(/$i/, @AVindex1st))
				{
					push(@AVindex, $i);
					push(@AVselect, [@{$AVset[$i]}]);
					push(@IntSecselect, [@{$IntSec[$i]}]);
				}
			}
		}
		unshift(@AVselect1st, shift(@AVselect));
		unshift(@IntSecselect1st, shift(@IntSecselect));
		unshift(@AVindex1st, shift(@AVindex));
		@AVselect = ();
		@IntSecselect = ();
		@AVindex = ();
		my $count = @AVselect1st;
		my @intAV = ();
		foreach my $x (@{$AVselect1st[0]})
		{
			for(my $i=0; $i<$count; $i++)
			{
				foreach my $y (@AVselect1st)
				{	foreach my $z (@{$y})
					{
						if($x == $z)	{	push(@intAV, $x);	}
					}
				}
			}
		}
		my @goalCandidate=();
		foreach my $x (@IntSecselect1st)
		{	foreach my $y (@{$x})
				{
					push(@goalCandidate, $y);
				}
		}
		my @common = ();
		foreach my $x (@intAV)
		{	
			foreach my $y (@goal)
			{
				if($x == $y)	{	push(@common, $x);	}
			}
		}
		my $intAVSize = @intAV;
		my $commonSize = @common;
		if($intAVSize == $commonSize)
		{
			print OUTPUT @AVindex1st."\n";
			foreach my $x (@AVindex1st)
			{
				print OUTPUT "(".$attIndex[$x].", ".$valIndex[$x].") ";
			}
			print OUTPUT "-> ($header, $attrVal)\n";
			my @subtract=();
			foreach my $x (@goal)
			{
				my $flag = 0;
				foreach my $m (@goalCandidate)
				{
					if($x == $m)	{$flag = 1;}
				}
				if(flag)	{	shift(@goal);	}
				else	{	push(@subtract, $x);	}
			}
			@goal = @subtract;
		}
		else
		{
			@goal = @goalCandidate;
		}
	}
}
close OUTPUT;
